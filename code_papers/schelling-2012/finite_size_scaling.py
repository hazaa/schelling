 # -*- coding: utf-8

import phase_diag
import schell
import numpy as np
import matplotlib.pylab as plt
import h5py
import os.path
import argparse

#----------------------------------------------
class finite_size_scaling():
    # compare the susceptibility in the neighboorhood of the noise-driven
    # phase transition for various simulation sizes
     def __init__(self):
         self.nwidth = 3    # number of (width,height) values 
         self.nfrac = 50     # number of frac values
         self.fmin= 0.0     # minimum value of frac
         self.fmax= 0.6     # maximum value of frac
         self.ord_prm_idx=2 # index of the selected order param
                            # 0:interf dens, 1:susceptibility, 2:spec heat
         self.dens = 0.9
         self.tol = 0.3

         self.dir_data='data'
         self.fname_data = 'finite_size_scaling'
         self.suffix_data = 'hdf5'
         self.dsetname = 'OrderParameter' #dataset name in hdf5 file
         self.dsetname_full = 'FullOrderParameter' #dataset name in hdf5 file

         self.s=[]   # schelling simulation
         self.pd=[]  # phase diagram object
         self.width= []
         self.frac=[]
         self.ord_prm=[] #store the selected order parameter
         self.range_ord_prm=[]  # index of order parameters 

         self.plot_color = False

     def init(self):       
         s = 10
         self.width= 30  + np.arange(self.nwidth*s, step=s)
         self.frac = np.linspace(self.fmin, self.fmax, num=self.nfrac) 
         self.ord_prm= np.zeros( (self.nwidth, self.nfrac) )

     def run_save(self):
         # create file 
         # see  http://h5py.alfven.org/docs-2.0/intro/quick.html
         f = h5py.File( self.get_save_fname(), 'w')
          
         # create simulation object 
         self.s=schell.Schell()
         self.s.set_params()

         # loop
         self.range_ord_prm = range( self.s.nb_ord_prm ) 

         for i in range(self.nwidth):
             # param schelling
             w = int(self.width[i])
             self.s.set_params(['width', w, 'height', w, \
                                    'density',self.dens, 'tol', self.tol])
             # phase diagram
             self.s.alloc()
             self.p = ['frac', self.frac]
             self.pd = phase_diag.Phase_diag()          
             self.pd.init(self.s,self.p)
             self.pd.run()
             
             # save order parameter full array             
             for j in self.range_ord_prm:
                  o = self.pd.get_ord(j)
                  dset_name =  self.dsetname_full+'_ord'+str(j)+'_wid'+str(i)
                  dset = f.create_dataset( dset_name, \
                                                data=o)             
                  dset.attrs["width"] = w        
                  dset.attrs["ord_prm_idx"] = j
                  dset.attrs["frac"]        = self.frac        
                  dset.attrs["density"]        = self.dens      
                  dset.attrs["tol"]         = self.tol
                  # extract only one order param
                  if(j==self.ord_prm_idx):
                       self.ord_prm[i] = o 
                  

         # save order parameter sub array    
         dset = f.create_dataset( self.dsetname, data=self.ord_prm)
         dset.attrs["width"]       = self.width
         dset.attrs["ord_prm_idx"] = self.ord_prm_idx
         dset.attrs["frac"]        = self.frac        
         dset.attrs["dens"]        = self.dens      
         dset.attrs["tol"]         = self.tol
         f.close()    
 
     def get_save_fname(self):
        i=1
        imax = 100
        fname = './' + self.dir_data + '/' +  self.fname_data
        full_fname = fname+'_'+str(i)+'.'+self.suffix_data
        c=os.path.exists(full_fname)
             
        while c :
             fname = './' + self.dir_data + '/' +  self.fname_data
             full_fname = fname+'_'+str(i)+'.'+self.suffix_data
             c=os.path.exists(full_fname)
             i = i+1
             if(i>imax):
                  break             
        print "Data will we saved in:", full_fname
        return full_fname


     def load(self, fname=[]):
        if (fname==[]):
             fname=self.fname_data
        full_fname = './' + self.dir_data + '/' +  fname
        
        f = h5py.File(full_fname, 'r')
        dset = f[self.dsetname] 

        for name, value in dset.attrs.iteritems():
             print name+":", value

        self.frac = dset.attrs["frac"]
        self.width = dset.attrs["width"]
        self.nwidth = len(self.width)
        self.ord_prm =  dset.value
        f.close()

     def plot(self, normalize=0):
         plt.ion()
         plt.clf()
         styl = ['-','--','-.',':']
         # loop    
         for i in range(self.nwidth):
             o = self.ord_prm[i] 
             w = int(self.width[i])
             #o =o/(self.frac *w*w) # rescaled
             scale_fact = o[0]
             if self.plot_color:
                  plt.plot(self.frac, o/scale_fact , \
                                label= str(self.width[i]), linewidth=1)
             else:
                  plt.plot(self.frac, o/scale_fact , \
                                label= str(self.width[i]), linewidth=1 ,\
                                color='k', linestyle=styl[i])

         plt.xlabel('f')
         plt.ylabel('order parameter')
         #plt.axis([self.fmin, self.fmax, 0, 0.001])
         plt.legend()


if __name__ == "__main__":

   # parse arguments
     parser = argparse.ArgumentParser()
     parser.add_argument('-infile', nargs=1)
     args=parser.parse_args()

     if(args.infile==None):
          f=finite_size_scaling()
          f.init()
          f.run_save()
          f.plot()
          raw_input("---TYPE KEYBOARD----")
     else:
          # load. Example:
          # ~/python finite_size_scaling.py -infile finite_size_scaling_1.hdf5
          f = finite_size_scaling()
          f.load(args.infile[0])
          f.plot()
          raw_input("---TYPE KEYBOARD----")
