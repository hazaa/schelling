# -*- coding: utf-8
#----------------------------------------------
# PROGRAM: phase_diagram_noise.py
# PURPOSE: compute and plot the density interface and energy of 
#          Schelling simulation when noise level is rising
# VERSION : 3 july 2012
# COMMENT : 
# AUTHOR  : Aurélien Hazan
# LANGUAGE: Python
#----------------------------------------------

import phase_diag
import schell
import numpy
import pickle
import matplotlib.pylab as plt

dir_data='data'
fname_data = 'phase_diagram_noise.data'
mypath = './' + dir_data + '/' +  fname_data

# schelling simulation  
s=schell.Schell()
s.set_params()
s.alloc()               
s.set_params(['width',30,'height',30])           
s.set_params(['density',0.9, 'tol', 0.3])   

# phase diagram
p = ['frac', numpy.linspace(0.1,0.9,num=10)]
pd = phase_diag.Phase_diag()          
pd.init(s,p)
pd.run()
pd.plot()
pd.save(mypath)
pd.free()


# load
#pd = phase_diag.Phase_diag()          
#pd.load(mypath)
#pd.plot()

# plot  
nr=3
nc=1
plt.subplot(nr,nc, 1)
plt.xlabel('')
plt.ylabel('interface density')

plt.subplot(nr,nc, 2)
plt.xlabel('')
plt.ylabel('susceptibility')

plt.subplot(nr,nc, 3)
plt.xlabel('frac')
plt.ylabel('specific heat')

