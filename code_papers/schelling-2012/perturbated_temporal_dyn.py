# -*- coding: utf-8

import schell
import numpy as np
import matplotlib.pylab as plt
import h5py
import os.path


class perturbated_temporal_dynamic():
    # compares x=f(t) in three cases: with a perturbation 
    # starting at the beginning, 
    # starting after n steps
    # and without perturbation
    
    def __init__(self):
        self.s=[]
        self.interface_density=[]
        self.stepMax=[]
        self.frac=[]
        self.tol=[]
        self.density=[]
        self.iterMax=[]
        self.trigTime=[]
        self.stepBurn=[]

    def init(self):
        self.frac=0.2
        self.tol=0.3
        self.density=0.90

        self.stepMax= 500
        self.iterMax = 10
        self.trigTime = int(np.floor(self.stepMax/2))
        self.stepBurn = 250      

        self.s=schell.Schell()
        self.s.set_params()
        self.s.set_params(['width',30,'height',30, 'density',self.density,
                           'tol', self.tol, 'iterMax',self.iterMax,
                           'trigTime', self.trigTime,
                           'stepBurn',self.stepBurn,
                           'stepMax', self.stepMax])   

        self.interface_density=np.zeros( (4,self.stepMax) )


    def run(self):
        # without perturbation, frac=0
        self.s.set_params(['frac',0.])   
        self.s.run()
        m = np.array(self.s.sch.getSegregationRecMean())
        self.interface_density[0,:] = m
        print "no perturb, frac=0: ord=",  self.s.get_ord_prm()

        # perturbation starting at the beginning, 
        self.s.set_params(['frac',self.frac, 'trigTime', 1])   
        self.s.run()
        m = np.array(self.s.sch.getSegregationRecMean())
        self.interface_density[2,:] = m
        print "perturb at begin: ord=",  self.s.get_ord_prm()
        
        # perturbation starting after n steps
        self.s.set_params(['frac',self.frac, 'trigTime', self.trigTime])   
        self.s.run()
        m = np.array(self.s.sch.getSegregationRecMean())
        self.interface_density[3,:] = m
        print "perturb half way: ord=",  self.s.get_ord_prm()

        # without perturbation, frac>0
        self.s.set_params(['frac',self.frac, 'trigTime', self.stepMax])   
        self.s.run()
        m = np.array(self.s.sch.getSegregationRecMean())
        self.interface_density[1,:] = m
        print "no perturb, frac>0: ord=",  self.s.get_ord_prm()

    def plot(self):
        plt.ion()
        plt.clf()

        t = np.arange(self.stepMax)
        plt.plot(t, self.interface_density[0,:], 'k--',label='no switching, f=0')
        plt.plot(t, self.interface_density[1,:], 
                 'k:', label='no switching,  f=%1.1f'%self.frac)

        plt.plot(t, self.interface_density[2,:], 
                 'k-.',label='switching starts at t=0')
        plt.plot(t, self.interface_density[3,:], 
                 'k', label='switching starts at t=%i'%self.trigTime)
        plt.xlabel('steps')
        plt.ylabel('ensemble average of contact density')
        plt.legend()


        tit = 'tolerance= %(mytol)2.2f  density=%(mydens)2.2f  f=%(frac)2.2f'% { "mytol": self.tol, "mydens": self.density, "frac":self.frac }
        plt.title(tit)


if __name__ == "__main__":
    p=perturbated_temporal_dynamic()
    p.init()
    p.run()
    p.plot()
    raw_input("-- Type keyboard ---")
