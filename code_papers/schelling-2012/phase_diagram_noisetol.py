# -*- coding: utf-8
#----------------------------------------------
# PROGRAM: phase_diagram_noisetol.py
# PURPOSE: compute and plot the density interface and energy of 
#          Schelling simulation when noise (fraction of perturbating
#          agents) and tolerance increase.
# VERSION : 7 sept 2012
# COMMENT : 
# AUTHOR  : Aurélien Hazan
# LANGUAGE: Python
#----------------------------------------------


import phase_diag
try:
	import schell
except:
	print "could not load simulator schell"	
import numpy as np
import matplotlib.pylab as plt
import h5py
import os.path
import argparse

class phase_diagram_noisetol():
     
     def __init__(self): 
          self.stepMax = 500
          self.iterMax = 10
          self.trigTime = 0
          self.stepBurn = 250

          self.width = 30
          self.height = 30
          
          self.frac_len = 40     # number of frac values
          self.frac_min= 0.0     # minimum value of frac
          self.frac_max= 0.5     # maximum value of frac
          self.frac=[]

          self.tol_len = 40     # number of frac values
          self.tol_min= 0.1     # minimum value of frac
          self.tol_max= 0.9     # maximum value of frac
          self.tol=[]

          self.density= 0.95          

          self.dir_data='data'
          self.fname_data = 'phase_diagram_noisetol'
          self.suffix_data = 'hdf5'
          self.hdf5_file = []

          self.s=[]   # schelling simulation
          self.pd=[]  # phase diagram object


     def init(self):            
         self.frac = np.linspace(self.frac_min, self.frac_max, 
                                  num=self.frac_len) 
         self.tol = np.linspace(self.tol_min, self.tol_max, 
                                  num=self.tol_len) 

     def run(self):                
          # create simulation object 
          self.s=schell.Schell()
          self.s.set_params()
          p_fixed = ['width', self.width, 'height', self.height,
                             'iterMax',self.iterMax,'stepMax',self.stepMax,
                             'trigTime', self.trigTime, 
                             'stepBurn', self.stepBurn,
                             'density', self.density  ]
          self.s.set_params(p_fixed)
          
         # phase diagram
          self.pd = phase_diag.Phase_diag()          
          self.pd.fname_data = self.fname_data
          self.p = ['frac', self.frac, 
                    'tol', self.tol]     
          self.pd.init(self.s,self.p, p_fixed)
          self.pd.run()
         

     def get_save_fname(self):
          i=1
          imax = 100
          fname = './' + self.dir_data + '/' +  self.fname_data
          full_fname = fname+'_'+str(i)+'.'+self.suffix_data
          c=os.path.exists(full_fname)
          
          while c :
               fname = './' + self.dir_data + '/' +  self.fname_data
               full_fname = fname+'_'+str(i)+'.'+self.suffix_data
               c=os.path.exists(full_fname)
               i = i+1
               if(i>imax):
                    break             
               print "Data will we saved in:", full_fname
               return full_fname

          
     def load(self,fname):
          self.pd = phase_diag.Phase_diag()
          self.pd.load(fname)
        
  
     def free(self):
          self.pd.free();


     def plot(self):          
         plt.ion()
         plt.clf()
         self.pd.plot_color=False
         self.pd.plot()

         


if __name__ == "__main__":

     # parse arguments
     parser = argparse.ArgumentParser()
     parser.add_argument('-infile', nargs=1)
     args=parser.parse_args()

     if(args.infile==None):
          # run simulations
          pd_noisetol = phase_diagram_noisetol()
          pd_noisetol.init()
          pd_noisetol.run()
          pd_noisetol.plot()
          raw_input("---TYPE KEYBOARD----")
          pd_noisetol.free()
     else:
          # load. Example:
          # ~/python phase_diagram_noisetol.py -infile data/phase_diagram_noisetol_1.hdf5
          pd_noisetol = phase_diagram_noisetol()
          pd_noisetol.init() 
          pd_noisetol.load(args.infile[0])
          pd_noisetol.plot()
          raw_input("---TYPE KEYBOARD----")
          pd_noisetol.pd.free()

