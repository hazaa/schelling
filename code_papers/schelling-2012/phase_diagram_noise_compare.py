# -*- coding: utf-8
#----------------------------------------------
# PROGRAM: phase_diagram_noise_compare.py
# PURPOSE: compare the density interface of Schelling simulation 
#          when f increases and switching agents are active and inactive.
#
# VERSION : 17 june 2013
# COMMENT : 
# AUTHOR  : Aurélien Hazan
#----------------------------------------------


import numpy as np
import matplotlib.pylab as plt
import h5py
import os.path
import argparse
from os.path import join

##############################
# PARAMETERS
path_= '/home/aurelien/synchro/boulot/code/papers_code/schelling-2012/data'
fname_active = 'phase_diagram_noise_2.hdf5'
fname_inactive = 'phase_diagram_noise_inactive_1.hdf5'
dset_name = 'OrderParameter0'
control_param_name='control_param frac'
##############################
# LOAD
fname_active=os.path.join(path_,fname_active)
fname_inactive=os.path.join(path_,fname_inactive)

if( not os.path.exists(fname_active)):
    raise IOError("file fname_active not found")
if( not os.path.exists(fname_inactive)):
    raise IOError("file fname_active not found")

print "open: ",fname_active
print "open: ",fname_inactive

f_act = h5py.File( fname_active, 'r')
f_inact = h5py.File( fname_inactive, 'r')

dset  = f_act[dset_name]
o_act = dset.value
#f_act = dset.attrs['control_param_frac']
f_act = dset.attrs[control_param_name]

dset    = f_inact[dset_name]
o_inact = dset.value
f_inact = dset.attrs[control_param_name]

##############################
# plot
x = np.linspace(0,0.5,10)

plt.plot(f_act, o_act, label='active')
plt.plot(f_inact, o_inact, label='inactive')
plt.plot( x,x ,'--', label='x=y'  )

plt.xlabel('f')
plt.ylabel('contact density')
plt.legend()
plt.show()
