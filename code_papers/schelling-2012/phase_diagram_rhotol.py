# -*- coding: utf-8
#----------------------------------------------
# PROGRAM: phase_diagram_rhotol.py
# PURPOSE: compute and plot the density interface and energy of 
#          Schelling simulation when rho ant tol vary
# VERSION : 10 july 2012
# COMMENT : 
# AUTHOR  : Aurélien Hazan
# LANGUAGE: Python
#----------------------------------------------

# TODO 3D: 
# *ticks: un peu d'espace ?

import phase_diag
import schell
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import axes3d
import h5py
import os.path
import argparse

DEBUG = 0

class phase_diagram_rhotol():
     
     def __init__(self): 
          self.stepMax = 500
          self.iterMax = 10
          self.trigTime = 0
          self.stepBurn = 250

          self.width = 30
          self.height = 30
          
          self.frac_len = 3     # number of frac values
          self.frac_min= 0.0     # minimum value of frac
          self.frac_max= 0.5     # maximum value of frac
          self.frac=[]

          self.dens_len = 30
          self.dens_min = 0.6
          self.dens_max = 0.95
          self.dens= []
          
          self.tol_len = 30
          self.tol_min = 0.0
          self.tol_max = 1
          self.tol = []

          self.dir_data='data'
          self.fname_data = 'phase_diagram_rhotol'
          self.suffix_data = 'hdf5'
          self.dsetname = 'OrderParameter' #dataset name in hdf5 file
          self.hdf5_file = []

          self.s=[]   # schelling simulation
          self.pd=[]  # phase diagram object

          self.font_size_3D =12
          self.plot_log = True # for suscep and specific heat
          self.plot_style='3D' # 'contourf', 'contour', '3D'
          self.nlevels = 3#30 # number of levels for contour plot

     def init(self):            
          self.frac = np.linspace(self.frac_min, self.frac_max, 
                                  num=self.frac_len) 
          self.dens = np.linspace(self.dens_min, self.dens_max, 
                                  num=self.dens_len) 
          self.tol = np.linspace(self.tol_min, self.tol_max, 
                                 num=self.tol_len) 

     def run(self):                
          # create simulation object 
          self.s=schell.Schell()
          self.s.set_params()
          self.s.set_params(['iterMax',self.iterMax,'stepMax',self.stepMax,
                             'trigTime', self.trigTime, 
                             'stepBurn', self.stepBurn])
          self.s.set_params(['width', self.width, 'height', self.height] )
          self.s.alloc()
          

         # phase diagram
          self.pd = phase_diag.Phase_diag()          
          self.pd.fname_data = self.fname_data
          self.p = ['density', self.dens, 'tol', self.tol,'frac', self.frac]    
          self.pd.init(self.s,self.p)
          self.pd.run()
         


     def get_save_fname(self):
          i=1
          imax = 100
          fname = './' + self.dir_data + '/' +  self.fname_data
          full_fname = fname+'_'+str(i)+'.'+self.suffix_data
          c=os.path.exists(full_fname)
          
          while c :
               fname = './' + self.dir_data + '/' +  self.fname_data
               full_fname = fname+'_'+str(i)+'.'+self.suffix_data
               c=os.path.exists(full_fname)
               i = i+1
               if(i>imax):
                    break             
               print "Data will we saved in:", full_fname
               return full_fname

          
     def load(self,fname):
          self.pd = phase_diag.Phase_diag()
          self.pd.load(fname)
        
  
     def free(self):
          self.pd.free();


     def plot(self):          
          """
          le diag de phase x=F(rho, tol) pour plusieurs valeurs de frac
          cf pd.plot, mais avec 3 arguments (le dernier est utilisé pour 
          faire subplot avec 3 valeurs uniquement
          """
          print "===== phase_diagram_rhotol.plot() ======= "
          plt.ion()
          plt.clf()
          n_frac_plot = min(3,self.frac_len)
          idx = np.round(np.linspace(0, self.frac_len-1 ,num = n_frac_plot))

          density = []
          tol = []
          frac = []
          
          for i_fig in range(self.pd.nb_ord_prm):
               fig=plt.figure(i_fig+1)
               levels=[]
             
               for i_frac_plot in range(n_frac_plot):
                    nr=1
                    nc = n_frac_plot
                    # suplotting is different in 2D and 3D
                    if(self.plot_style=='3D'):
                         # subplotting 3D : http://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html?highlight=plot_surface#subplotting
                         ax = fig.add_subplot(nr, nc, i_frac_plot+1, 
                                              projection='3d')
                    else:
                         plt.subplot(nr,nc, i_frac_plot+1)
                    
                    order_param = self.pd.get_ord(i_fig)
                    i_frac = idx[i_frac_plot]
                    o = order_param[:,:,i_frac]
                    # log transform
                    if((i_fig>0) & (self.plot_log)):
                         o = np.log(o)
                         
                    # get control parameter values
                    print "idx=",idx, "i_frac_plot=", i_frac_plot, \
                        "i_frac=",i_frac                    
                
                    r = range(len(self.pd.param_list)/2)
                    for k in r:
                         pname = self.pd.param_list[2*k]
                         pval = self.pd.param_list[2*k+1]                   
                         if pname=="density":
                              density=pval
                         elif pname=="tol":
                              tol=pval
                         elif pname=="frac":
                              frac=pval
                         else:
                              print "k,pname,pval=",k,pname, pval
                              raise ValueError('unknown label')
                    
                              
                    # beware : Y,X instead of X,Y, because of meshgrid
                    T,D = np.meshgrid( tol, density )
                    print "shape(D)=",np.shape(D), "shape(T)",np.shape(T),\
                        "shape(o)=", np.shape(o) 
                    
                    # see http://matplotlib.sourceforge.net/examples/
                    # pylab_examples/contour_image.html
                    #
                    # http://matplotlib.org/examples/mplot3d/contourf3d_demo2.html
                    if i_frac_plot==0:
                         levels = np.linspace(np.min(o), np.max(o), 
                                              self.nlevels)

                    if(self.plot_style=='contourf'):
                         CS=plt.contourf(T,D,o, levels,
                                         cmap=plt.cm.get_cmap('jet', 
                                                              len(levels)-1) )
                         
                         if i_frac_plot==(n_frac_plot-1):
                              plt.colorbar(CS, format="%1.1e")
                    if(self.plot_style=='contour'):
                         CS = plt.contour(T, D, o, self.nlevels, colors='k' )
                         plt.clabel(CS, fontsize=9, inline=1)
                    if(self.plot_style=='3D'):
                         ax = plt.gca(projection='3d')
                         #ax.plot_surface(T,D,o, rstride=8, cstride=8, alpha=0.3)
                         ax.plot_wireframe(T,D,o, rstride=8, cstride=8,colors='k')
                         #off = - 0.3 * (levels[-1]-levels[0])
                         #cset = ax.contour(T, D, o, zdir='z',offset=off,
                         #                  colors='k' )
                         #ax.set_xlabel('X')
                         ax.set_xlim(0, 1)
                         #ax.set_ylabel('Y')
                         #ax.set_ylim(0, 1)
                         #ax.set_zlabel('Z')
                         ax.set_zlim(levels[0],levels[-1])
                         ax.tick_params(axis='both',labelsize=self.font_size_3D)
                    else:
                         ax = plt.gca()

                    # same inversion here
                    if (self.plot_style=='3D'):
                         #plt.xlabel("tolerance",size=self.font_size_3D )
                         ax.set_xlabel("tolerance",size=self.font_size_3D
                                       ,linespacing=10)
                    else:
                         plt.xlabel("tolerance")
                         
                    if (i_frac_plot==0) | (self.plot_style=='3D'):
                         #plt.ylabel("density", size=self.font_size_3D)
                         ax.set_ylabel("density",size=self.font_size_3D
                                       ,linespacing=10)
                         ax.set_zlabel('x')
          

                    tit = 'f=%(myfrac)2.2f'% \
                        {"myfrac":frac[i_frac] }
                    plt.title(tit)


     def plot_fixed_density(self):          
          """

          """
          plt.ion()
          plt.clf()
          n_frac_plot = min(3,self.frac_len)
          idx = np.round(np.linspace(0, self.frac_len-1 ,num = n_frac_plot))

          density = []
          tol = []
          frac = []
          y_min = []
          y_max=[]

          for i_fig in range(self.pd.nb_ord_prm):
               plt.figure(i_fig+1)
               levels=[]
             
               
               for i_frac_plot in range(n_frac_plot):
                    nr=1
                    nc = n_frac_plot
                    plt.subplot(nr,nc, i_frac_plot+1)
                    order_param = self.pd.get_ord(i_fig)
                    i_frac = idx[i_frac_plot]
                    o = order_param[:,:,i_frac]
                    # log transform
                    if(i_fig>0):
                         o = np.log(o)
                         
                    # get control parameter values
                    print "idx=",idx, "i_frac_plot=", i_frac_plot, \
                        "i_frac=",i_frac                    
                
                    r = range(len(self.pd.param_list)/2)
                    for k in r:
                         pname = self.pd.param_list[2*k]
                         pval = self.pd.param_list[2*k+1]                   
                         if pname=="density":
                              density=pval
                         elif pname=="tol":
                              tol=pval
                         elif pname=="frac":
                              frac=pval
                         else:
                              print "k,pname,pval=",k,pname, pval
                              raise ValueError('unknown label')
                    # fixed density
                    idx_density = -3
                    density = density[idx_density]                    
                    plt.plot(tol,o[idx_density,:], 'k')
                    print "density=",density

                    # limits
                    if i_fig==0:
                         plt.ylim([0,1])
                    
                    if i_fig==1 :
                         if i_frac_plot==0:
                              y_min = np.min(o[idx_density,:])
                              y_max = np.max(o[idx_density,:])
                         else:
                              plt.ylim([y_min, y_max])
      

                    plt.xlabel("tolerance")
                    if i_frac_plot==0:
                         if(i_fig==0):
                              plt.ylabel("x")
                         else:
                              plt.ylabel("susceptibility")
          

                    tit = 'f=%(myfrac)2.2f'% \
                        {"myfrac":frac[i_frac] }
                    plt.title(tit)


          # tol fixé, interface en fonction; pour plusieurs frac
          #           suscept
          #           spec heat
                    
          # idem avec la densité
          #           suscept
          #           spec heat
                    
                    
if __name__ == "__main__":

     # parse arguments
     parser = argparse.ArgumentParser()
     parser.add_argument('-infile', nargs=1)
     args=parser.parse_args()

     if(args.infile==None):
          # run simulations
          pd_rhotol = phase_diagram_rhotol()
          pd_rhotol.init()
          pd_rhotol.run()
          pd_rhotol.plot()
          raw_input("---TYPE KEYBOARD----")
          pd_rhotol.free()
     else:
          # load. Example:
          # ~/python phase_diagram_rhotol.py -infile data/phase_diagram_rhotol_1.hdf5
          pd_rhotol = phase_diagram_rhotol()
          pd_rhotol.init() 
          pd_rhotol.load(args.infile[0])
          pd_rhotol.plot()
          #pd_rhotol.plot_fixed_density()
          raw_input("---TYPE KEYBOARD----")
          pd_rhotol.free()

