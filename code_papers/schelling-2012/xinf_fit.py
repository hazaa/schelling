# -*- coding: utf-8
#----------------------------------------------
# PROGRAM: xinf_fit.py
# PURPOSE: 
#
# VERSION : 24 sept 2012
# COMMENT : 
# AUTHOR  : Aurélien Hazan
# LANGUAGE: Python
#----------------------------------------------


import phase_diag
import schell
import numpy as np
#from scipy.optimize import curve_fit
from scipy.optimize import leastsq, fmin
import matplotlib.pylab as plt
import scipy.stats as stats


def  get_logstep( stepMax, n_step):
    stop = np.log(stepMax) / np.log(10)
    step = np.floor( np.logspace(1, stop , n_step ))
    step = step.astype(int)
    return step[step<stepMax]




def f_rm(t, alpha, beta ):
    """
    Analytic expression of interface density in the Rogers/McKane 
    model, eq.(8)
    """
    if( not(np.isscalar(alpha)) | (not(np.isscalar(beta))) ):
        raise ValueError("alpha and beta must be scalar")

    p = np.sqrt(alpha * beta)
    ta = np.tanh( t* p )
    num = p + alpha * ta
    denom = 2 * p + (alpha + beta)* ta
    x_t = num / denom

    return x_t





def residuals_rm(p, y, x):
    """
    residuals for RM fit
    """
    alpha,beta = p
    err = y- f_rm(x, alpha, beta ) 
    return err
    
    



class xinf_fit():
    """
    tol fixed such that segregation appears,  since we study the 
    segregating regime
    """
    def __init__(self): 
        self.step = [] # time
        
        self.stepMax = 20000
        self.iterMax = 3
        self.trigTime = 0
        self.stepBurn = 2500
        
        self.width = 30
        self.height = 30

        self.frac_len = 20     # number of frac values
        self.frac_min= 0.8     # minimum value of frac
        self.frac_max= 0.95     # maximum value of frac
        self.frac=[]

        self.tol=0.3
        self.density= 0.9     
        
        self.s=[]   # schelling simulation
        self.pd=[]  # phase diagram object

        # plotting constants
        self.ymin=-0.01 
        self.ymax=0.6
        
        # results
        self.p_fit_rm=[]
        self.beta = []
        self.intercept = []
        
    def fit_rm(self, frac):
         """
         """
         self.frac = frac
         self.interface_density=np.zeros( (1,self.stepMax) )

         # create simulation object 
         self.s=schell.Schell()
         self.s.set_params()
         self.s.set_params(['iterMax',self.iterMax,'stepMax',self.stepMax,
                            'trigTime', self.trigTime, 
                            'stepBurn', self.stepBurn,
                            'density', self.density,
                            'tol', self.tol,
                            'frac', frac
                            ])
         self.s.set_params(['width', self.width, 'height', self.height] )
         self.s.alloc()
         # run simulation
         self.s.run()
         self.iface_density = np.array(self.s.sch.getSegregationRecMean())
         # fit 
         n_step = 100
         self.step = get_logstep( self.stepMax, n_step)
         p0 = [0.1, 0.1]
         """self.p_fit_rm,pcov=curve_fit( f_rm,
                                       self.step,
                                       self.iface_density[self.step])
                  
         self.p_fit_rm = leastsq(residuals_rm, p0, 
                                 args=(self.iface_density[self.step],
                                       self.step))
                                       """
         # error function to minimize
         fp = lambda c, x: (np.sqrt(c[0]* c[1]) + c[0] * np.tanh( x* np.sqrt(c[0]* c[1]) ))/( 2 * np.sqrt(c[0]* c[1]) + (c[0]+c[1])* np.tanh( x* np.sqrt(c[0]* c[1]) ))

         e = lambda p, x, y: (abs((fp(p,x)-y))).sum()
         self.p_fit_rm = fmin(e, p0, args=(self.step,
                                         self.iface_density[self.step]))
         print "self.fit_rm=",self.p_fit_rm
 
         
    def fit_beta(self):
        """
        find best fit for beta in RM-like model of x at long times
        when frac>0 
        Regression: xsim =  1 - beta *  (1-f)
        NB: fixed density and tolerance
        
        """
        # load xsim = f( frac)
        fname = 'data/phase_diagram_noise_1.hdf5'
        self.pd = phase_diag.Phase_diag()
        self.pd.load(fname)
        idx_iface_dens = 0
        self.iface_density = self.pd.get_ord(idx_iface_dens)
        self.frac = self.pd.param_list[1]
        
        # select indices such that frac >0.7
        idx = self.frac > 0.7
        self.frac = self.frac[idx]
        self.iface_density = self.iface_density[idx]

        # find best fit for beta in xsim =  1 - beta *  (1-f)
        beta0 = [-1.0]
        slope,intercept,r_value,p_value,std_err=stats.linregress(self.frac, 
                                                           self.iface_density)
        self.intercept = intercept
        self.beta_hat = slope
    
        print "x=", self.frac
        print "y=",self.iface_density
        

    def plot_rm(self, fig=1):
        """
        """
        plt.figure(fig)
        """
        alpha= self.p_fit_rm[0][0]
        beta = self.p_fit_rm[0][1]
        """
        alpha= self.p_fit_rm[0]
        beta = self.p_fit_rm[1]
         # plt fit_rm f=0, with fitted alpha,beta
        n_step = 300      
        step = get_logstep( self.stepMax, n_step)
        iface_fit = f_rm( step, alpha, beta)
                

        plt.semilogx(step, iface_fit, 
                     label='fit RM (alpha=%.1e, beta=%.1e) '%(alpha,beta) )
        plt.semilogx(self.step, self.iface_density[self.step], 'o',
                     label = 'simulation (f=%.2f)' %self.frac)
        plt.legend()
        plt.xlabel("step")
        plt.ylabel("interface density")
        plt.title('Fit RM /simulation ')
        plt.ylim([self.ymin, self.ymax])

        
    def plot_beta(self, fig=1):
        """
        """
        plt.figure(fig)
        n_step = 10      
        m = np.min(self.frac)
        M = np.max(self.frac)
        frac = np.linspace( m, M, n_step)
        
        iface_fit = self.intercept + self.beta_hat *frac
        
        plt.plot(frac, iface_fit, '-+',
                     label='fit (a=%.1e, beta=%.1e) '%(self.intercept,
                                                  self.beta_hat) )
        plt.plot(self.frac, self.iface_density, 'o',
                     label = 'simulation ')
        plt.legend()
        plt.xlabel("frac")
        plt.ylabel("interface density")
        plt.title('Fit "a + beta* f"  vs simulation ')
        plt.xlim((m,M))



    def free(self):
        self.pd.free();
        
        
if __name__ == "__main__":
    
    x = xinf_fit();
    
    # ########### FIT RM ###############
    # unmodified
    frac = 0.2
    
    x.fit_rm(frac)
    x.plot_rm(1)
    # modified
    """
    frac = 0.1
    x.fit_rm(frac)
    x.plot_rm(2)
    frac = 0.2
    x.fit_rm(frac)
    x.plot_rm(3)
    """
    # fit beta
    """
    x.fit_beta()
    x.plot_beta(1)
    """
    plt.show()
