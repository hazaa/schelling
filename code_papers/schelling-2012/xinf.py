 # -*- coding: utf-8

import phase_diag
import schell
import numpy
import pickle
import matplotlib.pylab as plt
#----------------------------------------------
class xinf():
    # computes $x_{\infty}(f)$  near 1, and compares to analytic expression 
    def __init__(self):
        self.s=[]   # schelling simulation
        self.pd=[]  # phase diagram object
        self.dir_data='data'
        self.fname_data = 'make_plots_xinf.data'
        self.ord_prm_idx=0 # index of the order param we're interested in
        self.ntol = 8    # number of tolerance values
        self.nfrac = 3   # number of frac values
        self.frac = [] 
        self.tol = []
        self.beta =[]
        self.ord=[]      # where we store the phase diagram data
        self.theoretical = [] # theoretical model from Rogers/McKane

    def init(self):    
        self.frac = numpy.linspace(0.85,0.95,num=self.nfrac)
        self.tol = numpy.linspace(0.1,0.9,num=self.ntol) 

    def run(self):
        # compute phase diagram
        self.s=schell.Schell()
        self.s.set_params()
        self.s.set_params(['width',30, 'height', 30,'density',0.9])   
        self.s.alloc()
        self.p = ['frac', self.frac, \
             'tol' , self.tol  ]
        self.pd = phase_diag.Phase_diag()          
        self.pd.init(self.s,self.p)
        self.pd.run()
        self.ord = self.pd.get_ord(self.ord_prm_idx)
        # compute theoretical model
        # x_inf ~ 1- beta*(1-f)   ; beta = 2*(1-t)
        # see CR-13042012.pdf              
        self.theoretical = numpy.zeros( (self.nfrac,self.ntol) ); 
        self.beta = 2.0*(1.0-self.tol)
        for i in range(self.nfrac):
            for j in range(self.ntol):
                self.theoretical[i,j] = 1- self.beta[j]*(1-self.frac[i])


    def save(self):
        mypath = './' + self.dir_data + '/' +  self.fname_data
        f = open( mypath, 'w')
        pickle.dump(self.ord,f)
        pickle.dump(self.frac,f)
        pickle.dump(self.beta,f)
        pickle.dump(self.theoretical,f)
        f.close()

    def load(self):
        mypath = './' + self.dir_data + '/' +  self.fname_data
        f = open(mypath, 'r')
        self.ord = pickle.load(f)
        self.frac = pickle.load(f)
        self.beta= pickle.load(f)
        self.theoretical= pickle.load(f)
        f.close()


    def plot(self):
        plt.ion()
        plt.clf()
        if(0):
            nr = 1
            nc = 2
        # plot 1: simulation
            plt.subplot(nr,nc, 1)
        #B,F = numpy.meshgrid(self.beta, self.frac )
            F,TOL = numpy.meshgrid( self.frac, self.tol )
            cp = plt.contourf( F, TOL, numpy.transpose(self.ord))
            cbar = plt.colorbar(cp)
            plt.xlabel('frac')
            plt.ylabel('tol')
            plt.title('x_inf simulation')

        # plot 2 : theoretical
            #plt.subplot(nr,nc, 2)
        F,B = numpy.meshgrid(self.frac,self.beta )
        cp = plt.contourf( F, B, numpy.transpose(self.theoretical))
        plt.xlabel('frac')
        plt.ylabel('beta')
        cbar = plt.colorbar(cp)
        
        plt.title('x_inf theor = 1-beta*(1-frac)')



#----------------------------------------------
if __name__ == "__main__":
    # full run
    x=xinf()
    x.init()
    x.run()
    x.save()
    x.plot()
    

# load: if you've run the simulations already, you can just load:
#
#import xinf
#x = xinf.xinf()  
#x.init()       
#x.load()
#x.plot()
