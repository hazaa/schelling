# -*- coding: utf-8
#----------------------------------------------
# PROGRAM: phase_diagram_noise.py
# PURPOSE: compute and plot the density interface and energy of 
#          Schelling simulation when noise (fraction of perturbating
#          agents) increases.
# VERSION : 13 july 2012
# COMMENT : 
# AUTHOR  : Aurélien Hazan
# LANGUAGE: Python
#----------------------------------------------


import phase_diag
import schell
import numpy as np
import matplotlib.pylab as plt
import h5py
import os.path
import argparse


DEBUG = 0

class phase_diagram_noise():
     
     def __init__(self): 
          self.stepMax = 500
          self.iterMax = 50
          self.trigTime = 0
          self.stepBurn = 250

          self.width = 30
          self.height = 30
          
          self.frac_len = 50     # number of frac values
          self.frac_min= 0.0     # minimum value of frac
          self.frac_max= 0.8     # maximum value of frac
          self.frac=[]

          self.density= 0.95
          self.tol = 0.3

          self.dir_data='data'
          self.fname_data = 'phase_diagram_noise'
          self.suffix_data = 'hdf5'
          self.dsetname = 'OrderParameter' #dataset name in hdf5 file
          self.hdf5_file = []

          self.s=[]   # schelling simulation
          self.pd=[]  # phase diagram object

          self.nlevels = 8

     def init(self):            
          self.frac = np.linspace(self.frac_min, self.frac_max, 
                                  num=self.frac_len) 

     def run(self):                
          # create simulation object 
          self.s=schell.Schell()
          self.s.set_params()
          self.s.set_params(['iterMax',self.iterMax,'stepMax',self.stepMax,
                             'trigTime', self.trigTime, 
                             'stepBurn', self.stepBurn,
                             'density', self.density,
                             'tol', self.tol
                             ])
          self.s.set_params(['width', self.width, 'height', self.height] )
          self.s.alloc()
          

         # phase diagram
          self.pd = phase_diag.Phase_diag()          
          self.pd.fname_data = self.fname_data
          self.p = ['frac', self.frac]     
          self.pd.init(self.s,self.p)
          self.pd.run()
         


     def get_save_fname(self):
          i=1
          imax = 100
          fname = './' + self.dir_data + '/' +  self.fname_data
          full_fname = fname+'_'+str(i)+'.'+self.suffix_data
          c=os.path.exists(full_fname)
          
          while c :
               fname = './' + self.dir_data + '/' +  self.fname_data
               full_fname = fname+'_'+str(i)+'.'+self.suffix_data
               c=os.path.exists(full_fname)
               i = i+1
               if(i>imax):
                    break             
               print "Data will we saved in:", full_fname
               return full_fname

          
     def load(self,fname):
          self.pd = phase_diag.Phase_diag()
          self.pd.load(fname)
        
  
     def free(self):
          self.pd.free();


     def plot(self):          
          # le diag de phase x=F(rho, tol) pour plusieurs valeurs de frac
          # cf pd.plot, mais avec 3 arguments (le dernier est utilisé pour 
          # faire subplot avec 3 valeurs uniquement
          plt.ion()
          plt.clf()       
          self.pd.plot()
          
                    
                    
if __name__ == "__main__":

     # parse arguments
     parser = argparse.ArgumentParser()
     parser.add_argument('-infile', nargs=1)
     args=parser.parse_args()

     if(args.infile==None):
          # run simulations
          pd_noise = phase_diagram_noise()
          pd_noise.init()
          pd_noise.run()
          pd_noise.plot()
          raw_input("---TYPE KEYBOARD----")
          pd_noise.free()
     else:
          # load. Example:
          # ~/python phase_diagram_noise.py -infile data/phase_diagram_noise_1.hdf5
          pd_noise = phase_diagram_noise()
          pd_noise.init() 
          pd_noise.load(args.infile[0])
          pd_noise.plot()
          raw_input("---TYPE KEYBOARD----")
          pd_noise.pd.free()

