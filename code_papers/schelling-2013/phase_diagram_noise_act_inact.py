# -*- coding: utf-8
#----------------------------------------------
# PROGRAM: phase_diagram_noise_act_inact.py
# PURPOSE: same as phase_diagram_noise.py, 
#          with trigTime taking value 0 and stepMax to 
#          compare active and inactive switching agents
# VERSION : october 2013
# COMMENT : 
# AUTHOR  : Aurélien Hazan, www.aurelienhazan.eu
# LANGUAGE: Python
#----------------------------------------------


import phase_diag
import schell
import numpy as np
import matplotlib.pylab as plt
import h5py
import os.path
import argparse


DEBUG = 0

class phase_diagram_noise():
     
     def __init__(self): 
          self.stepMax = 500
          self.iterMax = 10
          self.trigTime = [] # see init() 
          self.stepBurn = 250

          self.width = 30
          self.height = 30
          
          self.frac_len = 3     # number of frac values
          self.frac_min= 0.0     # minimum value of frac
          self.frac_max= 0.9     # maximum value of frac
          self.frac=[]

          self.density= 0.95
          self.tol = 0.3
          self.p_h= 0.0001 # uMoveIfHappy
          self.p_u= 0.05    # uMoveIfUnhappy  0.2
          self.p_s= 0.5   # pChange          0.05

          self.dir_data='data'
          self.fname_data = 'phase_diagram_noise_actinact'
          self.suffix_data = 'hdf5'
          self.dsetname = 'OrderParameter' #dataset name in hdf5 file
          self.hdf5_file = []
          self.description = [] # plain text description of the experiment

          self.s=[]   # schelling simulation
          self.pd=[]  # phase diagram object
          self.snapshot_length = self.stepMax # recorded trajectory
          
          self.color_plot=True
          self.nlevels = 8

     def init(self):            
          self.frac = np.linspace(self.frac_min, self.frac_max, 
                                  num=self.frac_len) 
          self.trigTime = np.array([0,self.stepMax]) 

     def run(self):                
          # create simulation object 
          self.s=schell.Schell()
          self.s.set_params()
          self.s.set_params(['iterMax',self.iterMax,'stepMax',self.stepMax,
                             'stepBurn', self.stepBurn,
                             'density', self.density,
                             'tol', self.tol,         
                             'uMoveIfHappy', self.p_h,  
                             'uMoveIfUnhappy', self.p_u,
                             'pChange', self.p_s
                             ])
          # width and height must be kept separately
          self.s.set_params(['width', self.width, 'height', self.height] )
          self.s.alloc()
          

         # phase diagram
          self.pd = phase_diag.Phase_diag()   
          # set parameters of phase diagram
          self.pd.snapshot_length = self.snapshot_length 
          self.pd.fname_data = self.fname_data
          self.pd.description = self.description
          self.p = ['frac', self.frac, 'trigTime', self.trigTime]     
          self.pd.init(self.s,self.p)
          self.pd.run()
         


     def get_save_fname(self):
          i=1
          imax = 100
          fname = './' + self.dir_data + '/' +  self.fname_data
          full_fname = fname+'_'+str(i)+'.'+self.suffix_data
          c=os.path.exists(full_fname)
          
          while c :
               fname = './' + self.dir_data + '/' +  self.fname_data
               full_fname = fname+'_'+str(i)+'.'+self.suffix_data
               c=os.path.exists(full_fname)
               i = i+1
               if(i>imax):
                    break             
               print "Data will we saved in:", full_fname
               return full_fname

          
     def load(self,fname):
          # load Phase_diag()
          self.pd = phase_diag.Phase_diag()
          self.pd.load(fname)
          # load the other simulation parameters
          # TODO: DO THAT AUTOMATICALLY IN PHASE_DIAG
          self.hdf5_file = h5py.File( fname, 'r')
          subgroup = self.hdf5_file["SimulationParams"]
          self.height = subgroup.attrs["height"]
          self.width = subgroup.attrs["width"]
          self.density = subgroup.attrs["density"]
          self.tol = subgroup.attrs["tol"]
          self.p_s = subgroup.attrs["pChange"]
          self.p_u = subgroup.attrs["uMoveIfUnhappy"]
          self.p_h = subgroup.attrs["uMoveIfHappy"]
          self.description = self.hdf5_file.attrs["description"]
          self.hdf5_file.close()

     def free(self):
          self.pd.free();


     def plot(self):          
          # le diag de phase x=F(rho, tol) pour plusieurs valeurs de frac
          # cf pd.plot, mais avec 3 arguments (le dernier est utilisé pour 
          # faire subplot avec 3 valeurs uniquement
          plt.ion()
          plt.clf()       
          #self.pd.plot()
          for i in range(self.pd.nb_ctrl_prm):
               if self.pd.param_list[2*i]=='frac':
                    ctrl_param_name = self.pd.param_list[2*i]# !!!!!TODO!!!!!!!
                    ctrl_param_val = self.pd.param_list[2*i+1]
                    
          order_param = self.pd.get_ord(0)
          a = order_param[:,0]
          b = order_param[:,1]
          print self.pd.param_list
          print a,b,a.size, b.size,           ctrl_param_val #size(order_param[:,0]), size(ctrl_param,val)
          plt.plot(ctrl_param_val, order_param[:,0], 'k',label='active')
          plt.plot( ctrl_param_val, order_param[:,1], 'k--', label='inactive' )
          plt.xlabel('f')
          plt.ylabel('xinf')
          plt.legend(loc=2)
          ##########
          # parametric plot frac -> (inactive, active-inactive  )
          plt.figure()
          delta=order_param[:,0] - order_param[:,1]
          plt.plot(order_param[:,1], delta, '-',
                   marker='o' )
          plt.xlabel('structure (=x_inf^inact)')
          plt.ylabel('feu rouge (=x_inf^act-x_inf^inact)')
          x_0 = order_param[0,1]
          y_0 = delta[0]
          plt.plot([x_0 , 0.5], [y_0 , y_0+(0.5-x_0)],'--',color='red')
          ax=plt.gca()
          for i in range(0,ctrl_param_val.size,3):
               x = order_param[i,1]
               y=  delta[i]
               s = 'f=%1.2f'%(ctrl_param_val[i])
               t = ax.text( x, y, s)


          ###########
          # final state
          plt.figure()
          #plt.suptitle("tolerance=%1.1f, density=%1.1f, p_s=%1.1f, p_u=%1.1f,p_h=%f "%(self.tol,self.density, self.p_s, self.p_u, self.p_h), size=18)
          plt.suptitle("tolerance=%f, density=%f, p_s=%f \n p_u=%f,p_h=%f "%(self.tol,self.density, self.p_s, self.p_u, self.p_h), size=18)

          width= self.width 
          height= self.height
          imin=0
          imax = ctrl_param_val.size-1
          # f min
          state = self.pd.get_finstate((imin,0)) # active
          state = state.reshape((self.width,self.height) )
          ax = plt.subplot(2,2,1) 
          if(self.color_plot):
               plt.matshow(state, fignum=False)
          else:
               matshow_bw(state,vmin=1,vmax=2,ax=ax)
          plt.xlabel('active')
          plt.ylabel('f='+str(ctrl_param_val[imin]) )
     
          state = self.pd.get_finstate((imin,1)) # inactive
          state = state.reshape((self.width,self.height))
          ax = plt.subplot(2,2,2) 
          if(self.color_plot):
               plt.matshow(state, fignum=False)
          else:
               matshow_bw(state,vmin=1,vmax=2,ax=ax)
          plt.xlabel('inactive')

          # f max
          state = self.pd.get_finstate((imax,0)) # active
          state = state.reshape((self.width,self.height) )
          ax = plt.subplot(2,2,3) 
          if(self.color_plot):
               plt.matshow(state, fignum=False)
          else:
               matshow_bw(state,vmin=1,vmax=2,ax=ax)
          plt.xlabel('active')
          plt.ylabel('f='+str(ctrl_param_val[imax]) )
     
          state = self.pd.get_finstate((imax,1)) # inactive
          state = state.reshape((self.width,self.height))
          ax = plt.subplot(2,2,4) 
          if(self.color_plot):
               plt.matshow(state, fignum=False)
          else:
               matshow_bw(state,vmin=1,vmax=2,ax=ax)
          plt.xlabel('inactive')

          
          
if __name__ == "__main__":

     # parse arguments
     parser = argparse.ArgumentParser()
     parser.add_argument('-infile', nargs=1)
     args=parser.parse_args()

     if(args.infile==None):
          # run simulations
          description = raw_input("Type a description of the experiment: ")
          pd_noise = phase_diagram_noise()
          pd_noise.init()
          pd_noise.run()
          #pd_noise.plot()
          pd_noise.free()
     else:
          # load. Example:
          # ~/python phase_diagram_noise_act_inact.py -infile data/phase_diagram_noise_actinact_1.hdf5
          pd_noise = phase_diagram_noise()
          pd_noise.init() 
          pd_noise.load(args.infile[0])
          print "DESCRIPTION OF THE EXPERIMENT:\n", pd_noise.description, "\n"
          pd_noise.plot()
          
          raw_input("---TYPE KEYBOARD----")
          pd_noise.pd.free()

