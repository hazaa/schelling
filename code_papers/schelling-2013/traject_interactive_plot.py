# -*- coding: utf-8
#----------------------------------------------
# PROGRAM: traject_interactive.py
# PURPOSE: plot interactively time-dependent trajectories
# VERSION : october 2013
# COMMENT : 
# AUTHOR  : Aurélien Hazan, www.aurelienhazan.eu
# LANGUAGE: Python
#----------------------------------------------
from matplotlib.widgets import Slider
import phase_diag
#import schell
import numpy as np
import matplotlib.pylab as plt
import h5py
import os.path
import argparse

class traject_plot():
    def __init__(self): 
        self.pd = phase_diag.Phase_diag()    # phase diagram
        self.ord_param=[]
        self.nb_ctrl_prm = []
    
    def load(self,fname):
        print "Opening: ", fname
        self.pd.load(fname)
        self.dsetname_ctrl = self.pd.dsetname_ctrl
        self.nb_ctrl_prm= len(self.pd.param_list)/2
        
        dset_name =  self.dsetname_ctrl+str(0)
        self.dset_frac = self.pd.hdf5_file[dset_name]
        self.dset_frac=self.dset_frac[:,0]
        dset_name =  self.dsetname_ctrl+str(1)
        self.dset_trigTime = self.pd.hdf5_file[dset_name]


    def free(self):
        self.pd.free()
    
    def plot(self):
        f = plt.figure()
        ax = plt.gca()
        #fig, ax = plt.subplots() 
        step=10
        print 'dset_frac_size=',self.dset_frac.size
        for i in range(0,self.dset_frac.size,step):
            t1 = self.pd.get_trajectory((i,0))
            t2 = self.pd.get_trajectory((i,1))
            x = range(t1.size)
            l1=str('%1.1f'%(self.dset_frac[i]))+' act'
            l2=str('%1.1f'%(self.dset_frac[i]))+' inact'
            if(i%3)==0: c = 'b'
            if(i%3)==1: c = 'r'
            if(i%3)==2: c = 'y'
            ax.plot( x, t1, '-', color=c, label=l1)
            ax.plot( x, t2, '--', color=c, label=l2)

        plt.ylabel('x')
        plt.xlabel('time')
        #plt.legend()
        #legend = ax.legend()
        ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.show()

    def plot_interactive(self):   
        # widgets: http://nbviewer.ipython.org/urls/raw.github.com/jakevdp/matplotlib_pydata2013/master/notebooks/03_Widgets.ipynb

       
        print "dset_frac=",self.dset_frac
        
        t1 = self.pd.get_trajectory((0,0))
        t2 = self.pd.get_trajectory((0,1))

        fig, ax = plt.subplots()
        fig.subplots_adjust(bottom=0.2, left=0.1)
        
        line1, = plt.plot(range(t1.size), t1, lw=2)
        line2, = plt.plot(range(t2.size), t2, '--',lw=2 )

        def on_change(val):
            i = int(val)
            line1.set_ydata( self.pd.get_trajectory((i,0)) )
            line2.set_ydata( self.pd.get_trajectory((i,1)) )
            plt.title('frac='+str(self.dset_frac[i]))

        slider_ax = plt.axes([0.1, 0.1, 0.8, 0.02])
        slider = Slider(slider_ax, "frac index", 0, 
                        self.dset_frac.size-1, valinit=0, 
                        color='#AAAAAA')
        slider.on_changed(on_change)
        plt.show()

        

if __name__ == "__main__":

     # parse arguments
     parser = argparse.ArgumentParser()
     parser.add_argument('-infile', nargs=1)
     args=parser.parse_args()

     if(args.infile==None):
         print("Usage: \n $python traject_interactive_plot -infile filename.h5")
     else:
         t = traject_plot()
         t.load(args.infile[0])
         #t.plot()
         t.plot_interactive()
