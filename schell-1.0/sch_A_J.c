#include <stdlib.h>
#include <stdio.h>
#include<math.h>
#include<iostream>
#include<fstream>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/normal_distribution.hpp>
#include <vector>
#include <algorithm>
#include <numeric>
//#include<SDL/SDL.h>
//#include <SDL/SDL_image.h>
#include "boost/multi_array.hpp"

#ifdef WIN32
  #pragma comment(lib, "SDLmain.lib")
  #pragma comment(lib, "SDL.lib")
#endif
int it=100000;
//#define N 100
//#define q 3
using namespace std;
int N;
int q,nomove;
int Cv;
int voisin;
double nonvoisin;

int gt=5;
int E1,Ef,Ei;
double ID;

int cpt;
double x,y,M,Occ,agt_switch;
double w;
double X, p_st,p_u,p_h,p_test;
int i,j,l,p,k;
int u;
int m,n,mf,nf,agt,state_agt;
int Ni,Nv,Nd;
double p_f;
int nl[8];
int LDNS,ct;
double lab;


double ms,ms2,ms3,m1,m2,m3;
char filea[50];

typedef boost::rand48 base_generator_type;
base_generator_type generator(gt);
boost::uniform_real<> uni_dist(0,1);
boost::variate_generator<base_generator_type&,boost::uniform_real<> >uni(generator,uni_dist);


typedef boost::multi_array<int, 1> one_i_array_type;
typedef boost::multi_array<double, 2> two_d_array_type;
typedef boost::multi_array<int, 2> two_i_array_type;

ofstream meanszNPNL("IDmeansizeNPNLabc.txt");
ofstream TAILLEFRNPNL("IDTAILLEFRNPNLabc.txt");
ofstream TAILLEFR2NPNL("IDTAILLEFR2NPNLabc.txt");
ofstream TAILLEFR3NPNL("IDTAILLEFR3NPNLabc.txt");



//SDL_Surface *screen;
const char *NPNLCv200X5a;
const char *NPNLCv200X5b;
const char *NPNLCv200X5c;




void putpixel(int x, int y, int color)
{
  /*  unsigned int *ptr = (unsigned int*)screen->pixels;
  int lineoffset = y * (screen->pitch / 4);
  ptr[lineoffset + x] = color;*/  //AUREL
}


struct couple {int a;int b;};
struct sgl {double e;};

int main(int argc, char *argv[])
{
  
  N=atoi(argv[1]);          // [AH] taille de la grille  
  X=atof(argv[2]);          // [AH] tolérance
  Occ=atof(argv[3]);        // [AH] densité d'occupation
  agt_switch=atof(argv[4]); // [AH] fraction d'agents switch
  p_u=atof(argv[5]);        // [AH] proba de saut si unhappy
  p_f=atof(argv[6]);        // [AH] proba de switch des agents switch (paramétré en dur)  
  
  int P=50*N*N+1000*it;
  std::vector<int> lb;
  std::vector<int> cluster;
  std::vector<int> cluster2;
  std::vector<int> cluster3;
  std::vector<double> fcluster;
  std::vector<double> fcluster2;
  std::vector<double> fcluster3;
  std::vector<double> meansizecluster;
  std::vector<double> meansizecluster2;
  std::vector<double> meansizecluster3;
  std::vector< int > liste_value;
  std::vector< couple> liste_position;
  std::vector< couple> liste_sp;
  std::vector<couple> neighbour;
  couple c;
  two_i_array_type S(boost::extents[N+2][N+2]);
  two_i_array_type category(boost::extents[(N+2)*(N+2)][q]);
  two_i_array_type label(boost::extents[(N+2)*(N+2)][q]);
  two_d_array_type nbvoisins(boost::extents[(N+2)*(N+2)][q]);
  one_i_array_type nb(boost::extents[q]);
  one_i_array_type indexe(boost::extents[q]);
  
  
  
  
  //screen = SDL_SetVideoMode(100, 100, 32, SDL_SWSURFACE);
  std::fill( S.data(), S.data() + S.num_elements(), 0 );
  
  
   /*AGENTS SWITCH: remplissage par cases noires et blanches*/
  for(i=1;i<N*N*agt_switch+1;i++)      /*remplissage du réseau par des cases noires et blanches*/          
    
    
    
    {
      do	{x=(rand()+1.)/((double)RAND_MAX+1.);				 
	m=ceil(N*x);	
	y=(rand()+1.)/((double)RAND_MAX+1.);
	n=ceil(N*y);
      }
      while(S[m][n]!=0);
      if(uni()<0.5) 
	{S[m][n]=-1;}
      else
	{S[m][n]=1;}
      c.a=m;
      c.b=n;
      liste_position.push_back(c);
      liste_value.push_back(2);
    }

 /*AGENTS NON-SWITCH: remplissage par cases noires et blanches*/
  cout<<Occ<<endl;
  cout<<N*N*Occ/2<<N*N*Occ<<" "<<Occ<<endl;
  cout<<agt_switch<<" "<<X<<" "<<Occ<<endl;
  for(i=1;i<N*N*Occ/2+1;i++)      /*remplissage du réseau par des cases noires et blanches*/          
    
    
    
    {do	{x=(rand()+1.)/((double)RAND_MAX+1.);				/*remplissage par des cases noires*/
	m=ceil(N*x);	
	y=(rand()+1.)/((double)RAND_MAX+1.);
	n=ceil(N*y);
      }
      while(S[m][n]!=0);
      
      S[m][n]=1;
      c.a=m;
      c.b=n;
      liste_position.push_back(c);
      liste_value.push_back(1);
      
      do	{x=(rand()+1.)/((double)RAND_MAX+1.);				  /*remplissage par des cases blanches*/
	m=ceil(N*x);	
	y=(rand()+1.)/((double)RAND_MAX+1.);
	n=ceil(N*y);
      }
      while(S[m][n]!=0);
      
      S[m][n]=-1;
      c.a=m;
      c.b=n;
      liste_position.push_back(c);
      liste_value.push_back(-1);
    }
  
  
  
  
  
  for(i=1;i<N+1;i++)               
    for(j=1;j<N+1;j++)     
      
      {{
	  if(S[i][j]==0) {c.a=i; c.b=j; liste_sp.push_back(c);}
	  
	}}
  cout<<"size="<<liste_position.size()<<endl;
  
  boost::uniform_int<> unilist_dist(1,liste_position.size());
  boost::variate_generator<base_generator_type&,boost::uniform_int<> >unilist(generator,unilist_dist);
  boost::uniform_int<> unilistsp_dist(1,liste_sp.size());
  boost::variate_generator<base_generator_type&,boost::uniform_int<> >unilistsp(generator,unilistsp_dist);
  
  for(j=0;j<N+2;j++)								/*conditions aux limites */
    {
      S[0][j]=2;
      S[N+1][j]=2;
      S[j][N+1]=2;
      S[j][0]=2;
    }
  int ct_im=0;
  int val;
  cout<<X<<endl;
  for(p=0;p<P;p++)							/*départ de la boucle d'iteration*/
    { //printf("%d\n ",p);
      
      if(nomove>N*N) {cout<<"break"<<endl;break;}
      if(p%50000==0 || (p<50000 &&p %10000==0)) { //cout<<"nomove="<<nomove<<endl;
	ct_im++;
	
	for(i=1;i<N+1;i++)
	  {for(j=1;j<N+1;j++)
	      {if(S[i][j]==1) putpixel(i,j,0xff0000);
		if(S[i][j]==-1) putpixel(i,j,0x0000ff);
		if(S[i][j]==0) putpixel(i,j,0xffffff);
	      }
	  }
	
	/*SDL_UpdateRect(screen, 0, 0, 100, 100);   //AUREL
	SDL_Event event;
	while (SDL_PollEvent(&event)) 
	  {switch (event.type) 
	      {case SDL_KEYDOWN:
		  break;
	      case SDL_KEYUP:
		if (event.key.keysym.sym == SDLK_ESCAPE)
		  return 0;
		break;
	      case SDL_QUIT:
		return(0);
		
	      }
	    
	      }*/
	val=ct_im+1000;
	sprintf(filea,"%d.bmp",val);
	//SDL_SaveBMP(screen, filea);  //AUREL
      }
      /*do{
	x=(rand()+1.)/((double)RAND_MAX+1.);				
	m=ceil(N*x);	
	y=(rand()+1.)/((double)RAND_MAX+1.); 
	n=ceil(N*y);
	}
	while(S[m][n]==0);*/
      agt=unilist();
      
      m=liste_position[agt-1].a;
      n=liste_position[agt-1].b;
      
      
      /*calcul du nombre de voisins*/
      Ni=0;
      Nv=0;
      state_agt=liste_value[agt-1];
      cpt=-1;
      
      ID=0.;
      if(state_agt!=2) /* AGENTS NON SWITCH */
	{for(k=0;k<2;k++)
	    {if(S[m-1][n+k]==0) {Nv++;}
	      if(S[m+k][n+1]==0) {Nv++;}
	      if(S[m-k][n-1]==0) {Nv++;}
	      if(S[m+1][n-k]==0) {Nv++;}
	      if(S[m-1][n+k]==S[m][n]) {Ni++;}
	      if(S[m+k][n+1]==S[m][n]) {Ni++;}
	      if(S[m-k][n-1]==S[m][n]) {Ni++;}
	      if(S[m+1][n-k]==S[m][n]) {Ni++;}
	      
	    }
	  
	  if(m==1|| n==1||m==N||n==N)
	    Nd=5-Ni-Nv;
	  if(m==1&&n==1 ||m==1&&n==N || m==N&&n==1 ||m==N&&n==N)
	    Nd=3-Ni-Nv;     
	  if(m!=1 && n!=1 && m!=N && n!=N)
	    Nd=8-Ni-Nv;
	  
	  if(Nd>X*(Nd+Ni)) p_st=p_u;
	  else p_st=p_h;
	  
	  
	  u=S[m][n];
	  
	  
	  
	  
	  ///////////////////////////////////////////////////////////////////////////////////////////////////
	  ct=0;
	  random_shuffle ( liste_sp.begin(), liste_sp.end() );
	  
	  /* choix d'une nouvelle position pour l'individu*/
	  do{
	    
	    /*do	{x=(rand()+1.)/((double)RAND_MAX+1.);				
	      mf=ceil(N*x);	
	      y=(rand()+1.)/((double)RAND_MAX+1.); 
	      nf=ceil(N*y);
	      }
	      while(S[mf][nf]!=0);*/
	    
	    
	    
	    mf=liste_sp[ct].a;
	    nf=liste_sp[ct].b;
	    
	    ct++;
	    /*calcul du nombre de voisins dans cette nouvelle position*/
	    Ni=0;
	    Nv=0;
	    
	    
	    S[m][n]=0;   
	    
	    /*printf("mf=%d\n",mf);
	      printf("nf=%d\n",nf);*/
	    
	    for(k=0;k<2;k++)
	      {if(S[mf-1][nf+k]==0) {Nv++;}
		if(S[mf+k][nf+1]==0) {Nv++;}
		if(S[mf-k][nf-1]==0) {Nv++;}
		if(S[mf+1][nf-k]==0) {Nv++;}
		if(S[mf-1][nf+k]==u) {Ni++;}
		if(S[mf+k][nf+1]==u) {Ni++;}
		if(S[mf-k][nf-1]==u) {Ni++;}
		if(S[mf+1][nf-k]==u) {Ni++;}
		
	      }
	    if(mf==1|| nf==1||mf==N||nf==N)
	      Nd=5-Ni-Nv;
	    if(mf==1&&n==1 ||mf==1&&nf==N || mf==N&&nf==1 ||mf==N&&nf==N)
	      Nd=3-Ni-Nv;     
	    if(mf!=1&&nf!=1&&mf!=N&&nf!=N)
	      Nd=8-Ni-Nv;
	    
	  }
	  while(Nd>X*(Nd+Ni) && ct<liste_sp.size());
	  p_test=uni();
	  
	  if(Nd<=X*(Nd+Ni) && p_test<=p_st ) /* si l'agent est satisfait dans cette nouvelle position*/
	    
	    {S[mf][nf]=u; liste_position[agt-1].a=mf;liste_position[agt-1].b=nf; liste_sp[ct-1].a=m;liste_sp[ct-1].b=n;
	      
	      nomove=0;
	      /*for(i=1;i<N+1;i++)
		{for(j=1;j<N+1;j++)
		{if(S[i][j]==1) putpixel(i,j,0xff0000);
		if(S[i][j]==-1) putpixel(i,j,0x0000ff);
		if(S[i][j]==0) putpixel(i,j,0xffffff);
		}
		}
		
		SDL_UpdateRect(screen, 0, 0, 100, 100);
		SDL_Event event;
		while (SDL_PollEvent(&event)) 
		{switch (event.type) 
		{case SDL_KEYDOWN:
		break;
		case SDL_KEYUP:
		if (event.key.keysym.sym == SDLK_ESCAPE)
		return 0;
		break;
		case SDL_QUIT:
		return(0);
		
		}
		
		} 
	      */
	      
	    }else{S[m][n]=u; nomove++; 
	    continue;}
	  
	}
      else{ /* AGENTS SWITCH */
	p_f=uni();
	if(p_f<=0.05) S[m][n]=-S[m][n]; // ????????????????????????? ERREUR ??
      }
      
      
      
      /*else{do	{x=(rand()+1.)/((double)RAND_MAX+1.);				
	mf=ceil(N*x);	
	y=(rand()+1.)/((double)RAND_MAX+1.); 
	nf=ceil(N*y);
	}
	while(S[mf][nf]!=0);
	
	//calcul du nombre de voisins dans cette nouvelle position
	
	
	S[m][n]=0;           //important de le faire ici car si on le fait avant c'est une case vide potentielle
	S[mf][nf]=u;
	for(i=1;i<N+1;i++)
	{for(j=1;j<N+1;j++)
	{if(S[i][j]==1) putpixel(i,j,0xff0000);
	if(S[i][j]==-1) putpixel(i,j,0x0000ff);
	if(S[i][j]==0) putpixel(i,j,0xffffff);
	}
	}
	
	SDL_UpdateRect(screen, 0, 0, 640, 480);
	SDL_Event event;
	while (SDL_PollEvent(&event)) 
	{switch (event.type) 
	{case SDL_KEYDOWN:
	break;
	case SDL_KEYUP:
	if (event.key.keysym.sym == SDLK_ESCAPE)
	return 0;
	break;
	case SDL_QUIT:
	return(0);
	
	}
			
			}



			

}*/		


//}


    }	
  
  
  
  
  
  
  return(0);
}

