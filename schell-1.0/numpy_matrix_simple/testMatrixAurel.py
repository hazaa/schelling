# -*- coding: utf-8

"""
calcul matriciel "en place", wrapper swig python/c
version simplifiée
"""

# Import NumPy
import numpy as np
import Matrix

typeCode = "i"

# Test (type* INPLACE_ARRAY2, int DIM1, int DIM2)
# (see testMatrix.py)
floor = Matrix.__dict__["Floor"]
matrix = np.array([[6,7],[8,9]],typeCode)
floor(matrix,7)
np.testing.assert_array_equal(matrix, np.array([[7,7],[8,9]]))
