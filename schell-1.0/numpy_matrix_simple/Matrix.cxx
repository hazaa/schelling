#include <stdlib.h>
#include <math.h>
#include <iostream>
#include "Matrix.h"

void  Floor(int* array, int rows, int cols, int floor) {	    
  int i, j, index;                                                  
  for (j=0; j<cols; ++j) {                                          
    for (i=0; i<rows; ++i) {                                        
      index = j*rows + i;                                           
      if (array[index] < floor) array[index] = floor;               
    }                                                               
  }                                                                 
} 
