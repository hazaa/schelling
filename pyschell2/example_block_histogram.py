# -*- coding: utf-8

"""
Compute block count of each type of agent.
Compute histogram of block counts.

coded by Aurélien Hazan
"""
import numpy as np
import Schelling
import matplotlib.pylab as plt
from pyphystat.block import *

# simulation parameters
N = 100          # grid size
step0,step1= 100000,1000000   # number of time steps
nb_iter = 0   # unused
tol= 0.3          # tolerance
Occ=0.7       # occupation density
p_u=0.1         # probability to jump if unhappy
p_h=0.01        # probability to jump if happy
seed= 5
# variables
b=Schelling.boostRNG(seed)
typeCode = "i"  # type of the matrix. i=integer
mat1 = np.zeros((N,N),typeCode)
trans=np.array([0,0])
sim = Schelling.Sim(mat1,trans,tol,Occ,p_u,p_h,nb_iter,b)
sim.init_random()

# run
r=sim.run(step0)
mat0 = mat1.copy()
r=sim.run(step1)

# crop matrices ???? (remove border)

# compute block count 
blocksize =5
f= partial(count_goodval, goodval=[-1,1])
l0=apply_submat(mat0, blocksize,f)
l1=apply_submat(mat1, blocksize,f)

# checksum (block count vs full count) 
for i,(l,m) in enumerate(zip([l0,l1],[mat0,mat1])):
    nA=np.sum(m==-1)
    nB=np.sum(m==1)
    s= np.sum(np.array(l),0)
    if ((nA!=s[0])|(nB!=s[1])):
        print i,' count error', (nA, nB),s 


# compute histogram
# http://docs.scipy.org/doc/numpy/reference/generated/numpy.histogram2d.html#numpy-histogram2d
nbins = 10
fs= 18 # fontsize
bins = np.linspace(0, blocksize**2, nbins)
fig = plt.figure(figsize=(15,15))

for i,(l,m,niter) in enumerate(zip([l0,l1],[mat0,mat1],[step0,step1])):
    # histograms
    a=np.array(l)
    H, xedges, yedges = np.histogram2d(a[:,0], a[:,1], bins=[bins,bins])
    X, Y = np.meshgrid(xedges, yedges)
    ax = fig.add_subplot(2,2,2*i+1)
    ax.pcolormesh(X, Y, H)
    ax.set_aspect('equal')
    ax.set_xlabel('histogram', fontsize=fs)
    # final state
    ax = fig.add_subplot(2,2,2*i+2)
    ax.matshow(m)
    ax.set_xlabel('final state', fontsize=fs)
    # some text
    bbox = ax.get_position()
    fig.text(0.03, 0.95*bbox.y1,
             "n_iter=%d"%(niter) , ha = 'left', fontsize=fs)

params_str='Blocksize='+str(blocksize)+' tol='+str(tol)+' rho='+str(Occ)+ " p_u="+str(p_u)+ " p_h="+str(p_h)
fig.suptitle('Histogram of block count and finale state.\n'+params_str,
             fontsize=fs)
plt.show()
