/*
Memory management:
http://swig.org/Doc3.0/Python.html#Python_nn30

*/

/*
ORIGINAL CODE BY Laetitia Gauvin 
http://lps.ens.fr/~laetitia/
PhD thesis: https://hal.archives-ouvertes.fr/tel-00556769

NB: 
struct couple {int a;int b;};
std::vector< int > liste_value;           case pleine: valeur 
     agent normal: -1,1
     agent switch: 2
std::vector< couple> liste_position;      case pleine: position 
lv_ptr : pointeur vers liste_value
lp_ptr : pointeur vers liste_position
array:  case noire: array[index]=1; case blanche: array[index]=-1;
        condition aux limites: 2        
Swig c++/Python interface by A.Hazan.
*/

/* 
   TODO: 
   - use boost for random init 
   - input = array with A,B non-switching agents, and A/B switching agents
   - same encoding for input and output array (right now, that's not the case)
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>

//#include <boost/random/normal_distribution.hpp>
#include <vector>
#include <algorithm>
#include <numeric>
//#include "boost/multi_array.hpp"



#include "Schelling.h"
#define DEBUG 1


int gt=5;  // GENERATOR SEED ????????????????????
typedef boost::rand48 base_generator_type;
base_generator_type generator(gt);
boost::uniform_real<> uni_dist(0,1);
boost::variate_generator<base_generator_type&,boost::uniform_real<> >uni(generator,uni_dist);



Sim::~Sim(){
	// clear vectors
	// NB: Sim.array is owned by Python
	liste_value.clear();
	liste_position.clear();
	liste_sp.clear();
}


int Sim::init_random(){
	// CHECK ARGS:
	if(X<0 || X>1 || Occ<0 || Occ>1 || p_u<0|| p_u>1|| p_h<0||p_h>1|| nb_iter<0)
		return -1;
	// check init has not been done already	
	if (init_done==1)
		return -1;	
	// declarations   		
	int N=rows-2;
	int nomove;
	double x,y;	
	int i,j, index,m,n;
   	couple c;	
 	// register all sites as empty
	for(i=1;i<N+1;i++)               
		for(j=1;j<N+1;j++)     
		{{
			c.a=i; c.b=j; liste_sp.push_back(c);
		}}
    // shuffle list of empty sites
 	random_shuffle ( liste_sp.begin(), liste_sp.end() );
 	// fill in with +1
	for(i=1;i<N*N*Occ/2+1;i++)
	{
		// read and delete last element in list of empty sites
		c=liste_sp.back();
		liste_sp.pop_back();
		// copy coordinates to list of occupied sites
		liste_position.push_back(c);
		liste_value.push_back(1);
		// update state array
		m=c.a;
		n=c.b;
		index = n*rows + m; 
		array[index]=1;
	}
	// fill in with -1
	for(i=1;i<N*N*Occ/2+1;i++)
	{
		// read and delete last element in list of empty sites
		c=liste_sp.back();
		liste_sp.pop_back();
		// copy coordinates to list of occupied sites
		liste_position.push_back(c);
		liste_value.push_back(-1);
		// update state array
		m=c.a;
		n=c.b;
		index = n*rows + m; 
		array[index]=-1;
	}
	//finish initialization
	init_common();
	return 1;
}


	
int Sim::init_from_array(){
	/*CHECK ARGS:*/
	if(X<0||X>1||Occ<0||Occ>1||p_u<0||p_u >1||p_h<0||p_h >1||nb_iter<0)
		return -1;
	// check init has not been done already	
	if (init_done==1)
		return -1;	
	/* TO BE DONE*/	
		
	init_common();	
	return 1;	
	}

int Sim::init_common(){
	// declarations   		
	int N=rows-2;
	int i,j, index;
   	couple c;
  
	// conditions aux limites 
   for(j=0;j<N+2;j++)
    {
      index = j*rows + 0; // FIX AH
      array[index]=2;
      index = j*rows + N+1;
      array[index]=2;
      index = (N+1)*rows + j;
      array[index]=2;
      index = 0*rows + j; // FIX AH
      array[index]=2;
    }
	
	init_done=1;		
	return 1;	
	}

int Sim::run(int step){
	// check init was done
	if(init_done!=1)
		return -1;
	// declarations 
	int N=rows-2;
	int nomove;
	double x,y;
	double p_st,p_test;
	int i,j,p,k;
	int u, is_happy;
	int m,n,mf,nf,agt,state_agt;
	int Ni,Nv,Nd;
	int ct;
	int index;	
	std::vector<couple> neighbour;
	couple c;
   
// SET UP RANDOM GENERATORS
// TODO: keep pointers, to avoid re-declaring.
	boost::uniform_real<> uni_dist(0,1);
	boost::variate_generator<base_generator_type&,boost::uniform_real<> >uni(b_rng->generator,uni_dist);
	boost::uniform_int<> unilist_dist(1,liste_position.size());
    boost::variate_generator<base_generator_type&,boost::uniform_int<> >unilist(b_rng->generator,unilist_dist);
    boost::uniform_int<> unilistsp_dist(1,liste_sp.size());
    boost::variate_generator<base_generator_type&,boost::uniform_int<> >unilistsp(b_rng->generator,unilistsp_dist);
  
  for(p=0;p<step;p++) /*départ de la boucle d'iteration*/
    {       
      if(nomove>N*N) {
	// may be necessary for automatic stop	  
	//if (DEBUG){cout<<"break"<<endl;} 
	//break;
      }
      
      // choose an agent at random and get its 2d coordinates
      agt=unilist();
      m=liste_position[agt-1].a;
      n=liste_position[agt-1].b;      
      
      // compute the number of neighbors
      // Nd: number of sites with the opposite type 
      Ni=0; // number of sites with same type as the agent
      Nv=0; // number of empty sites around the agent 
      state_agt=liste_value[agt-1];
    
    //  if(state_agt!=2)/* AGENTS NON SWITCH */
	{for(k=0;k<2;k++)
	    { index = (n+k)*rows + m-1; // FIX AH
	      if(array[index]==0) {Nv++;}
	      index = (n+1)*rows + m+k; // FIX AH
	      if(array[index]==0) {Nv++;}
	      index = (n-1)*rows + m-k; // FIX AH
	      if(array[index]==0) {Nv++;}
	      index = (n-k)*rows + m+1; // FIX AH
	      if(array[index]==0) {Nv++;}
	      index = (n+k)*rows + m-1; // FIX AH
	      if(array[index]==array[n*rows + m]) {Ni++;}
	      index = (n+1)*rows + m+k; // FIX AH
	      if(array[index]==array[n*rows + m]) {Ni++;}
	      index = (n-1)*rows + m-k; // FIX AH
	      if(array[index]==array[n*rows + m]) {Ni++;}
	      index = (n-k)*rows + m+1; // FIX AH
	      if(array[index]==array[n*rows + m]) {Ni++;}	      
	    }	  
	  // compute CURRENT number of neighbors of the opposite type
	  if(m==1|| n==1||m==N||n==N)
	    Nd=5-Ni-Nv;
	  if(m==1&&n==1 ||m==1&&n==N || m==N&&n==1 ||m==N&&n==N)
	    Nd=3-Ni-Nv;     
	  if(m!=1 && n!=1 && m!=N && n!=N)
	    Nd=8-Ni-Nv;
	  
      // set transition probability: agent is unhappy if Nd>X*(Nd+Ni)
	  if(Nd>X*(Nd+Ni)) {
		  is_happy=0;
		  p_st=p_u;
		  }
	  else {
		  is_happy=1;
		  p_st=p_h;
		  }
	  // get agent type
	  u=array[n*rows + m];	 	  
	  	 
	  ct=0; // index in the list of potential empty landing sites 
	  random_shuffle ( liste_sp.begin(), liste_sp.end() );

	  /* choix d'une nouvelle position pour l'individu*/
	  do{
		// get x,y coordinates of potential free site  
	    mf=liste_sp[ct].a;
	    nf=liste_sp[ct].b;
	    
	    ct++;
	    /*calcul du nombre de voisins dans cette nouvelle position*/
	    Ni=0;
	    Nv=0;	    
	    array[n*rows + m]=0;   
	    
	    for(k=0;k<2;k++)
	      {
		index = (nf+k)*rows + mf-1;
		if(array[index]==0) {Nv++;}
		index = (nf+1)*rows + mf+k;
		if(array[index]==0) {Nv++;}
		index = (nf-1)*rows + mf-k;
		if(array[index]==0) {Nv++;}
		index = (nf-k)*rows + mf+1;
		if(array[index]==0) {Nv++;}
		index = (nf+k)*rows + mf-1;
		if(array[index]==u) {Ni++;}
		index = (nf+1)*rows + mf+k;
		if(array[index]==u) {Ni++;}
		index = (nf-1)*rows + mf-k;
		if(array[index]==u) {Ni++;}
		index = (nf-k)*rows + mf+1;
		if(array[index]==u) {Ni++;}		
	      }
	    // compute POTENTIAL number of neighbors of the opposite type
	    if(mf==1|| nf==1||mf==N||nf==N)
	      Nd=5-Ni-Nv;
	    if(mf==1&&n==1 ||mf==1&&nf==N || mf==N&&nf==1 ||mf==N&&nf==N)
	      Nd=3-Ni-Nv;     
	    if(mf!=1&&nf!=1&&mf!=N&&nf!=N)
	      Nd=8-Ni-Nv;
	    
	  }
	  while(Nd>X*(Nd+Ni) && ct<liste_sp.size());
	  
	  p_test=uni();
	  
	  // move if agent is happy in the new location, with probability p_st
	  if(Nd<=X*(Nd+Ni) && p_test<=p_st ) 
	    {
		  // save new position, free old position
	      array[nf*rows + mf]=u ;
	      liste_position[agt-1].a=mf;
	      liste_position[agt-1].b=nf;
	      liste_sp[ct-1].a=m;
	      liste_sp[ct-1].b=n;	      
	      nomove=0;
	      // record transition:
	      if(is_happy)
	      { transition[0]++;}
	      else
		  {	transition[1]++; }		  
	    }else{ /* no move*/
	    array[n*rows + m]=u;// S[m][n]=u; 
	    nomove++; 
	    continue;}	  
	}
            
    }	
  return p;
   
}


SimSwitch::~SimSwitch(){
}

int SimSwitch::run(int step){
  return 0;
}
