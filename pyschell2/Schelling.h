/*
 * Schelling.h
 * 
 * Copyright 2015 Aurélien Hazan <>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef SCHELL_H
#define SCHELL_H

#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/linear_congruential.hpp>

struct couple {int a;int b;};
struct sgl {double e;};

/*
 * 
 * name: inconnu
 * @param
 * @return
 * http://garajeando.blogspot.fr/2012/02/revisiting-wrapping-boost-random.html
 */

class boostRNG{
 public:
	boostRNG(int s): generator(s){};
	~boostRNG(){};
	boost::rand48 generator;	
	};

/*
 * 
 * name: inconnu
 * @param
 * @return
 * 
 */
class Sim{
public:
  Sim (int* array, int rows, int cols, int* array1, int size, double x, double o,double pu, double ph, int ni, boostRNG *b):
    array(array),transition(array1),transition_size(size),rows(rows),cols(cols),X(x),Occ(o),p_u(pu),p_h(ph),nb_iter(ni), b_rng(b) 
	{init_done=0;};
  ~Sim();
  int init_random();
  int init_from_array();
  int run(int step);
  
//private: // DEBUG
  int init_common(); /* must be called at the end of init_..*/
	
  int init_done; 
  int* array;       // 2d array. state of the system
  int* transition;  // transition count vector.
  int transition_size; 
  int rows;	
  int cols;
  double X;			// tolerance
  double Occ; 		// occupancy	
  double p_u; 		// probability to move if unhappy
  double p_h;	     
  int nb_iter;	    // unused
  
  std::vector <int> liste_value;          /* case pleine: valeur */
  std::vector <couple> liste_position;     /* case pleine: position */
  std::vector <couple> liste_sp;            /* case vide: position */
  
 private :
	boostRNG *b_rng;
}; 

/*
 * 
 * name: inconnu
 * @param
 * @return
 * 
 */
class SimSwitch{
public:
SimSwitch(int* array, int rows, int cols, int* array1, int size, double x, double o,double a,double pu, double pf, int ni, boostRNG *b):
    array(array),transition(array1),transition_size(size),rows(rows),cols(cols),X(x),Occ(o),agt_switch(a),p_u(pu),p_f(pf),nb_iter(ni), b_rng(b) 
	{init_done=0;};
  ~SimSwitch();
  int run(int step);
   int init_done; 
  int* array;       // 2d array. state of the system
  int* transition;  // transition count vector.
  int transition_size; 
  int rows;	
  int cols;
  double X;			// tolerance
  double Occ; 		// occupancy	
  double p_u; 		// probability to move if unhappy 
  int nb_iter;
  // extra parameters:
  double p_f;  		// probability to switch for switch agents
  double agt_switch; // proportion of switch agents
  
  private :
	boostRNG *b_rng;
};



#endif
