# -*- coding: utf-8

"""
Python wrapper to schelling simulator in c++
(see testMatrix.py)

coded by Aurélien Hazan
"""

# Import NumPy
import numpy as np
import Schelling
import matplotlib.pylab as plt
#from pyphystat.rng import circle_rng
#import pyphystat.Pyphystat

typeCode = "i"  # type of the matrix. i=integer
N = 50      # grid size
steps= 1000
nb_iter = 0   # unused
X= 0.3          # tolerance
Occ=0.7       # occupation density
agt_switch=0.0  # switching agents proportion
p_u=0.1         # probability to jump if unhappy
p_h=0.01         # probability to jump if happy

# random init
b=Schelling.boostRNG(5)
mat = np.zeros((N,N),typeCode)
trans=np.array([0,0])
#mat0 = np.copy(mat)
sim = Schelling.Sim(mat,trans,X,Occ,p_u,p_h,nb_iter,b)
sim.X

# assert sim.p_u=p_u, sim.X=X ...

###########
# INIT RANDOM
sim.init_random()
# assert sim.init_done=1
# assert sim.init_random()           # do it again to check flag control
# assert proportion vide=occ
# assert nb(1)=nb(-1)
# assert sim.liste_sp.__sizeof__()==np.sum(mat==-1) # remove border before
# assert border = 2
############
# RUN
cum_jump=[]
n=20
steps=10000
for i in range(n):
	r=sim.run(steps)
	cum_jump.append(trans.copy()) # copy is necessary

#############
# PLOT
plt.matshow(mat); plt.show()
plt.plot(range(n-1),flow); plt.show()

##############
# conditions aux limites (avoir 2 partout)
# Q: cela ne pose-t-il pas un pb d'encodage avec switch ?

############
# memory management: on crée mat, puis sim(mat), puis on détruit mat. 
# => sim pointe vers zone desallouée!!
l= sim.liste_sp
del(sim)
print(l)

############
sim.init_from_array()

#######################
# indépendance des RNG
# http://garajeando.blogspot.fr/2012/02/revisiting-wrapping-boost-random.html

##########################
# with switching agents
p_f=0.01        # probability to switch 
