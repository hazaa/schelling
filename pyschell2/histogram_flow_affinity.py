# compute histogram
# estimate distributions
# estimate prob1(x*)-prob2(x*) along the border (see Durrett)
# compute flow, compare
# affinity ?

############
# RUN
cum_jump=[]
n=20
steps=10000
for i in range(n):
	r=sim.run(steps)
	cum_jump.append(trans.copy()) # copy is necessary
##############
# COMPUTE FLOW
cum_jump=np.array(cum_jump)
flow = np.diff(cum_jump, n=1,axis=0) 
