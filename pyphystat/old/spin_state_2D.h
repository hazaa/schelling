/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

#ifndef _SPIN_STATE_2D_
#define _SPIN_STATE_2D_

#include "spin_state.h"

//! 2D state class 
/*!
  the spin state is encoded as a 2dim array.
  The default set where the elements belong to is {-1,0,1}
  Relabelling is possible using the second constructor
  and the fields label_plus_one, label_minus_one.
 */

class state_2D: public state {
  int nx;
  int ny;
 public:
  int **matrix;
  int label_plus_one;
  int label_minus_one;
 public:
  state_2D(int** m, int _nx, int _ny);
  state_2D(int** m, int _nx, int _ny, 
	   int _label_plus_one, int _label_minus_one);
  int get(int i);
  void update_data(int **m);
};


/*
  
 */


class neighbour_2D: public neighbour {
  int nx;
  int ny;
  int nneighbour;
 public:
  neighbour_2D(int _nneighbour, int _nx, int _ny);
  int get(int dim, int site);
};


#endif
