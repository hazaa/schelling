/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

/**********************************************************************
 * PROGRAM: spin_state_2D
 * PURPOSE: member functions of spin_state_2D and neighbour_2D
 *
 * VERSION : 29 july 2012
 * COMMENT : inspired by W.Krauth's book, algorithm 5.1
 *           see http://www.smac.lps.ens.fr/index.php/Main_Page
 * AUTHOR  : Aurelien Hazan
 * LANGUAGE: C++
 *
 **********************************************************************/

/*************************************************************
 * Site encoding:
 * 
 * 0     |  nx      | ... | nx(ny-1)
 * 1     |  nx +1   |    
 *   
 * nx-1  |  2nx -1  | ... | nx ny-1
 *
 *
 * Neighbourhood encoding: 
 * let s be the actual site
 *    -neighbour along x (dimension 0):   
 *          if s is not along the bottom line, neighour is site s+1
 *          else, we take the first element of the same colon
 *    -neighbour along y (dimension 1):   
 *          if s is not along the right colon, neighbour is site + nx
 *          else we compute the remainder of euclidean division by nx 
 *               and add nx(ny-1)
 *************************************************************/
#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "spin_state_2D.h"

#define DEBUG 0

/**************************************************************/

state_2D::state_2D(int** m, int _nx, int _ny){
   matrix = m;
   nx = _nx;
   ny = _ny;
   label_plus_one= 1;
   label_minus_one= -1;
}

state_2D::state_2D(int** m, int _nx, int _ny,
		   int _label_plus_one, int _label_minus_one)
{
  matrix = m;
  nx = _nx;
  ny = _ny;
  label_plus_one= _label_plus_one;
  label_minus_one=_label_minus_one;
}

int state_2D::get(int i){
  int ix, iy;
  div_t temp=div(i,nx);
  ix = temp.rem;
  iy = temp.quot;
  int spin=matrix[ix][iy] ;
  
  // relabelling
  if( spin==0)
    { }
  else
    { 
      if(spin==label_plus_one)
	spin=1;
      else
	{
	  if(spin==label_minus_one)
	    spin=-1;
	  else
	    {
	      perror( "unsupported label" );
	      exit(-1);
	      //throw "unsupported label";
	    }
	}      
    }
  
  return spin;
}

void state_2D::update_data(int **m){
  matrix = m;
}

/**************************************************************/

neighbour_2D::neighbour_2D(int _nneighbour, int _nx, int _ny){
  nneighbour= _nneighbour;
  nx = _nx;
  ny = _ny;
}

int neighbour_2D::get(int dim, int site){
  
  int neigh_site ;  
  if(DEBUG) std::cout<<"NEIGH2D: nneighbour="<<nneighbour<<" nx="<<nx<<" ny="<<ny<<std::endl;

  if(nneighbour==4)
    {
      div_t temp;
      int a = nx*(ny-1);
      
      switch(dim){
      case 0: //x
	temp = div(site+1, this->nx);
	if(temp.rem==0)
	  neigh_site = site - (nx-1);
	else
	  neigh_site = site + 1;	
	break;
      case 1: //y
	if(site< a)
	  neigh_site = site + nx;
	else
	  {
	    temp = div(site, nx);
	    neigh_site = temp.rem;
	  }
	break;
      default:
	std::cout << "error"<<std::endl;
	//throw "unsupported dimension";
      }
	
    }
  else
    {
      std::cout << "error"<<std::endl;
      //throw "unsupported number of neighbours"      ;
    }

  
  return neigh_site;
}
/**************************************************************/
