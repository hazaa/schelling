#ifndef PYPHYSTAT_H
#define PYPHYSTAT_H

double interface_density(int* array, int rows, int cols);
void count_block_content(int* array, int rows, int cols, int* array_count, int rows_count, int cols_count, int block_size);
#endif
