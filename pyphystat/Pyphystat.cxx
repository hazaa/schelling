#include <stdlib.h>
#include <stdio.h>
#include<math.h>

#include "Pyphystat.h"

/*
  see "A unified framework for Schelling’s model of segregation"
  Rogers, McKane
  http://arxiv.org/abs/1104.1971v2
  Equation (3):
  x= number of edges between agents of opposite types
  ------------------------------------------------
  number of edges between agents of any type

  REMARK: only interfaces between cells whith nonzero values will be counted

  IN:
  --
  array 

  OUT:
  --
  float x 
*/
double interface_density(int* array, int rows, int cols)
{
  double iface_dens=-1.0 ;   // density of heterogeneous interface 
  int iface_nb   = 0;   // number of heterogeneous interface 
  int index;
  int c;        // this cell
  int c_next_x; // next cell along x axis
  int c_next_y; // next cell along y axis
  
  for(int i=0; i<rows; i++) 
    for(int j=0; j<cols; j++) 
      {
	// get content of cells
	index = j*rows + i;
	c = array[index];
	
	// compute interface number
	if( c !=0)
	  {
	    // get content of cells
	    if(i==rows-1){
	      index = j*rows + 0;
	      c_next_x = array[index]; // toroidal geometry
	    }
	    else{
	      index = j*rows + i+1;
	      c_next_x = array[index];
	    }
	    if(j==cols-1)
	      {
		index = 0*rows + i;
		c_next_y = array[index]; // toroidal geometry
	      }
	    else
	      {
		index = (j+1)*rows + i;
		c_next_y = array[index];
	      }
	    // increment heterogeneous interface
	    if( c_next_x>0)
	      if( c != c_next_x) iface_nb++;
	    
	    if( c_next_y>0)
	      if( c != c_next_y) iface_nb++;	    
	  }
      }
  // normalize to get interface density
  iface_dens = ((double)iface_nb )/((double)(2*rows*cols)) ;
  
  return iface_dens;
}

/*
  
 */
void count_block_content(int* array, int rows, int cols, int* array_count, int rows_count, int cols_count, int block_size)
{
  // noty coded yet
}
