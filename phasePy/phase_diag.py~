# -*- coding: utf-8

#################################################################
# Copyright 2012 Aurelien Hazan
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 
# 
#################################################################

import numpy
import rnd_sim
import itertools
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import axes3d
#import matplotlib.ticker as tick
import h5py
import os.path
import types
import re

import matplotlib.font_manager # used ?


class Phase_diag(rnd_sim.Rnd_sim):
    def __init__(self):
        self.my_sim=[]      # pointer to simulation object
        self.param_list=[]  # control param list (see init() )
        self.nb_ctrl_prm=[]          # number of parameters
        self.param_size=[]  # vector containing the size of each param vector
        self.param_idx=[]   
        self.idx_it=[]      # internal (product interator, see run())
        self.hdf5_file=[]
        self.dir_data='data'
        self.fname_data = 'phase_diag'
        self.suffix_data = 'hdf5'
        self.dsetname_ord = 'OrderParameter' #dataset name in hdf5 file
        self.dsetname_ctrl = 'ControlParameter' #dataset name in hdf5 file
        self.dsetname_snap = 'OrderParamTrajectory' #dataset name in hdf5 file
        self.dsetname_finstate = 'FinalState' #dataset name in hdf5 file
        self.ord_prm_name=[] # name of order parameters computed by simulation
        self.nb_ord_prm=[] # number of order parameters
        self.nb_ctrl_prm=[] # number of control parameters
        self.nlevels = 32  # contourf plot parameter
        self.snapshot_length = 10 # length of snapshop trajectories
        self.finstate_length = [] # length of concatenated final state
        self.save_finstate = True # record final states 
        self.description = [] # plain text description of the experiment

        self.plot_style = '3D' # {'3D', 'contour', 'countourf'}
        self.plot_color = False
        

    def get_save_fname(self):
        # this function provides a name for the save file
        i=1
        imax = 100
        fname = './' + self.dir_data + '/' +  self.fname_data
        full_fname = fname+'_'+str(i)+'.'+self.suffix_data
        c=os.path.exists(full_fname)
             
        while c :
             fname = './' + self.dir_data + '/' +  self.fname_data
             full_fname = fname+'_'+str(i)+'.'+self.suffix_data
             c=os.path.exists(full_fname)
             i = i+1
             if(i>imax):
                  break             
        print "Data will we saved in:", full_fname
        return full_fname
      
        
    def init(self, s, p, p_fixed=None):
        # process arg list
        # the argument list p should come under the form 
        # ['param_name1', param_val1, 'param_name2', param_val2...] 
        self.my_sim = s
        self.param_list = p
        self.param_fixed_list = p_fixed
        self.nb_ctrl_prm = len(p)/2
        self.param_size = numpy.zeros( self.nb_ctrl_prm, dtype = int  )
     
        for i in range(self.nb_ctrl_prm): 
            self.param_size[i]= len(p[2*i+1])        
            self.param_idx.append(range( len(p[2*i+1]) ))

        # get output file name, create hdf5 file
        # set file attributes 
        # see http://h5py.alfven.org/docs-2.0/intro/quick.html
        fname = self.get_save_fname()
        self.hdf5_file = h5py.File( fname, 'w')
        self.ord_prm_name = self.my_sim.get_ord_prm_name() 
        self.nb_ord_prm = len(self.ord_prm_name)
        self.hdf5_file.attrs["nb_order_param"]=self.nb_ord_prm
        self.hdf5_file.attrs["nb_control_param"]=self.nb_ctrl_prm
        self.hdf5_file.attrs["description"]=self.description

        # create datasets for order parameters
        z = numpy.zeros( self.param_size )
      
        for j in range(self.nb_ord_prm):
            dset_name =  self.dsetname_ord+str(j)
            dset =  self.hdf5_file.create_dataset( dset_name, data=z) 
            # record name and value of control params
            for k in range(self.nb_ctrl_prm):
                pname = self.param_list[2*k]
                pval = self.param_list[2*k+1]
                dset.attrs['control_param ' + pname]=pval
            dset.attrs["order_param"] = self.ord_prm_name[j]
        
        # create datasets for control parameters
        for j in range(self.nb_ctrl_prm):
            dset_name =  self.dsetname_ctrl+str(j)
            dset =  self.hdf5_file.create_dataset( dset_name, data=z) 
            dset.attrs["name"] = self.param_list[2*j]
        # create dataset for  snapshot (=subsampled trajectory of ord param)
        # one dimension is added for time, compared to the order parameter
        # dataset    
        snap_size=numpy.concatenate( (self.param_size,
                                      numpy.array([self.snapshot_length] )) )
        dset_snap=self.hdf5_file.create_dataset(self.dsetname_snap, snap_size )

        # create dataset for final simulation state
        # must be done at run time because size of the final state is not known at this point
            
        # record all numeric and strings attributes of the current
        # simulation in a subgroup
        full_simulation_param=self.my_sim.get_sim_params()
        subgroup = self.hdf5_file.create_group("SimulationParams")
        for k in range(len(full_simulation_param)/2):
               pname = full_simulation_param[2*k]
               pval = full_simulation_param[2*k+1]
               subgroup.attrs[pname]=pval

    def run(self):
        # create product iterator 
        # cf 'product' in http://docs.python.org/library/itertools.html
        self.idx_it= itertools.product(*self.param_idx)
        
        # main loop
        for n_i,i in enumerate(self.idx_it):
            # put current param value in list p = (pname,pval)
            p =[]
            for j in range(self.nb_ctrl_prm):
                idx = i[j]
                pname = self.param_list[2*j]
                pval = self.param_list[2*j+1][idx]
                p.append(pname)
                p.append(pval)
                # write in hdf5
                dset_name_full =  self.dsetname_ctrl+str(j)
                dset = self.hdf5_file[dset_name_full] 
                # check name corresponds
                if(pname==dset.attrs["name"]):
                    dset[i]= pval
                else:
                    raise ValueError('dataset name mismatch')
                
            # set up the simulation, run it
            if(self.param_fixed_list!=None): 
                self.my_sim.set_params(self.param_fixed_list)
            self.my_sim.set_params(p)
            print "PARAMS SHOULD BE CHECKED HERE"
            self.my_sim.run()

            # get order param and final state
            o = self.my_sim.get_ord_prm()
            finstate = []
            if self.save_finstate:
                finstate = self.my_sim.get_state()
            
            # debug
            print "============================="
            print "idx=",i, "paramlist=", p , "ord_prm=",o,
            print self.my_sim.sch.printConfig()
            print "self.my_sim.density=",self.my_sim.density
            print "self.my_sim.frac=",self.my_sim.frac
            "================================"

            # write order parameters 
            for j in range(self.nb_ord_prm):
                # order params
                dset_name_full =  self.dsetname_ord+str(j)
                dset = self.hdf5_file[dset_name_full] 
                dset[i]=o[j]


            # write trajectory snapshot
            dset_snap = self.hdf5_file[self.dsetname_snap]              
            x_t = numpy.array(self.my_sim.sch.getSegregationRecMean())
            idx = numpy.floor(numpy.linspace(0, len(x_t)-1, 
                                             num=self.snapshot_length))
            idx = idx.astype(int)
            dset_snap[i]= x_t[idx]
            
            # write final state
            if self.save_finstate:
                # create dataset for final simulation state
                if n_i==0:
                    self.finstate_length= finstate.size
                    finstate_size = numpy.concatenate((self.param_size,
                                                       numpy.array([self.finstate_length] )))
                    dset_finstate=self.hdf5_file.create_dataset(self.dsetname_finstate,
                                                                finstate_size,'i' )
                # write
                dset_finstate[i]=finstate.flatten()

    def free(self):
        if (self.my_sim!=[]):
            self.my_sim.free();
        self.hdf5_file.close()

    def load(self, fname=[]):
        # read control and order parameters
        # (not the other simulation parameters)
        if(fname==[]):
            fname=self.filename
        self.hdf5_file = h5py.File( fname, 'r')

        self.nb_ctrl_prm= self.hdf5_file.attrs["nb_control_param"]
        self.nb_ord_prm = self.hdf5_file.attrs["nb_order_param"]
        
        # get number of control parameters 
        dset_name_full =  self.dsetname_ord+str(0)
        dset = self.hdf5_file[dset_name_full] 

        # reconstruct control param list
        for name, value in dset.attrs.iteritems():
            m = re.search("control_param", name)   
            if (m!= None):
                # discard the prefix "control_param " 
                # (NB: note the space, which corresponds to +1)
                self.param_list.append(name[m.end()+1:]) 
                self.param_list.append(value)
                print "phase_diag control_param", name, "=", value
          
        # reconstruct order param list
        # this is needed because one can't store a python list in hdf5
        # with h5py (may be possible with pytables)
        for i in range(self.nb_ord_prm):
            dset_name_full =  self.dsetname_ord+str(i)
            dset = self.hdf5_file[dset_name_full] 
            for name, value in dset.attrs.iteritems():
                m = re.search("order_param", name)   
                if (m!= None):
                    self.ord_prm_name.append(value)
                    

    def get_ord(self, ord):
        # Return a multidimensional array of (scalar) order
        # parameters with index ord.
        dset_name_full =  self.dsetname_ord+str(ord)
        dset = self.hdf5_file[dset_name_full] 
        return dset.value

    def get_finstate(self, idx):
        # return a one-dimensional array
        # idx is a tuple
        dset_finstate = self.hdf5_file[self.dsetname_finstate] 
        return dset_finstate[idx]

    def get_trajectory(self, idx):
        # return a one-dimensional array
        # idx is a tuple
        dset_trajectory = self.hdf5_file[self.dsetname_snap] 
        return dset_trajectory[idx]
        

    def plot(self):
        if self.nb_ctrl_prm>2:
            raise ValueError('dimension too high')
        
        plt.ion()
        plt.clf()
        fig=plt.gcf()
        nr = self.nb_ord_prm
        nc = 1        
        #font0 = font.FontProperties()
        #font0.set_size('large')
        #font0.set_size(18)

        for i in range(nr):
            if self.nb_ctrl_prm==1:
                nc = 1   
                ax=plt.subplot(nr,nc, i+1)
                    
                ctrl_param_name = self.param_list[0]
                ctrl_param_val = self.param_list[1]
                order_param = self.get_ord(i)
                plt.plot(ctrl_param_val, order_param, 'k')
                
                if i==(nr-1):
                    # remap text. TODO: remove it
                    if ctrl_param_name=='frac':
                        c = 'f'
                    else:
                        c= ctrl_param_name
                    plt.xlabel(c)
                plt.ylabel(self.ord_prm_name[i])
                yfmt=ax.yaxis.get_major_formatter()
                yfmt.set_powerlimits([ -3, 3])
                #plt.ticklabel_format(style='sci', axis='y')
                #tick.ScalarFormatter.set_scientific(True)
            elif self.nb_ctrl_prm==2:   
                nc = 1                    

                if(self.plot_style=='3D'):
                    #ax = fig.add_subplot(nr, nc, i+1, 
                     #                    projection='3d')
                    fig=plt.figure()
                else:
                    plt.subplot(nr,nc, i+1)

                order_param = self.get_ord(i)

                dset_name_full =  self.dsetname_ctrl+str(0)
                dset = self.hdf5_file[dset_name_full]
                ctrl_param_name1 = dset.attrs["name"]
                C1= dset.value
                dset_name_full =  self.dsetname_ctrl+str(1)
                dset = self.hdf5_file[dset_name_full]
                ctrl_param_name2 = dset.attrs["name"]
                C2 =  dset.value

                levels = numpy.linspace(numpy.min(order_param),
                                        numpy.max(order_param), 
                                     self.nlevels)
                if self.plot_style=='contourf':
                    CS=plt.contourf(C1,C2,order_param, levels,
                                    cmap=plt.cm.get_cmap('jet', len(levels)-1),
                                    )
                                #fontproperties=font0)
                elif self.plot_style=='contour':
                    l = numpy.linspace(numpy.min(order_param),
                                       numpy.max(order_param),
                                       5) 
                    CS = plt.contour(C1, C2, order_param, levels=l,colors='k' )
                    plt.clabel(CS, fontsize=9, inline=1)
                elif (self.plot_style=='3D'):
                    ax = plt.gca(projection='3d') 
                    ax.plot_wireframe(C1,C2,order_param,
                                      rstride=8, cstride=8,colors='k')
                # same inversion here
                if(self.plot_style=='3D'):
                    # remap text label. TODO: remove it
                    if ctrl_param_name1=='frac':
                       c = 'f'
                    else:
                        c = ctrl_param_name1
                    plt.xlabel(c)
                    #plt.zlabel('x')                    
                else:
                    if i==(nr-1):
                        plt.xlabel(ctrl_param_name1)
                        
                # remap text label. TODO: remove it
                if ctrl_param_name2=='tol':
                    c = 'tolerance'
                else:
                    c = ctrl_param_name2
                plt.ylabel(c)
                if self.plot_color:
                    plt.colorbar(CS,format="%1.1e")
                    
                
        # call the simulations's own plot
        #plt.figure()    
        #self.my_sim.plot()


if __name__ == "__main__":
    import schell
    s=schell.Schell()
    s.set_params()
    s.alloc()     
    s.set_params(['iterMax',30, 'width',30,'height',30, \
                      'stepMax', 1000, 'trigTime',0, 'stepBurn',500]) 
    s.set_params(['density',0.9])   #important!
    s.set_params(['frac',0.0])   #important!
    p = [#'frac', numpy.linspace(0.1,0.9,num=2), 
         'density', numpy.linspace(0.7,0.98,num=10),
         'tol', numpy.linspace(0.0,0.9,num=10)  
         ]
    pd = Phase_diag()          
    pd.init(s,p)
    pd.run()
    i=0
    print "order_param[",i,"]=", pd.get_ord(i)
    print "order param name=", pd.ord_prm_name
    pd.plot()
    raw_input("---TYPE KEYBOARD----")
    pd.free()

    #load
    # import phase_diag
    # pd = phase_diag.Phase_diag()
    # pd.load('./data/phase_diag_16.hdf5')
    # pd.plot()
