import numpy as np
import matplotlib.pylab as plt
#import matplotlib.ticker as tick
import schell
from plot.classes import *
# --------------------------
# init objets, set parameters
color_plot = False
print_x = False     # add interface density as x label

s=schell.Schell()
s.set_params()
w=30
s.set_params(['width',w,'height',w])   
s.set_params(['iterMax',1,'stepMax',1000,'stepBurn',0])
f=0.3; tolerance=0.3;density=0.95
s.set_params(['density',density, 'tol', tolerance, 'frac',f])   
s.alloc()

# --------------------------
# run one step and plot
s.single_run(3)


nr=1
nc=2
#plt.subplot(nr,nc,1)
state=s.get_state()
fig = plt.figure()
plt.suptitle("f= %1.1f, tolerance=%1.1f, density=%1.1f"%(f, tolerance,density), size=18)
ax = plt.subplot(1,2,1) 

if(color_plot):
    plt.matshow(state, fignum=False)
else:
    #http://matplotlib.org/examples/pylab_examples/contourf_hatching.html
    #levels = [1]
    #x = np.linspace(0, w, w)
    #plt.contour(x, x, state, levels=levels,colors='k', interpolate=None )
    matshow_bw(state,vmin=1,vmax=2,ax=ax)
    #cs = plt.contourf(x, x, state, levels=levels, 
    #                  hatches=['.', '/', '\\', None, '\\\\', '*'],
     #                                       )
    # create a legend for the contour set
    #artists, labels = cs.legend_elements()
    #plt.legend(artists, labels, handleheight=2)

o=s.get_ord_prm()
print o
if print_x:
    plt.xlabel('x=%1.2f'%o[0] )

# --------------------------
# run several steps and plot
s.run()

#plt.subplot(nr,nc,2)
state=s.get_state()
ax = plt.subplot(1,2,2) 

if(color_plot):
    plt.matshow(state, fignum=False)
    #plt.colorbar()
else:
    #levels = [1]
    #x = np.linspace(0, w, w)
    #plt.contour(x, x, state, levels=levels,colors='k')
    matshow_bw(state,vmin=1,vmax=2,ax=ax)

o=s.get_ord_prm()
print o
if print_x:
    plt.xlabel('x=%1.2f'%o[0] )



plt.show()

# barplot + hatch
#http://matplotlib.org/examples/pylab_examples/hatch_demo.html?highlight=hatch
'''
import numpy as np
import matplotlib.pylab as plt
from matplotlib.colors import ListedColormap
#from matplotlib.patches import Patch
fig = plt.figure()
#ax.fill([1,3,3,1],[1,1,2,2], fill=False, hatch='\\')
cmap=ListedColormap(['white','black'])
m=np.array([[0,1,2],[0,1,2],[0,1,2]])
plt.matshow(m, fignum=False, vmin=1, vmax=2, cmap=cmap)

ax=fig.gca()
idx=(m==0).nonzero()
#ax.bar(range(1,3), range(1,3), color='white', edgecolor='black', hatch="/")
#beware of the inversion between bottom and left
ax.bar(bottom=idx[0]-0.5, height=np.ones(3),
left=idx[1]-0.5, width=1, color='white', edgecolor='black', hatch="/")
ax.set_xlim(-0.5, 3)
ax.set_ylim(3,-0.5)

plt.show()
'''


# slice
#http://stackoverflow.com/questions/15582105/python-plot-stacked-image-slices
'''
from mpl_toolkits.mplot3d import Axes3D
fig=plt.figure()
ax = fig.gca(projection='3d')

x = np.linspace(0, 1, 30)
X, Y = np.meshgrid(x, x)
levels = np.linspace(-1, 1, 3)
eps=0.001
ax.contourf(X, Y, s.get_state(), zdir='z', 
            levels=[0-eps,1-eps,2-eps], colors=['w','r','b'] ,
            antialiased=False, offset =0)
#plt.contourf(s.get_state())
plt.show()



fig = plt.figure()
 ax = fig.gca(projection='3d')

x = np.linspace(0, 1, 30)
X, Y = np.meshgrid(x, x)
levels = np.linspace(-1, 1, 3)
ax.contourf(X, Y, 0+0.1*s.get_state(), zdir='z', levels=0 + 0.1*levels)
ax.contourf(X, Y, 1+0.1*s.get_state(), zdir='z', levels=1 +0.1*levels)
ax.contourf(X, Y, 2+0.1*s.get_state(), zdir='z', levels=2+0.1*levels)

ax.legend()
ax.set_xlim3d(0, 1)
ax.set_ylim3d(0, 1)
ax.set_zlim3d(0, 2)
'''

