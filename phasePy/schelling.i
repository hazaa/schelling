%module schelling
%{
#include "schelling_naive.h"
  %}

/* stl templates:
 *   found in swig-1.3.40/Examples/python/std_vector
 */
%include stl.i
 /* instantiate the required template specializations */
namespace std {
  %template(IntVector)    vector<int>;
   %template(DoubleVector) vector<double>;
 }

/*
%include "numpy.i"
%init %{
  //import_array();
  %}

%define %apply_numpy_typemaps(TYPE)
%apply (TYPE* INPLACE_ARRAY2, int DIM1, int DIM2) {(TYPE* array, int rows, int cols)};
%enddef 
%apply_numpy_typemaps(double            )
*/

%include "schelling_naive.h"

 /*
%include "exception.i"
%exception {
   $action
   if (err_occurred()) {
      PyErr_SetString(PyExc_RuntimeError, err_message());
      return NULL;
   }
}
 */
