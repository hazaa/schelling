import numpy as np
import matplotlib.pylab as plt

import schell
from plot.classes import *
# --------------------------
# init objets, set parameters
color_plot = False
print_x = False     # add interface density as x label

s=schell.Schell()
s.set_params()
w=30
s.set_params(['width',w,'height',w])   
stepMax=1000
trigTime=stepMax
if trigTime<stepMax:
    switch_agent_active = 'active'
else:
    switch_agent_active = 'present but inactive'

s.set_params(['iterMax',1,'stepMax',stepMax,'stepBurn',0,
              'trigTime', trigTime])
f=0.9; tolerance=0.3;density=0.95
s.set_params(['density',density, 'tol', tolerance, 'frac',f])   
s.alloc()

# --------------------------
# run one step and plot
s.single_run(3)


nr=1
nc=2
#plt.subplot(nr,nc,1)
state=s.get_state()
fig = plt.figure()
if f>0:
    plt.suptitle("f= %1.1f, tolerance=%1.1f, density=%1.1f\nswitching agents: %s"%(f, tolerance,density,switch_agent_active), size=18)
else:
    plt.suptitle("f= %1.1f, tolerance=%1.1f, density=%1.1f"%(f, tolerance,density), size=18)
    
ax = plt.subplot(1,2,1) 

if(color_plot):
    plt.matshow(state, fignum=False)
else:
    matshow_bw(state,vmin=1,vmax=2,ax=ax)

o=s.get_ord_prm()
print o
if print_x:
    plt.xlabel('x=%1.2f'%o[0] )

# --------------------------
# run several steps and plot
s.run()

state=s.get_state()
ax = plt.subplot(1,2,2) 

if(color_plot):
    plt.matshow(state, fignum=False)
else:
    matshow_bw(state,vmin=1,vmax=2,ax=ax)

o=s.get_ord_prm()
print o
if print_x:
    plt.xlabel('x=%1.2f'%o[0] )



plt.show()

