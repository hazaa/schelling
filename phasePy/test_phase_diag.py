import schelling
import schell
import numpy
import matplotlib.pylab
import phase_diag


#######################
# main call
#######################

s=schell.Schell()
s.set_params()
s.alloc()                       #does nothing
s.set_params(['density',0.9])   #important!
p = ['frac', numpy.linspace(0.1,0.9,num=20)]
pd = phase_diag.Phase_diag()          
pd.init(s,p)
pd.run()
pd.plot()
pd.save()
pd.free()

# If you want to load from a file:
#pd = phase_diag.Phase_diag()          
#pd.load()
