 # -*- coding: utf-8

#################################################################
# Copyright 2012 Aurelien Hazan
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 
# 
#################################################################


import schelling
import scipy
import matplotlib.pylab as plt
import rnd_sim              
import string

class Schell(rnd_sim.Rnd_sim):
    def __init__(self):
        self.sch = schelling.SchellingNaive()
        self.name_ord_prm=()
        self.nb_ord_prm=[]
        self.stepMax = []
        self.iterMax= []
        self.nBlue = []
        self.nRed = []
        self.nGreen=[]
        self.frac = []
        self.width= []
        self.height=[]
        self.density = []
        self.uMoveIfHappy= []
        self.uMoveIfUnhappy= []
        self.iterMaxRandomPosition= []
        self.tol=[]
        self.stepBurn = []
        self.pChange= []
        self.trigTime =[]

    def set_params(self, t=[]): 
        if len(t)==0:
            self.name_ord_prm=('interface density', 'susceptibility', \
                                   'specific heat')
            self.nb_ord_prm= len(self.name_ord_prm) # output dimension
            self.stepMax = 1000
            self.iterMax= 30
            self.nBlue = 330
            self.nRed = 330
            self.nGreen=190
            self.width= 50
            self.height=50
            self.uMoveIfHappy= 0.0001
            self.uMoveIfUnhappy= 0.2
            self.iterMaxRandomPosition= 1000
            self.tol=0.3
            self.stepBurn = 500
            self.pChange= 0.05
            self.trigTime =200
            self.frac = float(self.nGreen)/  \
                float(self.nBlue+self.nRed+self.nGreen)
            self.density = float(self.nBlue+self.nRed+self.nGreen)/ \
                float(self.width*self.height)            
        else:
            self.parse_params(t)
            
        # compute number of agents    
        self.nBlue = int(scipy.floor(self.density*(self.width*self.height)/ \
                             (2+2*self.frac/(1-self.frac) ) ));
        self.nRed = self.nBlue;
        self.nGreen = int(scipy.floor( 2 *self.frac * self.nBlue/    \
                                        (1-self.frac)  ))
         # check 
        if( self.nBlue<=0 or self.nRed<=0):
            print self.frac, self.density, self.nBlue, self.nRed
            raise ValueError('nBlue or nRed invalid')


        # recompute correct value    
        self.frac = float(self.nGreen)/  \
            float(self.nBlue+self.nRed+self.nGreen)
        self.density = float(self.nBlue+self.nRed+self.nGreen)/ \
            float(self.width*self.height)            
    
        # copy parameters to c++ simulator   
        self.sch.init(self.stepMax, self.iterMax, self.nBlue, self.nRed,
                      self.nGreen, self.width, self.height, self.uMoveIfHappy,
                      self.uMoveIfUnhappy, self.pChange, self.trigTime,
                      self.iterMaxRandomPosition,  self.tol, self.stepBurn)
    

    def parse_params(self,t): 
        l= len(t)
        if( l%2 !=0):
            raise ValueError('size is not multiple of 2')
        for i in range(l/2):
            pname = t[2*i] 
            pval = t[2*i+1]
            cmd = string.join(['self.', pname, ' = pval'],sep='')
            if( hasattr(self,pname)==False ):
                raise AttributeError('schell has no such attribute')
            exec(cmd)               


    def alloc(self): pass

        
    def free(self): 
        self.sch.clear()
    
    def single_run(self, n):
        self.sch.singleRun(n)

    def run(self):
        self.sch.fullRun()

    def get_ord_prm_name(self): 
        return self.name_ord_prm

    def get_ord_prm(self): 
        return [self.sch.getSegregation(), 
                self.sch.getSusceptibility(), 
                self.sch.getSpecificHeat() ]

    def get_state_flat(self):
        return scipy.array(self.sch.getStateFlat())  

    def get_state(self):
        flat_state = self.get_state_flat()
        s = scipy.zeros((self.width,self.height))
        for i in range(self.width):
            for j in range(self.height):
                s[i,j]= flat_state[ i*self.width+j ]

        return s

    def plot(self):    
        print 'param ord=', self.get_ord_prm()
        
        # init
        nr = 3
        nc = 1
        plt.ion()
        plt.clf()

        # -- SEGREGATION --
        # get sample path, mean, var
        time = scipy.arange(self.stepMax)
        sample_path = scipy.array(self.sch.getSegregationRun())
        mean = scipy.array(self.sch.getSegregationRecMean())
        std = scipy.array(scipy.sqrt( self.sch.getSegregationRecVar()))
        dwn = mean - std
        up  = mean + std

        # plot sample path, mean, var
        self.fig1 = plt.figure(1)
        
        plt.subplot(nr,nc, 1)
        tit = 'tolerance= %(mytol)2.2f  density=%(mydens)2.2f  frac=%(frac)2.2f'% { "mytol": self.tol, "mydens": self.density, "frac":self.frac }
        plt.title(tit)

        plt.plot(time, mean, 'r', linewidth=2, label='mean' )
        plt.plot(time, sample_path, linewidth=1, label='sample' )
        plt.plot(time, dwn, 'r--',time,up, 'r--', label='std' )
        plt.axis([0, self.stepMax, 0, 1])
        plt.xlabel('steps')
        plt.ylabel('interface density')
        plt.legend()
        
        # plot standard deviation of order parameter
        # and time-averaged 
        averaged_std = scipy.array(scipy.sqrt( self.sch.getSusceptibility()))
        plt.subplot(nr,nc, 2)
        plt.plot(time, std,    \
                               time, averaged_std * scipy.ones(self.stepMax), 'r--' )
        plt.xlabel('steps')
        plt.ylabel('std(s)')

        # -- ENERGY --
        # get sample path, mean, var
        sample_path = scipy.array(self.sch.getEnergyRun())
        mean = scipy.array(self.sch.getEnergyRecMean())
        std = scipy.array(scipy.sqrt( self.sch.getEnergyRecVar()))
        dwn = mean - std
        up  = mean + std

        # plot sample path, mean, var        
        plt.subplot(nr,nc, 3)
        
        plt.plot(time, mean, 'r', linewidth=2, label='mean' )
        plt.plot(time, sample_path, linewidth=1, label='sample' )
        plt.plot(time, dwn, 'r--',time,up, 'r--', label='std' )
        plt.axis([0, self.stepMax, min(mean) , max(mean) ])
        plt.xlabel('steps')
        plt.ylabel('energy (BC)')
        plt.legend()
