 # -*- coding: utf-8

#################################################################
# Copyright 2012 Aurelien Hazan
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
# 
#################################################################


# 
# abstract class
#
class Rnd_sim:

    #def __init__(self): pass

    def set_params(self, t=[]): pass

    def alloc(self): pass
        
    def free(self): pass

    def run(self): pass

    def get_ord_prm(self): pass

    def get_ord_prm_name(self): pass

    def get_sim_params(self):
        # return the list of numeric parameters.
        # this is used to record datasets with full simulation configuration
        # (e.g. in phase_diag.py)
        # NB: only numeric and string params are concerned, because of hdf5 
        # format which doesn't natively support lists, tuples, etc..
        param_list=[]
        for attr, value in self.__dict__.iteritems():           

            if(isinstance(value,int) or isinstance(value,long)  
               or isinstance(value,float) or isinstance(value,str) ):
                param_list.append(attr);
                param_list.append(value);

        return param_list
