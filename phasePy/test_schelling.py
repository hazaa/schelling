import schelling
import schell
import scipy
import matplotlib.pylab


#########################################
#
# using python class
#
#########################################
def test_class():
    s=schell.Schell()
    # set params
    s.set_params()
    s.set_params(['width',30,'height',30])   
    s.sch.printConfig()
    print s.frac, s.density

    s.set_params(['frac',0.1])   
    s.sch.printConfig()
    print s.frac, s.density

    s.set_params(['density',0.9])   
    s.sch.printConfig()
    print s.frac, s.density
    # run and plot
    s.run()
    s.plot()

#########################################
#
# direct creation of c++ objet
#
#########################################
def test_cpp():
    # params
    stepMax = 1000
    iterMax= 10
    nBlue = 330
    nRed = 320
    nGreen= 190
    frac = nGreen/(nBlue+nRed+nGreen)
    width= 30
    height= 30
    uMoveIfHappy=  0.0001
    uMoveIfUnhappy= 0.2 
    iterMaxRandomPosition= 1000
    tol=0.3
    stepBurn = 100
    density = (nBlue+nRed+nGreen)/(width*height)
    pChange= 0.05
    trigTime = 500
    # init and run
    sch = schelling.SchellingNaive()
    sch.init(stepMax, iterMax, nBlue, nRed, nGreen, width, height, 
             uMoveIfHappy, uMoveIfUnhappy, 
             pChange, trigTime,
             iterMaxRandomPosition,  tol, stepBurn)
    sch.fullRun()
    # get sample path, mean, var
    sample_path = sch.getSegregationRun()
    recmean =sch.getSegregationRecMean()
#recstd = scipy.sqrt( sch.getSegregationRecVar())
#std_dwn = recmean - recstd/2
#std_up  = recmean + recstd/2
#m = min(sample_path,std_dwn,std_up)
#M = max(sample_path,std_dwn,std_up)
# plot
    matplotlib.pyplot.ion()
    time = scipy.arange(stepMax)
    matplotlib.pyplot.plot(time, recmean)
#          ylim=c(m,M) );
    matplotlib.pyplot.xlabel('steps')
    matplotlib.pyplot.ylabel('interface density')



#########################################
#
# main call
#
#########################################
test_class()
