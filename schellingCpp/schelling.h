/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

#ifndef _SCHELLING_H
#define _SCHELLING_H

#include <iostream>
#include <vector>
#include <list>

using namespace std;

class Env;
class InteractionMatrixLine;
class Agent;

// ------------------------------------------------
class Position{
 public:
};

//-----------------------------------------------------------
//   INTERACTION CLASSES   
//-----------------------------------------------------------
class AbstractInteraction{
  
public:
  virtual void perform(Env* e, Agent* source, Agent* target)=0 ;
};


class DegenerateInteraction :public AbstractInteraction{
  
public:
  
};


class InteractionMatrixLine{
 private:
  InteractionMatrixLine( const InteractionMatrixLine& copy ); // DISABLED
  list<AbstractInteraction*> lstInterac;
  list<int> lstPriority;
public:
  InteractionMatrixLine();
  void add(DegenerateInteraction* i, int p);
  AbstractInteraction* getInteraction();
  ~InteractionMatrixLine();
};


//-----------------------------------------------------------
//   AGENT CLASSES   
//-----------------------------------------------------------

class PhysicalHalo{
 private:
  int distance;
 public:
  PhysicalHalo(int d){ distance=d;}
};


class Agent{
public:
  PhysicalHalo* halo;
  InteractionMatrixLine* iml;
  Position* p; // setPosition is implemented in derived class

  //Agent(){  }
  ~Agent();
  Position* getPosition();
  virtual void setPosition(Position* pos);
  virtual void perceive(Env* e, vector<Agent*>* neighbors){}
  void setInteractionMatrixLine(InteractionMatrixLine* iml );
  InteractionMatrixLine*  getInteractionMatrixLine();
  void setHalo(PhysicalHalo* h);
  PhysicalHalo* getHalo();
};
 

//-----------------------------------------------------------
//   ENVIRONMENT CLASSES   
//-----------------------------------------------------------

class Env{
  
public:
  vector<Agent*> vecAgentPtr;

public:
  //Env(){} // car Env n'est pas pure abstract; sinon link error
  void pushAgentPtr(Agent* a);
  void reserveAgentPtr(int n);
  virtual bool canBePutAt(Agent* a, Position* p)=0;
  //virtual list<Agent> getAgents(Position p)=0;
  virtual Agent* getAgent(Position* p)=0;
  virtual void putAgent(Agent* a, Position* p)=0;
  virtual void removeAgent(Agent* a, Position* p)=0;
  virtual void getNeighborhoodInHalo(Agent* agent, PhysicalHalo* halo,
				     vector<Agent*>* neighbors)=0; 
  
};



//-----------------------------------------------------------
//   SIMULATION CLASSES   
//-----------------------------------------------------------


class AgentOrderingPolicy{
public:
  //REMOVE
  //virtual void reorder(list<Agent*>* l)=0;
  virtual void reorder(vector<Agent*>* v)=0;
};


class BasicAgentOrderingPolicy: public AgentOrderingPolicy{
public:
  //REMOVE
  //void reorder(list<Agent*>* l);
  void reorder(vector<Agent*>* v);
};


// ------------------------------------------------
class SimulationCore{  // NOTE: pas abstract; comment faire constructeur ?
 protected: 
  AgentOrderingPolicy* orderPolicy;
  vector<Agent*>* neighbors;
  Env* environment;
public:
  // Env est abstrait, on ne peut déclarer que des pointeurs	
  SimulationCore (AgentOrderingPolicy* p, Env* e)
    : orderPolicy(p), environment(e){
    neighbors=new vector<Agent*>; // ??????
  } 
  ~SimulationCore(){
    delete neighbors;
  }
  void initialize();
  void step();
};

// ------------------------------------------------

/* 
	Schelling simulation abstract class
*/

class Schelling{ // is pure abstract class

  
 protected:
  // protected so that it can be accessed by derived class 
  Env* e;
  AgentOrderingPolicy* orderPolicy;
  SimulationCore* simcore;
  
  /* functions */
  virtual void resetState()=0;  // reset between (e.g. between two mc runs)
  
public:
  /* INIT: find a way to declare init without all the 
     arguments, that are specific to the implementation
     (or pass an argument structure rather than the list of args)
       
    virtual void init( int stepMax, int iterMax, 
		     int nBlue,  int nRed, int nGreen,  
		     int width,  int height, 
		     double uMoveIfHappy,  double uMoveIfUnhappy, 
		     int iterMaxRandomPosition, double tol,
		     int stepBurn) = 0;   
  */ 
  virtual void singleRun(int s) = 0;	
  virtual void fullRun() = 0;	     
  //virtual void getState(vector<vector<int> >* a)=0;
  virtual vector<vector<int> > getState()=0;
  //virtual void getSegregation(vector<double>* s)=0;
  virtual double getSegregation()=0;
};




#endif
