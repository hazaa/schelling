/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

#ifndef _REC_MEAN_VAR_H
#define _REC_MEAN_VAR_H

/*
  recursive mean and variance
  @param mean_new returns the new mean
  @param m2_new returns the new m2
  @param mean in the old mean  
  @param m2 is the old m2
  @param x_new is the new data used to compute mean_new and m2_new
  @param n is the time step
*/
void recursive_mean_var( double *mean_new, double *M2_new, double mean, double M2, double x_new, int n );

#endif
