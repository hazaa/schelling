/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

class bar{
private :
  int* i;
  
public:
  bar(){
    cout<<"in bar ctor"<<endl;
    i=new int;
    *i = 0;
  }
  bar( const bar&){
    cout<<"in bar cpy ctor"<<endl;
    i=new int;
    *i = 0;
  }
  
  ~bar(){ 
    cout<<"in bar dtor"<<endl;
    delete i;}
  void set(int ii){*i=ii;}
  int get(){ return *i;}
  
};



class foo{
public:
  bar *b;
  
public:
  foo(){ cout<<"in foo ctor"<<endl;
    b = new bar(); }
  
  foo( const foo&){
    cout<<"in foo cpy ctor"<<endl;
    b = new bar();
  }
  
  ~foo(){ 
    cout<<"in foo dtor"<<endl;
    delete b;}
  foo* getptr() {return this;}
  void printb(){ cout<<"b="<<b->get()<<endl;}

};

void dofoo(foo *f, int i){
  bar *b=  f->b;
  b->set(i);
}
