/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

#include "schelling_naive.h"


#define MAX_COLOR 10
#define COL_BLU 1
#define COL_RED 2

int DEBUG=0;
#define ORDER_PARAM 1

//-----------------------------------------------------------
//   POSITION CLASSES   
//-----------------------------------------------------------

PositionNaive::PositionNaive(){
  x=0;
  y=0;
}

PositionNaive::PositionNaive( const PositionNaive& copy){
  x = copy.x;
  y = copy.y;
}

//-----------------------------------------------------------
//   ENVIRONMENT CLASSES   
//-----------------------------------------------------------

EnvNaive::EnvNaive(int nr, int nc){
  nrow = nr;
  ncol= nc;
  theGrid = new Agent*[nrow*ncol];

  resetTheGrid();    
}

EnvNaive::~EnvNaive(){

  // clear theGrid
  delete theGrid;
  
  // clear lstAgent
  //list<Agent*>::iterator it;
  //vector<Agent*>::iterator it;
  
  // REMOVE
  //for ( it = vecAgent.begin();it!=vecAgent.end(); ++it) 
  //  delete (*it);
  
}

bool EnvNaive::canBePutAt(Agent* a, Position* p){
  
  // remark that a is not used in this function

  Agent* b;
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  b = theGrid[(pNaive->x)*nrow + pNaive->y];
  
  if(b==NULL)
    return true;
  else
    return false;  
}


Agent* EnvNaive::getAgent(Position *p){
  
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  Agent* a= theGrid[(pNaive->x)*nrow + pNaive->y];
  
  return a;
}


// putAgent
// la grille pointe vers l'agent
void EnvNaive::putAgent(Agent* a, Position* p){
  
  // set position
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  theGrid[(pNaive->x)*nrow + pNaive->y] = a;
  
  // debug
  if(DEBUG>1)
    cout<<"putAgent:"<< "theGrid[" << pNaive->x <<"]["<<pNaive->y <<"]"<<endl;
  
}


// removeAgent
// la grille pointe vers NULL
void EnvNaive::removeAgent(Agent* a, Position* p){
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  
  if(DEBUG>1)
    cout<<"removeAgent:"<< "theGrid[" << pNaive->x <<"]["<<pNaive->y <<"]"<<endl;
  
  theGrid[(pNaive->x)*nrow + pNaive->y] = NULL;
}

void EnvNaive::getNeighborhoodInHalo(Agent* agent, PhysicalHalo* halo,
				     vector<Agent*>* neighbors){
  // get agent position
  PositionNaive* pn = static_cast<PositionNaive*>(agent->getPosition());
  Agent* a;

  // circle around the position
  for(int dx=-1; dx<=1;dx++){
    for(int dy=-1; dy<=1;dy++)
      {
	if(!(dx==0 && dy==0))
	  {
	    // add offset
	    int new_x = pn->x + dx;
	    int new_y = pn->y + dy;
	    // enforce toroidal geometry
	    if(new_x==nrow){new_x=0;}
	    if(new_x==-1){new_x=nrow-1;}
	   
	    if(new_y==ncol){new_y=0;}
	    if(new_y==-1){new_y=ncol-1;}
	    
	    // push non-null pointer
	    a = theGrid[new_x*nrow + new_y];
	    if(a !=NULL)
	      neighbors->push_back(a);
	    
	  }
      }
  }
  
}

void EnvNaive::resetTheGrid(){
  
  for (int i=0; i<nrow;i++)
    for (int j=0; j<ncol;j++)
      theGrid[ i*nrow+j ]=NULL;
  
}


int EnvNaive::getNrow()
{
  return nrow;
}
int EnvNaive::getNcol()
{
  return ncol;
}



//-----------------------------------------------------------
//   AGENT CLASSES   
//-----------------------------------------------------------

/*Blue::Blue(){
  PositionNaive *pn=new PositionNaive;
  pn->x =0;
  pn->y =0;

  p = static_cast<Position*>(pn);
  }*/

/*Blue::Blue(int c){
  PositionNaive *pn=new PositionNaive;
  pn->x =0;
  pn->y =0;
  p = static_cast<Position*>(pn);
  color=c;
  }*/

Blue::Blue(int c, InteractionMatrixLine* i, PhysicalHalo* h){
  // position
  PositionNaive *pn=new PositionNaive;
  pn->x =0;
  pn->y =0;
  p = static_cast<Position*>(pn);
  // color
  color=c;
  // halo
  halo = h;
  // interaction
  iml=i;
}

Blue::Blue(const Blue& copy){
  // 
  PositionNaive *pncopy=static_cast<PositionNaive*>(copy.p);  

  // id
  id=copy.id;
  // position
  PositionNaive *pn=new PositionNaive;
  pn->x = pncopy->x;
  pn->y = pncopy->y;
  p = static_cast<Position*>(pn);
  // color
  color=copy.color;
  // halo
  halo = copy.halo;
  // interaction
  iml= copy.iml;
}


Blue::~Blue(){
  
  delete static_cast<PositionNaive*>(p);
  // delete iml ?? non car on voudrait le rendre static
}



void Blue::setPosition(Position* pos){
  PositionNaive *pn = static_cast<PositionNaive*>(p);
  PositionNaive *posn = static_cast<PositionNaive*>(pos);
  pn->x = posn->x;
  pn->y = posn->y;
}

 void Blue::printPosition(){
   PositionNaive *pn = static_cast<PositionNaive*>(p);
   cout<< "x="<< pn->x << " y=" << pn->y;
 }


void Blue::setColor(int c){
  color=c;
}

int Blue::getColor(){
  return color;
}


void Blue::resetNeighborCount(){
  nbLikeAgents=0;
  nbUnlikeAgents=0;
}

void Blue::incrementLikeCount(){
  nbLikeAgents++;
}

void Blue::incrementUnlikeCount(){
  nbUnlikeAgents++;  
}

int Blue::getLikeCount(){
  return nbLikeAgents;
}

int Blue::getUnlikeCount(){
  return nbUnlikeAgents;  
}


void Blue::perceive(Env* e, vector<Agent*>* neighbors){
  
  // init
  resetNeighborCount();
  int nNeighbor = neighbors->size();
  vector<Agent*>::iterator it;
  
  // count neighbors that have the same color and those who don't
  Agent *a;
  Blue* b;
  
  for ( it = neighbors->begin();it!=neighbors->end(); ++it) 
    {
      a = *it;
      
      // !!!! PROBLEME: ici il faut connaître la classe de l'agent!!
      b = static_cast<Blue*>(a);
      
      if(color==b->getColor())
	incrementLikeCount();
      else
	incrementUnlikeCount();
    }
  
}




//-----------------------------------------------------------
//   INTERACTION CLASSES   
//-----------------------------------------------------------

WanderSchellingInteraction::WanderSchellingInteraction(double t, double uH, double uU, int iMax){
  tol = t;
  uMoveIfHappy=uH;
  uMoveIfUnhappy=uU;
  iterMaxRandomPosition=iMax;
}

void WanderSchellingInteraction::perform(Env* e, Agent* source, Agent* target) 
{
  // get like and unlike
  //  PROBLEM : agent type has to be known !!
  Blue *b=static_cast<Blue*>(source);
  int like = b->getLikeCount();
  int unlike = b->getUnlikeCount();
  int nNeighbor = like+unlike;
  bool happy;

  // compute happiness
  double t;
  if(nNeighbor==0){
    happy=true;
    t=1;
  }	
  else{	
    // attention: differs from jedi's implementation
    t = ((double)unlike)/((double)(like+unlike));// not defined if nNeighbor==0
    if( t<tol )
      happy = true;
    else
      happy=false;	
  }

  // decide to move
  // toss a coin to know if we try to move or not
  double u=(double)rand()/((double)RAND_MAX + 1); // !!!!! TOCHANGE
  bool tryMoveAgent;
  if((happy && u<uMoveIfHappy)||( !happy && u<uMoveIfUnhappy ))
    tryMoveAgent=true;
  else
    tryMoveAgent=false;

  // debug
  /*cout<< "Wander:perform:   t="<<t<< " tol="<<tol<< " happy="<<happy;
  cout<< " u="<<u<< "  uH="<<uMoveIfHappy<< "  uU="<<uMoveIfUnhappy; 
  cout<< " try="<<   tryMoveAgent<<endl;*/
   
  // move
  if(tryMoveAgent)
    moveToRandomPos( b, e, iterMaxRandomPosition);
  
}


PerturbationInteraction::PerturbationInteraction(double p,int t){
  pChange = p;
  trigTime =t;
}

void PerturbationInteraction::perform(Env* e, Agent* source, Agent* target) 
{

  //  PROBLEM : agent type has to be known !!
  Blue *b=static_cast<Blue*>(source);

  // get simulation step
  EnvNaive* en= static_cast<EnvNaive*>(e);  
  int step = en->simstep ;
  
  // random color switch
  double u=(double)rand()/((double)RAND_MAX + 1); // !!!!! TOCHANGE
  if( step> trigTime && u<pChange)
    {
      int c= b->getColor();

      if( c==COL_BLU)
	b->setColor(COL_RED);
      else{
	if( c ==COL_RED)
	  b->setColor(COL_BLU);
	else{
	  cout << "color="<<c<<endl;
	  throw "unsupported color" ;//exception();
	}
      }
      //cout << "step=" << step << " trigTime="<<trigTime<<" u="<<u << " pChange="<<pChange<<" old_c="<<c<<" new_c="<<b->getColor()<< endl;
    }
}

//-----------------------------------------------------------
//   SCHELLING_NAIVE CLASSES   
//-----------------------------------------------------------

SchellingNaive::SchellingNaive(){
    iter = 0 ;
    step = 0;
}



SchellingNaive::~SchellingNaive(){
  // SEEMS NOT TO BE CALLED (WHEN DOING RM) ???
  if(DEBUG)
    cout << "entering destructor" << endl;
  
}


void SchellingNaive::clear(){
  // clear environment
  cout << "clear environment" << endl;
  delete this->e;
  
  // clear agents
  // is done in environment

  // order parameters internals
  delete bc_s;
  delete bc_n;

  // order parameter state matrix
  for (int i=0; i<width; i++)
    delete state_matrix[i];
  delete state_matrix;


  // sim core
  cout << "clear simcore" << endl;
  delete simcore;
}




void SchellingNaive::init(  int sM, int iM, 
			    int nb,  int nr, int ng,  
			    int w,  int h, 
			    double uH,  double uU, 
			    double pC, int tT,
			    int itM, double t, int sB){
  // copy params
  stepMax = sM;
  iterMax=iM;
  nBlue =nb;
  nRed = nr;
  nGreen=ng;
  width=w;
  height= h;
  uMoveIfHappy=uH;
  uMoveIfUnhappy=uU; 
  double pChange = pC;
  int trigTime = tT;
  iterMaxRandomPosition=itM;
  tol=t;
  stepBurn= sB;

  // check arguments 
  if( stepMax<0 | iterMax <0 | nBlue <0 | nRed<0 | nGreen<0
      | width<0 | height<0 | uMoveIfHappy<0.0 | uMoveIfHappy>1.0 
      | uMoveIfUnhappy<0.0 |  uMoveIfUnhappy>1.0 
      | pChange <0.0 | pChange>1.0 
      | trigTime<0 | iterMaxRandomPosition<0
      | stepBurn<0 | stepMax<=stepBurn  )
    throw "invalid argument" ;//exception();
  
  // create environment
  this->e = new EnvNaive(width, height);
  
  // init random position generation
  srand(time(NULL));		// TODO: get seed as argument
  
  // create interactions
  InteractionMatrixLine* iml_bluered=new InteractionMatrixLine;
  InteractionMatrixLine* iml_green=new InteractionMatrixLine;

  WanderSchellingInteraction* wi = new WanderSchellingInteraction(t,
								  uH,uU, 
								  iterMaxRandomPosition);
  PerturbationInteraction* pi = new PerturbationInteraction(pChange, trigTime);

  int priority = 1;
  iml_bluered->add(static_cast<DegenerateInteraction*>(wi),priority);
  iml_green->add(static_cast<DegenerateInteraction*>(pi),priority);
  int distance=1;
  PhysicalHalo* halo= new PhysicalHalo(distance);
  int color=COL_BLU;
  
  // create agent and pointer
  Blue br(color, iml_bluered, halo);
  Blue g(color, iml_green, halo);
  
  Blue *b_ptr;
  EnvNaive* en= static_cast<EnvNaive*>(this->e);  

  // memory allocation:  for agents and agent pointers 
  en->vecBlue.reserve(nBlue+nRed+nGreen);
  e->reserveAgentPtr(nBlue+nRed+nGreen);
  
  // memory allocation:  for order parameters (segregation related)
  segregationStep.resize(stepMax);         // one per simulation step
  segregationRecMean.resize(stepMax);         // one per simulation step
  segregationRecVar.resize(stepMax);         // one per simulation step
  segregationMeanWrtStep.resize(iterMax);  // mean over steps
  
  // initialization for recursive computation of segregation moments
  for(int i=0; i<stepMax; i++){
      segregationRecMean.at(i) = 0.0;
      segregationRecVar.at(i) = 0.0;
    }

  // memory allocation:  for order parameters (energy related)
  energyStep.resize(stepMax);
  energyRecMean.resize(stepMax);
  energyRecVar.resize(stepMax);
  
  // initialization for recursive computation of energy moments
  for(int i=0; i<stepMax; i++){
    energyRecMean.at(i) = 0.0;
    energyRecVar.at(i) = 0.0;
    }


  // allocate memory for state integer encoding
  stateFlat.resize(width*height);         // one per simulation step
  for(int i=0; i<width*height; i++)
    stateFlat.at(i) =0;
  
  state_matrix = new int*[width];
  for (int i=0; i<width; i++)
    state_matrix[i] = new int[height];
  
  // initialize external order parameter computation
  //     blume capel
  bc_ndim = 2 ; 
  bc_nneighbour=4;
  bc_n = new neighbour_2D(bc_nneighbour,width,height);
  bc_s = new state_2D(state_matrix, width, height, COL_RED, COL_BLU);

  if(DEBUG>1){
    cout<<" --- INIT --------" <<endl;
    cout<<"vecBlue.capacity()="<<  en->vecBlue.capacity() <<endl;
    cout<<"vecAgentPtr.capacity()="<< e-> vecAgentPtr.capacity() <<endl;
  }
  
  // main loop
  for(int i=0; i<(nBlue+nRed+nGreen); i++)
    {   
      // --- depends on the type of the agent --- 
      // push agent object to EnvNaive.vecBlue
      if(0<=i && i<nBlue ){
	en->vecBlue.push_back(br);  // push bluered
	//get pointer to agent
	b_ptr = &(en->vecBlue[i]);
	// set color
	b_ptr->setColor(COL_BLU);
      }
      else{
	if(nBlue<=i && i<nBlue+nRed )
	  {
	    en->vecBlue.push_back(br);
	    //get pointer to agent
	    b_ptr = &(en->vecBlue[i]);  // push bluered
	    // set color
	    b_ptr->setColor(COL_RED);
	    
	  }
	else{
	  if(nBlue+nRed<=i && i<nBlue+nRed+nGreen )
	    {
	      en->vecBlue.push_back(g); // push green
	      //get pointer to agent
	      b_ptr = &(en->vecBlue[i]);
	      // set color
	      if( float(rand())/float(RAND_MAX)<0.5)
		b_ptr->setColor(COL_BLU);
	      else
		b_ptr->setColor(COL_RED);
	    }
	  else
	    throw exception();
	}
      }   
      
      // --- doesn't depend on the type of the agent --- 
      // set id
      b_ptr->id =i;
 
      // push pointer to Env.vecAgentPtr
      e->pushAgentPtr( static_cast<Agent*>(b_ptr) );
      
      
      if(DEBUG>1)
	cout << "i="<<i<<" b_ptr="<<b_ptr<<" e-> vecAgentPtr[i]="<< e-> vecAgentPtr[i]<<endl;
      
      // move agent to random position
      moveToRandomPos( b_ptr, e, iterMaxRandomPosition);
      
    }
  
  // create agent ordering policy
  orderPolicy=new BasicAgentOrderingPolicy;
  
  // create simulation core
  SimulationCoreNaive *sc = new SimulationCoreNaive(static_cast<AgentOrderingPolicy*>(orderPolicy), static_cast<Env*>(e));
  sc->initialize();
  simcore=static_cast<SimulationCore*>(sc); 
  
  
}


void SchellingNaive::checkState(){

  // check agent list
  EnvNaive* en= static_cast<EnvNaive*>(e);  
  vector<Blue>::iterator it;
  vector<Agent*>::iterator it_ptr = e->vecAgentPtr.begin();
  Blue *b;
  Position *p;
  PositionNaive *pn;
  int i=0;
  
  for( it= en->vecBlue.begin(); it != en->vecBlue.end(); ++it, ++it_ptr, i++ )
    {
      // from EnvNaive:vecBlue
      pn = static_cast<PositionNaive*> ((*it).getPosition());
      cout <<"i="<<i<< " id="<< (*it).id << " x="<< pn->x << " y="<< pn->y;
      // from Env:vecAgentPtr
      cout <<"\t"<<(*it_ptr)<< " id="<< (static_cast<Blue*>(*it_ptr))->id ; 
      p = (*it_ptr)->getPosition();
      pn = static_cast<PositionNaive*>(p);
      cout<<" x="<< pn->x  << " y="<< pn->y << endl;
    }
  // check all agents have been enumerated
  

  // check theGrid
  
}

void SchellingNaive::resetState(){

  // reset the grid
  EnvNaive *en = static_cast<EnvNaive*>(e);
  en->resetTheGrid();
  Blue *b_ptr;
  
  // dummy position
  PositionNaive* pn = new PositionNaive;
  Position* p=static_cast<Position*>(pn);

 
  // main loop
  for(int i=0; i<(nBlue+nRed+nGreen); i++)
    {   
      // get agent ptr
      b_ptr = &(en->vecBlue[i]);
      
      // set internal position to 0,0
      b_ptr->setPosition(p);
      
      // move agent to random position
      moveToRandomPos( b_ptr, e, iterMaxRandomPosition);    
    }
  
  //free
  delete pn;
  
}


void SchellingNaive::singleRun(int s){
  for( int ss =0; ss<s; ss++)
    {
      step++;
      simcore->step();
      
      // make a copy of step in env
      EnvNaive* en= static_cast<EnvNaive*>(this->e);  
      en->simstep = step;

    }

  // copy internal state to stateFlat
  copyStateFlat();

}	

void SchellingNaive::fullRun(){
  for( iter=0; iter<iterMax; iter++){
    
    // run one iteration
    for( step=0; step<stepMax; step++){           
      simcore->step();    
      endOfStep();
      
      // make a copy of step in env
      EnvNaive* en= static_cast<EnvNaive*>(this->e);  
      en->simstep = step;

      if(DEBUG>1)
	cout<<"fullRun:iter="<<iter<<" step="<<step<<endl;
    }
    
    endOfRun();
    resetState();
  }
  
  endOfIter();

}	     

void SchellingNaive::endOfStep(){

  // initialize recursive estimation
  double order_param=-1.0;
  double mean_new;
  double var_new;
  
  // copy state
  getState(state_matrix);
  
  // order parameters:  blume-capel energy
  int nsites= width*height;
  int ndim=2;
  double K=1.0;               // TODO: set at initialization
  bc_s->update_data( state_matrix);
  order_param= energy_blume_capel( static_cast<state*>(bc_s),
				  static_cast<neighbour*>(bc_n),
				  nsites, ndim, K);
  energyStep.at(step) = order_param;

  // recursive mean and variance wrt iteration, at each time step
  // (cf http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance)
  recursive_mean_var( &mean_new, &var_new,
  		      energyRecMean.at(step), energyRecVar.at(step), 
		      order_param,
   		      iter+1); // Attention: depends on iter!
  energyRecMean.at(step) = mean_new;
  energyRecVar.at(step) = var_new;

  
  //  order parameters:  segregation 
  int ncolor = 2; // blue and red
  int *nagent = new int[ncolor];

  switch(ORDER_PARAM){
  case 0:
    nagent[0]=nBlue;
    nagent[1]=nRed;
    
    order_param = hk_wrap(state_matrix, width, height, ncolor, nagent);
    break;
  case 1:
    order_param = iface_dens(state_matrix, width, height, ncolor, nagent);
    break;
  default:
    throw exception();
  }
  
  segregationStep.at(step) = order_param;         // one per simulation step

  // recursive mean and variance wrt iteration, at each time step
  // (cf http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance)
  recursive_mean_var( &mean_new, &var_new,
  		      segregationRecMean.at(step), segregationRecVar.at(step), 
		      order_param,
   		      iter+1); // Attention: depends on iter!
  segregationRecMean.at(step) = mean_new;
  segregationRecVar.at(step) = var_new;

  // free memory
  delete nagent;
  

}


void SchellingNaive::endOfRun(){
  // compute average order parameter over time steps
  /* BUG!!! DOESN'T WORK
    double m= accumulate( segregationStep.begin()+stepBurn, 
			segregationStep.end(), 
			0);*/
  double m=0.0;
  std::vector<double>::iterator it = segregationStep.begin()+stepBurn ;
  for(it = segregationStep.begin()+stepBurn; it!=segregationStep.end(); it++)
    m += (*it);

  // compute length without burn
  int len = stepMax-stepBurn; // check size
  if(len<=0) throw exception();

  m = m/((double) len); 
  segregationMeanWrtStep.at(iter) = m;

  // copy internal state to stateFlat
  copyStateFlat();

  // debug
  if(DEBUG){
    cout << "endOfRun: (iter= "<<iter<<") ";
    for(int i=0; i<stepMax; i++)
      cout << segregationStep.at(i)<<" ";
    cout << "(segregationMeanWrtStep="<<m<<")"<< endl;
  }
}

void SchellingNaive::endOfIter(){
  // order param: segregation and susceptibility

  // compute mean over iterations of time-averaged order param
  /*!!! BUG: will not compute the right value     !!!!
    segregationMeanWrtIter = accumulate( segregationMeanWrtStep.begin(), 
				       segregationMeanWrtStep.end(), 
				       0);*/
  double m =0.0;
  std::vector<double>::iterator it = segregationMeanWrtStep.begin();
  for(it = segregationMeanWrtStep.begin(); it!=segregationMeanWrtStep.end(); it++)
    m += (*it);
  
  segregationMeanWrtIter = m/((double)iterMax);  

  // susceptibility
  // first normalize recursive variance
  // see http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#On-line_algorithm
  for(int i=0; i<stepMax; i++)
    segregationRecVar.at(i) =   segregationRecVar.at(i)/((double) iterMax);

  susceptibility = 0.0;
  for(it = segregationRecVar.begin()+stepBurn; it!=segregationRecVar.end(); it++)
    susceptibility += (*it);
  int len = stepMax-stepBurn; // check size 
  susceptibility /=  (double)len;  

  // order param: energy and specific heat 
  // first normalize recursive variance
  // then take the average on the last steps
  for(int i=0; i<stepMax; i++)
    energyRecVar.at(i) = energyRecVar.at(i)/((double) iterMax);

  specific_heat = 0.0;
  for(it = energyRecVar.begin()+stepBurn; it!=energyRecVar.end(); it++)
    specific_heat += (*it);
  len = stepMax-stepBurn; // check size 
  specific_heat /=  (double)len;  

      
  // debug
  if(DEBUG){
    cout << "endOfIter: ";
    for(int i=0; i<iterMax; i++)
      cout << segregationMeanWrtStep.at(i)<<" ";
    cout << endl;
  }
}


void SchellingNaive::getState(int **matrix){
  Agent *a;
  Blue *b;
  Position *p;
  PositionNaive *pn=new PositionNaive;

  for(int i=0; i<width; i++)
    for(int j=0; j<height; j++)
      {
	// generate position
	pn->x = i;
	pn->y = j;
	p = static_cast<Position*>(pn);

	// get agent
	a = e->getAgent(p);
	
	// get agent's color
	if(a==NULL)
	  matrix[i][j]=0;
	else{
	  // PROBLEME: ici on doit connaître le type de l'agent
	  b = static_cast<Blue*>(a);
	  matrix[i][j] = b->getColor();
	}
      }
  
  // clear 
  delete pn;
  
}



vector< vector<int> > SchellingNaive::getState(){
  vector< vector<int> > state(width, vector<int>(height,0));
  
  Agent *a;
  Blue *b;
  Position *p;
  PositionNaive *pn=new PositionNaive;

  for(int i=0; i<width; i++)
    for(int j=0; j<height; j++)
      {
	// generate position
	pn->x = i;
	pn->y = j;
	p = static_cast<Position*>(pn);

	// get agent
	a = e->getAgent(p);
	
	// get agent's color
	if(a==NULL)
	  state[i][j]=0;
	else{
	  // PROBLEME: ici on doit connaître le type de l'agent
	  b = static_cast<Blue*>(a);
	  state[i][j] = b->getColor();
	  if(state[i][j] >MAX_COLOR | state[i][j]<0)
	    throw exception();
	}
      }
  
  // clear 
  delete pn;
  
  return state ;
}


void SchellingNaive::copyStateFlat(){
  // convert internal state to flat integer vector stateFlat
  
  Agent *a;
  Blue *b;
  Position *p;
  PositionNaive *pn=new PositionNaive;

  int idx, s;
  
  for(int i=0; i<width; i++)
    for(int j=0; j<height; j++)
      {
	// generate position
	pn->x = i;
	pn->y = j;
	p = static_cast<Position*>(pn);

	// get agent
	a = e->getAgent(p);
	
	// get agent's color
	if(a==NULL)
	  s=0;
	else{
	  // PROBLEME: ici on doit connaître le type de l'agent
	  b = static_cast<Blue*>(a);
	  s = b->getColor();
	  if(s >MAX_COLOR | s<0)
	    throw exception();
	}
	// store
	idx = i*width +j ;
	stateFlat.at( idx  ) = s;
	
      }
  
  // clear 
  delete pn;
  
}

vector<int> SchellingNaive::getStateFlat(){
  return stateFlat;
}



/* segregation getters */

vector<double> SchellingNaive::getSegregationRun(){
  return segregationStep;
}

vector<double> SchellingNaive::getSegregationIter(){
  return segregationMeanWrtStep;
}

vector<double> SchellingNaive::getSegregationRecMean(){
  return segregationRecMean;
}
vector<double> SchellingNaive::getSegregationRecVar(){
  return segregationRecVar;
}


double SchellingNaive::getSegregation(){
  return segregationMeanWrtIter;
}

double SchellingNaive::getSusceptibility(){
  return susceptibility;
}


/* energy getters */

vector<double> SchellingNaive::getEnergyRun(){
  return energyStep;
}

/*vector<double> SchellingNaive::getEnergyIter(){
  return energyMeanWrtStep;
  }*/

vector<double> SchellingNaive::getEnergyRecMean(){
  return energyRecMean;
}
vector<double> SchellingNaive::getEnergyRecVar(){
  return energyRecVar;
}

double SchellingNaive::getSpecificHeat(){
  return specific_heat;
}

/* miscellaneous */

void SchellingNaive::printConfig(){
  cout<<"stepMax="<<stepMax<<" iterMax="<<iterMax<<" nBlue="<<nBlue<<" nRed="<<nRed<<" nGreen="<<nGreen<<endl;
  cout<<"width="<<width<<" heigth="<<height<<endl;
  cout<<"uMoveIfHappy="<<uMoveIfHappy<<" uMoveIfUnhappy="<<uMoveIfUnhappy<<endl;
  cout<<"iterMaxRandomPosition="<<iterMaxRandomPosition<<" tol="<<tol<<endl;
  cout<<"stepBurn="<<stepBurn<<endl;

  EnvNaive *en = static_cast<EnvNaive*>(e);
  vector<Blue>::iterator it;
  int i=0;
  Blue *b_ptr;
  /*for(it = (*v).begin(); it!=(*v).end(); it++, i++)
    //for(int i=0; i<nBlue+nRed; i++)
    {     
      b_ptr = &(*it);
      cout<<i<<" ";
      b_ptr->printPosition();
      cout << endl;
    }
  */
}



//-----------------------------------------------------------
//   SIMULATION CLASSES   
//-----------------------------------------------------------





//-----------------------------------------------------------
//   OTHER FUNCTIONS
//-----------------------------------------------------------


void moveToRandomPos(Blue *b, Env *e, int iterMaxRandomPosition){
  //allocation and initialisation
  Position* old_p = b->getPosition();
  PositionNaive* new_p=new PositionNaive();
  EnvNaive *en=static_cast<EnvNaive*>(e);
  
  bool canBePut;
  int width = en->getNrow();
  int height = en->getNcol();
  int j=0;
  
  // main loop
  do{
    new_p->x= rand()%width ; // watch index: must be in [0,width-1]
    new_p->y= rand()%height ;    

    canBePut = en-> canBePutAt(b, static_cast<Position*>(new_p));
    j++;
  }while ( !canBePut && j< iterMaxRandomPosition);

  // DEBUG
  if(DEBUG>1){
    cout<<"moveToRandomPos: canBePut="<< canBePut;
    cout <<"  new_pos x=";
    if(canBePut)
      cout<< new_p->x  <<" y=" << new_p->y<<endl;
    else
      cout<<" ---- failed ---- "<<endl;
  }    

  // put in environment
  if( canBePut )
    {
      en->removeAgent(b, static_cast<Position*>(old_p));     
      en->putAgent(b, static_cast<Position*>(new_p));
      // copy position pointer in agent 
      b->setPosition(static_cast<Position*>(new_p));
    }
  else
    throw std::exception();
  
  // clear
  delete new_p;
}
