/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/
					   

#include "hk_wrap.h"

double hk_wrap(int** matrix, int nrow, int ncol, int ncolor, int* nagent){
  
  // init & allocation of local matrix
  double segreg = 0.0;
  
  int** local_matrix = new int*[nrow];
  for (int i=0; i<nrow; i++)
    local_matrix[i] = new int[ncol];
  
  
  // main loop 
  for (int icolor=0; icolor<ncolor; icolor++)
    {
      
      // copy into local matrix
      for(int i=0; i<nrow; i++) 
	for(int j=0; j<ncol; j++) 
	  local_matrix[i][j]= matrix[i][j];

      // mask
      int color = icolor+1;
      mask_matrix(local_matrix, nrow, ncol, color);
      
      // Hoshen Kopelman
      int clusters = hoshen_kopelman(local_matrix,nrow,ncol);
      
      // Computer cluster size
      int* clust_vect = new int[clusters];
      cluster_size(local_matrix, nrow, ncol, clust_vect, clusters);
      
      // compute segregation
      segreg += ((double)sum_square(clust_vect,clusters))/((double)nagent[icolor] );
      
      //free memory
      delete clust_vect;
    }
  


  // free memory
  for (int i=0; i<nrow; i++)
    delete local_matrix[i];
  delete local_matrix;
  
  return segreg;
}



