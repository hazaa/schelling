/* ************************************************************
 * Copyright 2012 Aurelien Hazan
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *************************************************************/

#ifndef _SCHELLING_NAIVE_H
#define _SCHELLING_NAIVE_H
 
#include<cstdio>
#include<numeric>
#include<math.h>
#include <iostream>
#include <vector>
#include <cstdlib>

#include "schelling.h"
#include "hk_wrap.h"
#include "../phystat_toolbox/blume_capel/spin_state_2D.h"
#include "../phystat_toolbox/blume_capel/energy_blume_capel.h"
#include "iface_dens.h"
#include "recursive_mean_var.h"


using namespace std;

/* 
   Naive implementation of Schelling abstract class
   TODO:
   - RNG ? 
   - ordering policy ?? fr.lifl.jedi.controllersCore.orderingPolicies.
*/


//-----------------------------------------------------------
//   POSITION CLASSES   
//-----------------------------------------------------------

class PositionNaive: public Position{
 public:
  int x;
  int y;
 public:
  PositionNaive();
  PositionNaive( const PositionNaive& copy);
};

//-----------------------------------------------------------
//   AGENT CLASSES   
//-----------------------------------------------------------


class Blue:public Agent{
private:
  int color;
  int nbLikeAgents;
  int nbUnlikeAgents;
  bool happy;
  
private:
  void resetNeighborCount();
  void incrementLikeCount();
  void incrementUnlikeCount();
  Blue();	// disabled
public:
  int id;
  //Blue();	
  //Blue(int c);
  Blue(int c, InteractionMatrixLine* i, PhysicalHalo* h);
  Blue(const Blue&);
  ~Blue();	
  void setPosition(Position* pos);
  void printPosition();
  void perceive(Env* e,vector<Agent*>* neighbors);
  int getColor();
  void setColor(int c);
  int getLikeCount();
  int getUnlikeCount();
};



//-----------------------------------------------------------
//   ENVIRONMENT CLASSES   
//-----------------------------------------------------------


class EnvNaive: public Env{
  
  Agent** theGrid;

  int nrow; 
  int ncol;

 public:
  int simstep; // step is in class SchellingNaive, but some interactions
               // need the time step, so we make a copy here

  vector<Blue> vecBlue;	 // vector of objects, to enforce contiguous mem alloc	
                         // pointers to agents are stored in Env.vecAgentPtr
                         // to allow polymorphic use

 public:
  //EnvNaive();
  EnvNaive(int nr, int nc);
  ~EnvNaive();

  void resetTheGrid();

  bool canBePutAt(Agent* a, Position* p);
  Agent* getAgent(Position* p);
  void putAgent(Agent* a, Position* p);
  void removeAgent(Agent* a, Position* p);
  void getNeighborhoodInHalo(Agent* agent, PhysicalHalo* halo,
			     vector<Agent*>* neighbors); 
  int getNrow();
  int getNcol();

};

//-----------------------------------------------------------
//   VARIOUS FUNCTIONS   
//-----------------------------------------------------------

void moveToRandomPos(Blue *b, Env *e, int iterMaxRandomPosition);


//-----------------------------------------------------------
//   INTERACTION CLASSES   
//-----------------------------------------------------------

/*
  DESCIPTION:
  a wandering interaction that models the schelling dynamics
  
  CONDITIONS:
  source and target agents
 */
class WanderSchellingInteraction: public DegenerateInteraction{
private:
  double tol;
  double uMoveIfHappy;
  double uMoveIfUnhappy;
  int iterMaxRandomPosition;
public:
  WanderSchellingInteraction(double t , double uH, double uU, int iMax);
  void perform(Env* e, Agent* source, Agent* target) ;
};

/*
  DESCIPTION:
  an interaction where the agent doesn't move but has a fixed
  probability to change its color at each time step

  CONDITIONS:
  source only (no target)
  agent needs a color

 */
class PerturbationInteraction: public DegenerateInteraction{
private:
  double pChange; // probability to change color at each step
  int trigTime; // time when interaction is activated 
public:
  PerturbationInteraction(double p, int tT);
  void perform(Env* e, Agent* source, Agent* target) ;
};


//-----------------------------------------------------------
//   SIMULATION CLASSES   
//-----------------------------------------------------------


class SimulationCoreNaive: public SimulationCore{ 
  
public:
  SimulationCoreNaive (AgentOrderingPolicy* p, Env* e) 
    : SimulationCore(p,e){ }
};




//-----------------------------------------------------------
//   SCHELLING_NAIVE CLASSES   
//-----------------------------------------------------------

class SchellingNaive :public Schelling {

private:
  
  //! simulation parameters 
  
  int stepMax ;               // maximum steps per run
  int step ;                  // current step
  int iterMax ;               // maximum number of monte-carlo simulations
  int iter;              // index of current monte-carlo simulations
  int stepBurn;          // number of steps not considered for averaging
  int nBlue ;	         // number of blue agents
  int nRed ;             // number of red agents
  int nGreen ;           // number of green agents
  int width ;            // grid width
  int height ;	         // grid height
  double uMoveIfHappy ;  // probability of jump if happy 
  double uMoveIfUnhappy ;// probability of jump if unhappy  
  int iterMaxRandomPosition ;// nb of attempts to find a place when jumping
  double tol; // tolerance

  //! order parameters members
  /*!
    integer encoding of the state for order param computation
  */
  int** state_matrix;
  vector<int> stateFlat;
  
  // blume capel energy
  int bc_ndim ;     // dimension
  int bc_nneighbour; // moore neighbourhood
  neighbour_2D *bc_n;
  state_2D *bc_s;

  //!  order parameters storage 
  /*! 
    Segregation-related order parameters:

    Segregation at a given time step and a given iteration. 
    Is reset at the end of each run. Length: stepMax
    
    Recursive mean and variance of segregation (at a given time step
    over iterations). Length: stepMax

    Mean over steps for each iteration. Length: iterMax
    
    Mean of means over iterations. Length: 1
    
    Susceptibility (see Gauvin 09)
  */
  vector<double> segregationStep; 
  vector<double> segregationRecMean;      
  vector<double> segregationRecVar;      
  vector<double> segregationMeanWrtStep;  
  double segregationMeanWrtIter;      
  double susceptibility;

  //!  order parameters' storage 
  /*! 
    Energy and related order parameters (specific heat).
    (see Gauvin 09)
  */
  vector<double> energyStep; 
  vector<double> energyRecMean;      
  vector<double> energyRecVar;      
  double specific_heat;
  

private:
  //! class' internal functions
  void endOfStep();
  void endOfRun();
  void endOfIter();
  void getState(int **matrix);  
  void copyStateFlat();
   
public:
  //! constructor and destructor
  SchellingNaive();
  ~SchellingNaive();

  //! setters 
  void init(  int sM, int iM, int nb,  int nr, int ng,  
	      int w,  int h, double uH,  double uU, 
	      double pC, int tT,
	      int itM, double t, int sB); 
  void singleRun(int s);
  void fullRun();	     
  void clear();	
  void resetState();
  

  //! getters: generic
  vector< vector<int> > getState();
  vector<int> getStateFlat();

  //! getters: segregation
  double getSegregation();
  double getSusceptibility();  
  vector<double> getSegregationRun();
  vector<double> getSegregationIter();
  vector<double> getSegregationRecMean();
  vector<double> getSegregationRecVar();

  //! getters: energy
  vector<double> getEnergyRun();
  vector<double> getEnergyRecMean();
  vector<double> getEnergyRecVar();
  double getSpecificHeat();  
  
  //!  miscellaneous
  void printConfig();
  void checkState();

};




#endif
