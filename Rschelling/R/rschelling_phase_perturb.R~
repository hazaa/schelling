rschelling_phase_perturb<-function(){
        # Usage:
        #   source("schelling_wrap.R")
        #       source("schelling_phase.R")     
        #       l<-schelling_phase()

        # ---------------------
        # parametres de simulation fixes 

	iterMax= 10; 		   # Monte Carlo iterations
        stepMax=1000;                # duree d'une simulation 
        width=30;                  # largeur de la grille
        height=30;                 # hauteur de la grille  

        #    probabilite de saut 
        uMoveIfHappy = 0.0001;     # proba de saut si heureux 
        uMoveIfUnhappy = 0.1;      # proba de saut si malheureux
        iterMaxRandomPosition = 1000; # nombre d'iterations de recherche de 
                                      # nouvelle position si malheureux
	pChange = 0.1;

	trigTime=300;             # déclenchement de perturb (la perturbation
				  # démarre après trigTime)  
        stepBurn=600;             # calcul de segreg (seule les données
				  # après stepBurn sont prises en compte  


	# ---------------------
        # parametres de controle

        #    frac d'agents perturb
        nf = 5;
        f <- seq(from= 0.0, to=0.5,  length=nf);

        #    densite
        nd = 5;
        d <- seq(from= 0.8, to=0.95,  length=nd);
        
        #    tolerance
        nt = 5;
        t <- seq(from= 0.1, to=0.9,  length=nt);    
        
        # stockage
        s_arr = array(0, c(nd,nt,nf));

        # --------------------
        # boucle principale
        for(i_f in 1:nf){
                for(i_d in 1:nd){
  	                for(i_t in 1:nt){	


		        # calcul du nombre d'agents
			frac = f[i_f];			
                        nBlue = floor(d[i_d]*(width*height)/
					(2+2*frac/(1-frac) ) );
			nRed = nBlue;
			nGreen = floor( 2 *frac * nBlue/(1-frac)  )
			  
			# nouvelle simu
			sch<-new(Schelling);

			# initialisation
			sch$init(stepMax, iterMax, nBlue, nRed, nGreen,
				 width, height, 
		  		 uMoveIfHappy, uMoveIfUnhappy, 
				 pChange, trigTime,
				 iterMaxRandomPosition, t[i_t], 
				 stepBurn);

			# experience
			sch$fullRun();	       

			# récupération de s
                        s_arr[i_d,i_t,i_f] = sch$getSegregation();
                       
                        # print
                        messag<-c(" frac=", as.character(f[i_f]),
				  " dens=",as.character(d[i_d]), 
				  "(", as.character(nBlue),
				  ",", as.character(nGreen), ")",
				  " tol=",as.character(t[i_t]),		
                                  " s=", as.character(s_arr[i_d,i_t,i_f]) );
                        print(paste(messag, collapse="") );       

			# clear sim
			remove(sch);

			# partial save
			l<-list(segregation=s_arr, frac=f, dens=d, tol=t);
			save(l, file="phase.Rdata")
			                        }		
				}
                }


# plot
postscript("phase_diag1.eps", horizontal=FALSE, onefile=FALSE,
                  height=8, width=6, pointsize=10)

# densite et tol constantes, frac varie
i_d=3;
i_t=3;
s_vec <- l$segregation[i_d,i_t,];
mysub = paste( c("dens=",as.character(l$dens[i_d]) ,
      	         " tol=",as.character(l$tol[i_t])) ,
      	        collapse="")

plot(l$frac, s_vec , type="l", xlab="frac", ylab= "s",
	     			  ylim = c(0, max(s_vec)),
				  sub=mysub  )

# diag phase
postscript("phase_diag2.eps", horizontal=FALSE, onefile=FALSE,
                  height=8, width=6, pointsize=10)

par(mfrow=c(2, 3))
for(i_f in 1:nf){
	#mfg=c(i_f,1,nf,1)
	S = l$segregation[,,i_f];
	mysub = paste( c("frac=",as.character(l$frac[i_f])) , collapse="")
        image( l$dens, l$tol,
	       S,
	       zlim = c(0,700),
	       xlab="dens", ylab= "tol", 
	       sub = mysub)
	}

dev.off()


}
