#include "schelling.h"


//-----------------------------------------------------------
//   ENVIRONMENT CLASSES   
//-----------------------------------------------------------

void Env::reserveAgentPtr(int n)
{
  vecAgentPtr.reserve(n);
}

void Env::pushAgentPtr(Agent* a)
{
  vecAgentPtr.push_back(a);
}


//-----------------------------------------------------------
//   AGENT CLASSES   
//-----------------------------------------------------------
void Agent::setInteractionMatrixLine(InteractionMatrixLine* i ){
  iml=i;
}

InteractionMatrixLine* Agent::getInteractionMatrixLine()
{
  return iml;
}

void Agent::setHalo(PhysicalHalo* h){
  halo =h;
}

PhysicalHalo* Agent::getHalo(){
  return halo;
}

Position* Agent::getPosition(){
  return p;
}

void Agent::setPosition(Position* pos){
  // must be implemented in derived class because position
  // depends on implementation
}


Agent::~Agent()
{
  //delete iml;
  //delete halo;
  //delete p;
}

//-----------------------------------------------------------
//   INTERACTION CLASSES   
//-----------------------------------------------------------

InteractionMatrixLine::InteractionMatrixLine(){
}


void InteractionMatrixLine::add(DegenerateInteraction* i, int p){
  
  lstInterac.push_front( static_cast<AbstractInteraction*>(i) );
  lstPriority.push_front(p);
}

AbstractInteraction* InteractionMatrixLine::getInteraction(){
  // !!!!!!!!
  // TO CHANGE
  // !!!!!!!!
  AbstractInteraction* ai= lstInterac.front();
  return ai;
}


InteractionMatrixLine::~InteractionMatrixLine(){
  
  AbstractInteraction* ai;
  DegenerateInteraction* di;
  
  /*while(!lstInterac.empty())
    {
      ai= lstInterac.front();
      di= static_cast<DegenerateInteraction*>(ai);
      delete di;
      lstInterac.pop_front();
      }*/
  
}


//-----------------------------------------------------------
//   SIMULATION CLASSES   
//-----------------------------------------------------------


// BasicAgentOrderingPolicy
//
// \brief ne re-ordonne pas les agents.
//
//void BasicAgentOrderingPolicy::reorder(list<Agent*>* l)
void BasicAgentOrderingPolicy::reorder(vector<Agent*>* v)
{
  //cout << "reorder agents"<<endl;
  
}



void SimulationCore::step()
{
  // initialise
  vector<Agent*>::iterator it; 
  Agent* source_agent;
    
  // pre-interaction
  //?????????
  
  // reorder agents
  //orderPolicy->reorder( va );
 
  // main loop over agents
  for ( it = environment->vecAgentPtr.begin() ; it!= environment->vecAgentPtr.end(); ++it)  
    { 
      // check neighbors is empty
      if(!(*neighbors).empty() )
	throw std::exception();

      // l'objet stocké dans la liste est un pointeur vers un agent
      source_agent = *it;
      
      // détermination des voisins
      // NB: cette implémentation est différente ce celle de jedi.
      // cf fr/lifl/jedi/model/Agent.java
      PhysicalHalo* halo = source_agent-> getHalo();
      environment->getNeighborhoodInHalo(source_agent,  halo, neighbors); 
	
      // perception
      source_agent->perceive( environment, neighbors);
      // clear neighborhood
      //(*neighbors).clear();
      neighbors->clear();
      
      // SELECTION DES ACTIONS
      // ??????????
      //  for realizable interaction in interactionline{
      //   agent.interaction(agent, environment)
      // }
      InteractionMatrixLine* iml = source_agent->getInteractionMatrixLine();
      // get interaction
      AbstractInteraction* ai = iml->getInteraction();
      // trigger interaction
      Agent* target_agent=NULL; // TO CHANGE !!!!!!!!!!
      ai->perform(environment,  source_agent, target_agent) ;
    } 

  
  // post-interaction
  //?????????
  

}


