#include "schelling_naive.h"
#include<cstdio>

#define MAX_COLOR 10
int DEBUG=0;

//-----------------------------------------------------------
//   POSITION CLASSES   
//-----------------------------------------------------------

PositionNaive::PositionNaive(){
  x=0;
  y=0;
}

PositionNaive::PositionNaive( const PositionNaive& copy){
  x = copy.x;
  y = copy.y;
}

//-----------------------------------------------------------
//   ENVIRONMENT CLASSES   
//-----------------------------------------------------------

EnvNaive::EnvNaive(int nr, int nc){
  nrow = nr;
  ncol= nc;
  theGrid = new Agent*[nrow*ncol];
    
  // initialize
  for (int i=0; i<nrow;i++)
    for (int j=0; j<ncol;j++)
      theGrid[ i*nrow+j ]=NULL;
}

EnvNaive::~EnvNaive(){

  // clear theGrid
  delete theGrid;
  
  // clear lstAgent
  //list<Agent*>::iterator it;
  //vector<Agent*>::iterator it;
  
  // REMOVE
  //for ( it = vecAgent.begin();it!=vecAgent.end(); ++it) 
  //  delete (*it);
  
}

bool EnvNaive::canBePutAt(Agent* a, Position* p){
  
  // remark that a is not used in this function

  Agent* b;
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  b = theGrid[(pNaive->x)*nrow + pNaive->y];
  
  if(b==NULL)
    return true;
  else
    return false;  
}


Agent* EnvNaive::getAgent(Position *p){
  
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  Agent* a= theGrid[(pNaive->x)*nrow + pNaive->y];
  
  return a;
}


// putAgent
// la grille pointe vers l'agent
void EnvNaive::putAgent(Agent* a, Position* p){
  
  // set position
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  theGrid[(pNaive->x)*nrow + pNaive->y] = a;
  
  // debug
  if(DEBUG)
    cout<<"putAgent:"<< "theGrid[" << pNaive->x <<"]["<<pNaive->y <<"]"<<endl;
  
}


// removeAgent
// la grille pointe vers NULL
void EnvNaive::removeAgent(Agent* a, Position* p){
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  
  if(DEBUG)
    cout<<"removeAgent:"<< "theGrid[" << pNaive->x <<"]["<<pNaive->y <<"]"<<endl;
  
  theGrid[(pNaive->x)*nrow + pNaive->y] = NULL;
}

void EnvNaive::getNeighborhoodInHalo(Agent* agent, PhysicalHalo* halo,
				     list<Agent*>* neighbors){
  // get agent position
  PositionNaive* pn = static_cast<PositionNaive*>(agent->getPosition());
  Agent* a;

  // circle around the position
  for(int dx=-1; dx<=1;dx++){
    for(int dy=-1; dy<=1;dy++)
      {
	if(!(dx==0 && dy==0))
	  {
	    // add offset
	    int new_x = pn->x + dx;
	    int new_y = pn->y + dy;
	    // enforce toroidal geometry
	    if(new_x==nrow){new_x=0;}
	    if(new_x==-1){new_x=nrow-1;}
	   
	    if(new_y==ncol){new_y=0;}
	    if(new_y==-1){new_y=ncol-1;}
	    
	    // push non-null pointer
	    a = theGrid[new_x*nrow + new_y];
	    if(a !=NULL)
	      neighbors->push_front(a);
	    
	  }
      }
  }
  
}

int EnvNaive::getNrow()
{
  return nrow;
}
int EnvNaive::getNcol()
{
  return ncol;
}



//-----------------------------------------------------------
//   AGENT CLASSES   
//-----------------------------------------------------------

/*Blue::Blue(){
  PositionNaive *pn=new PositionNaive;
  pn->x =0;
  pn->y =0;

  p = static_cast<Position*>(pn);
  }*/

/*Blue::Blue(int c){
  PositionNaive *pn=new PositionNaive;
  pn->x =0;
  pn->y =0;
  p = static_cast<Position*>(pn);
  color=c;
  }*/

Blue::Blue(int c, InteractionMatrixLine* i, PhysicalHalo* h){
  // position
  PositionNaive *pn=new PositionNaive;
  pn->x =0;
  pn->y =0;
  p = static_cast<Position*>(pn);
  // color
  color=c;
  // halo
  halo = h;
  // interaction
  iml=i;
}

Blue::Blue(const Blue& copy){
  // 
  PositionNaive *pncopy=static_cast<PositionNaive*>(copy.p);  

  // id
  id=copy.id;
  // position
  PositionNaive *pn=new PositionNaive;
  pn->x = pncopy->x;
  pn->y = pncopy->y;
  p = static_cast<Position*>(pn);
  // color
  color=copy.color;
  // halo
  halo = copy.halo;
  // interaction
  iml= copy.iml;
}


Blue::~Blue(){
  
  delete static_cast<PositionNaive*>(p);
}



void Blue::setPosition(Position* pos){
  PositionNaive *pn = static_cast<PositionNaive*>(p);
  PositionNaive *posn = static_cast<PositionNaive*>(pos);
  pn->x = posn->x;
  pn->y = posn->y;
}

 void Blue::printPosition(){
   PositionNaive *pn = static_cast<PositionNaive*>(p);
   cout<< "x="<< pn->x << " y=" << pn->y;
 }


void Blue::setColor(int c){
  color=c;
}

int Blue::getColor(){
  return color;
}


void Blue::resetNeighborCount(){
  nbLikeAgents=0;
  nbUnlikeAgents=0;
}

void Blue::incrementLikeCount(){
  nbLikeAgents++;
}

void Blue::incrementUnlikeCount(){
  nbUnlikeAgents++;  
}

int Blue::getLikeCount(){
  return nbLikeAgents;
}

int Blue::getUnlikeCount(){
  return nbUnlikeAgents;  
}


void Blue::perceive(Env* e, list<Agent*>* neighbors){
  
  // init
  resetNeighborCount();
  int nNeighbor = (*neighbors).size();
  list<Agent*>::iterator it;
  
  // count neighbors that have the same color and those who don't
  Agent *a;
  Blue* b;
  
  for ( it = (*neighbors).begin();it!=(*neighbors).end(); ++it) 
    {
      a = *it;
      
      // !!!! PROBLEME: ici il faut connaître la classe de l'agent!!
      b = static_cast<Blue*>(a);
      
      if(color==b->getColor())
	incrementLikeCount();
      else
	incrementUnlikeCount();
    }
  
}




//-----------------------------------------------------------
//   INTERACTION CLASSES   
//-----------------------------------------------------------

WanderSchellingInteraction::WanderSchellingInteraction(double t, double uH, double uU, int iMax){
  tol = t;
  uMoveIfHappy=uH;
  uMoveIfUnhappy=uU;
  iterMaxRandomPosition=iMax;
}

void WanderSchellingInteraction::perform(Env* e, Agent* source, Agent* target) 
{
  // get like and unlike
  //  PROBLEM : agent type has to be known !!
  Blue *b=static_cast<Blue*>(source);
  int like = b->getLikeCount();
  int unlike = b->getUnlikeCount();
  int nNeighbor = like+unlike;
  bool happy;

  // compute happiness
  double t;
  if(nNeighbor==0){
    happy=true;
    t=1;
  }	
  else{	
    // attention: differs from jedi's implementation
    t = ((double)unlike)/((double)(like+unlike));// not defined if nNeighbor==0
    if( t<tol )
      happy = true;
    else
      happy=false;	
  }

  // decide to move
  // toss a coin to know if we try to move or not
  double u=(double)rand()/((double)RAND_MAX + 1); // !!!!! TOCHANGE
  bool tryMoveAgent;
  if((happy && u<uMoveIfHappy)||( !happy && u<uMoveIfUnhappy ))
    tryMoveAgent=true;
  else
    tryMoveAgent=false;

  // debug
  /*cout<< "Wander:perform:   t="<<t<< " tol="<<tol<< " happy="<<happy;
  cout<< " u="<<u<< "  uH="<<uMoveIfHappy<< "  uU="<<uMoveIfUnhappy; 
  cout<< " try="<<   tryMoveAgent<<endl;*/
   
  // move
  if(tryMoveAgent)
    moveToRandomPos( b, e, iterMaxRandomPosition);
  
}


//-----------------------------------------------------------
//   SCHELLING_NAIVE CLASSES   
//-----------------------------------------------------------

SchellingNaive::SchellingNaive(){
    iter = 0 ;
    step = 0;
}



SchellingNaive::~SchellingNaive(){
  // SEEMS NOT TO BE CALLED (WHEN DOING RM) ???
  if(DEBUG)
    cout << "entering destructor" << endl;
  
}


void SchellingNaive::clear(){
  // clear environment
  cout << "clear environment" << endl;
  delete this->e;
  
  // clear agents
  // is done in environment

  // sim core
  cout << "clear simcore" << endl;
  delete simcore;
}




void SchellingNaive::init(  int sM, int iM, 
			    int nb,  int nr, int ng,  
			    int w,  int h, 
			    double uH,  double uU, 
			    int itM, double t){
  // copy params
  stepMax = sM;
  iterMax=iM;
  nBlue =nb;
  nRed = nr;
  nGreen=ng;
  width=w;
  height= h;
  uMoveIfHappy=uH;
  uMoveIfUnhappy=uU; 
  iterMaxRandomPosition=itM;
  tol=t;

  // create environment
  this->e = new EnvNaive(width, height);
  
  // init random position generation
  srand(time(NULL));		// TODO: get seed as argument
  
  // create interactions
  InteractionMatrixLine* iml=new InteractionMatrixLine;
  WanderSchellingInteraction* ws = new WanderSchellingInteraction(t,uH,uU, iterMaxRandomPosition);
  int priority = 1;
  iml->add(static_cast<DegenerateInteraction*>(ws),priority);
  int distance=1;
  PhysicalHalo* halo= new PhysicalHalo(distance);
  int color=1;
  
  // create agent and pointer
  Blue b(color, iml, halo);
  Blue *b_ptr;
  EnvNaive* en= static_cast<EnvNaive*>(this->e);  

  // reserve memory in vectors
  en->vecBlue.reserve(nBlue+nRed);
  e->reserveAgentPtr(nBlue+nRed);

  // main loop
  for(int i=0; i<(nBlue+nRed); i++)
    {   
      // push agent object to EnvNaive.vecBlue
      en->vecBlue.push_back(b);
      b_ptr = &(en->vecBlue[i]);

      // change color
      if(i>nBlue)
	b_ptr->setColor(2);

      // set id
      b_ptr->id =i;
 
      // push pointer to Env.vecAgentPtr
      e->pushAgentPtr( static_cast<Agent*>(b_ptr) );
      
      // move agent to random position
      moveToRandomPos( b_ptr, e, iterMaxRandomPosition);
      
    }
  
  // create agent ordering policy
  orderPolicy=new BasicAgentOrderingPolicy;
  
  // create simulation core
  SimulationCoreNaive *sc = new SimulationCoreNaive(static_cast<AgentOrderingPolicy*>(orderPolicy), static_cast<Env*>(e));
  simcore=static_cast<SimulationCore*>(sc); 
  
  
}


void SchellingNaive::checkState(){

  // check agent list
  EnvNaive* en= static_cast<EnvNaive*>(e);  
  vector<Blue>::iterator it;
  vector<Agent*>::iterator it_ptr = e->vecAgentPtr.begin();
  Blue *b;
  Position *p;
  PositionNaive *pn;
  int i=0;
  
  for( it= en->vecBlue.begin(); it != en->vecBlue.end(); ++it, ++it_ptr, i++ )
    {
      // from EnvNaive:vecBlue
      pn = static_cast<PositionNaive*> ((*it).getPosition());
      cout <<"i="<<i<< " id="<< (*it).id << " x="<< pn->x << " y="<< pn->y;
      // from Env:vecAgentPtr
      cout <<"\t"<<(*it_ptr)<< " id="<< (static_cast<Blue*>(*it_ptr))->id ; 
      p = (*it_ptr)->getPosition();
      pn = static_cast<PositionNaive*>(p);
      cout<<" x="<< pn->x  << " y="<< pn->y << endl;
    }
  // check all agents have been enumerated


  // check theGrid
  
}

void SchellingNaive::resetState(){
  cout<<" resetState not implemented so far"<<endl;
}


void SchellingNaive::singleRun(int s){
  for( step=0; step<s; step++)
    simcore->step();
  
}	

void SchellingNaive::fullRun(){
  for( iter=0; iter<iterMax; iter++){
    for( step=0; step<stepMax; step++){     
      // simulation step
      simcore->step();
      // compute instant order parameter
      //    segregation
    }
    resetState();
    // compute average order parameter over time steps
    //    seg
  }
  // compute average over iterations
  //    seg

}	     


vector< vector<int> > SchellingNaive::getState(){
  vector< vector<int> > state(width, vector<int>(height,0));
  
  Agent *a;
  Blue *b;
  Position *p;
  PositionNaive *pn=new PositionNaive;

  for(int i=0; i<width; i++)
    for(int j=0; j<height; j++)
      {
	// generate position
	pn->x = i;
	pn->y = j;
	p = static_cast<Position*>(pn);

	// get agent
	a = e->getAgent(p);
	
	// get agent's color
	if(a==NULL)
	  state[i][j]=0;
	else{
	  // PROBLEME: ici on doit connaître le type de l'agent
	  b = static_cast<Blue*>(a);
	  state[i][j] = b->getColor();
	  if(state[i][j] >MAX_COLOR | state[i][j]<0)
	    throw exception();
	}
      }
  
  // clear 
  delete pn;
  
  return state ;
}



vector<double> SchellingNaive::getSegregation(){
  vector<double> seg(10,0);
  return seg;
}



void SchellingNaive::printConfig(){
  cout<<"stepMax="<<stepMax<<" iterMax="<<iterMax<<" nBlue="<<nBlue<<" nRed="<<nRed<<" nGreen="<<nGreen<<endl;
  cout<<"width="<<width<<" heigth="<<height<<endl;
  cout<<"uMoveIfHappy="<<uMoveIfHappy<<" uMoveIfUnhappy="<<uMoveIfUnhappy<<endl;
  cout<<"iterMaxRandomPosition="<<iterMaxRandomPosition<<" tol="<<tol<<endl;

  EnvNaive *en = static_cast<EnvNaive*>(e);
  vector<Blue>::iterator it;
  int i=0;
  Blue *b_ptr;
  /*for(it = (*v).begin(); it!=(*v).end(); it++, i++)
    //for(int i=0; i<nBlue+nRed; i++)
    {     
      b_ptr = &(*it);
      cout<<i<<" ";
      b_ptr->printPosition();
      cout << endl;
    }
  */
}



//-----------------------------------------------------------
//   SIMULATION CLASSES   
//-----------------------------------------------------------


void SimulationCoreNaive::initialize()
{
  
}





//-----------------------------------------------------------
//   OTHER FUNCTIONS
//-----------------------------------------------------------


void moveToRandomPos(Blue *b, Env *e, int iterMaxRandomPosition){
  //allocation and initialisation
  Position* old_p = b->getPosition();
  PositionNaive* new_p=new PositionNaive();
  EnvNaive *en=static_cast<EnvNaive*>(e);
  
  bool canBePut;
  int width = en->getNrow();
  int height = en->getNcol();
  int j=0;
  
  // main loop
  do{
    new_p->x= rand()%width ; // watch index: must be in [0,width-1]
    new_p->y= rand()%height ;    

    canBePut = en-> canBePutAt(b, static_cast<Position*>(new_p));
    j++;
  }while ( !canBePut && j< iterMaxRandomPosition);

  // DEBUG
  if(DEBUG){
    cout<<"moveToRandomPos: canBePut="<< canBePut;
    cout <<"  new_pos x=";
    if(canBePut)
      cout<< new_p->x  <<" y=" << new_p->y<<endl;
    else
      cout<<" ---- failed ---- "<<endl;
  }    

  // put in environment
  if( canBePut )
    {
      en->removeAgent(b, static_cast<Position*>(old_p));     
      en->putAgent(b, static_cast<Position*>(new_p));
      // copy position pointer in agent 
      b->setPosition(static_cast<Position*>(new_p));
    }
  else
    throw std::exception();
  
  // clear
  delete new_p;
}
