 
#ifndef _SCHELLING_H
#define _SCHELLING_H

#include <iostream>
#include <vector>
#include <list>

using namespace std;

class Env;
class InteractionMatrixLine;
class Agent;

// ------------------------------------------------
class Position{
 public:
};

//-----------------------------------------------------------
//   INTERACTION CLASSES   
//-----------------------------------------------------------
class AbstractInteraction{
  
public:
  virtual void perform(Env* e, Agent* source, Agent* target)=0 ;
};


class DegenerateInteraction :public AbstractInteraction{
  
public:
  
};


class InteractionMatrixLine{
 private:
  InteractionMatrixLine( const InteractionMatrixLine& copy ); // DISABLED
  list<AbstractInteraction*> lstInterac;
  list<int> lstPriority;
public:
  InteractionMatrixLine();
  void add(DegenerateInteraction* i, int p);
  AbstractInteraction* getInteraction();
  ~InteractionMatrixLine();
};


//-----------------------------------------------------------
//   AGENT CLASSES   
//-----------------------------------------------------------

class PhysicalHalo{
 private:
  int distance;
 public:
  PhysicalHalo(int d){ distance=d;}
};


class Agent{
public:
  PhysicalHalo* halo;
  InteractionMatrixLine* iml;
  Position* p; // setPosition is implemented in derived class

  //Agent(){  }
  ~Agent();
  Position* getPosition();
  virtual void setPosition(Position* pos);
  virtual void perceive(Env* e, list<Agent*>* neighbors){}
  void setInteractionMatrixLine(InteractionMatrixLine* iml );
  InteractionMatrixLine*  getInteractionMatrixLine();
  void setHalo(PhysicalHalo* h);
  PhysicalHalo* getHalo();
};
 

//-----------------------------------------------------------
//   ENVIRONMENT CLASSES   
//-----------------------------------------------------------

class Env{
  
public:
  vector<Agent*> vecAgentPtr;

public:
  //Env(){} // car Env n'est pas pure abstract; sinon link error
  void pushAgentPtr(Agent* a);
  void reserveAgentPtr(int n);
  virtual bool canBePutAt(Agent* a, Position* p)=0;
  //virtual list<Agent> getAgents(Position p)=0;
  virtual Agent* getAgent(Position* p)=0;
  virtual void putAgent(Agent* a, Position* p)=0;
  virtual void removeAgent(Agent* a, Position* p)=0;
  virtual void getNeighborhoodInHalo(Agent* agent, PhysicalHalo* halo,
				     list<Agent*>* neighbors)=0; 
  
};



//-----------------------------------------------------------
//   SIMULATION CLASSES   
//-----------------------------------------------------------


class AgentOrderingPolicy{
public:
  //REMOVE
  //virtual void reorder(list<Agent*>* l)=0;
  virtual void reorder(vector<Agent*>* v)=0;
};


class BasicAgentOrderingPolicy: public AgentOrderingPolicy{
public:
  //REMOVE
  //void reorder(list<Agent*>* l);
  void reorder(vector<Agent*>* v);
};


// ------------------------------------------------
class SimulationCore{  // NOTE: pas abstract; comment faire constructeur ?
 protected: 
  AgentOrderingPolicy* orderPolicy;
  list<Agent*>* neighbors;
  Env* environment;
public:
  // Env est abstrait, on ne peut déclarer que des pointeurs	
  SimulationCore (AgentOrderingPolicy* p, Env* e)
    : orderPolicy(p), environment(e){
    neighbors=new list<Agent*>;
  } 
  ~SimulationCore(){
    delete neighbors;
  }
  virtual void initialize()=0;
  void step();
};

// ------------------------------------------------

/* 
	Schelling simulation abstract class
*/

class Schelling{ // is pure abstract class

  
 protected:
  // protected so that it can be accessed by derived class 
  Env* e;
  AgentOrderingPolicy* orderPolicy;
  SimulationCore* simcore;
  
  /* functions */
  virtual void resetState()=0;  // reset between (e.g. between two mc runs)
  
public:
  virtual void init( int stepMax, int iterMax, 
		     int nBlue,  int nRed, int nGreen,  
		     int width,  int height, 
		     double uMoveIfHappy,  double uMoveIfUnhappy, 
		     int iterMaxRandomPosition, double tol) = 0;
  virtual void singleRun(int s) = 0;	
  virtual void fullRun() = 0;	     
  //virtual void getState(vector<vector<int> >* a)=0;
  virtual vector<vector<int> > getState()=0;
  //virtual void getSegregation(vector<double>* s)=0;
  virtual vector<double> getSegregation()=0;
};




#endif
