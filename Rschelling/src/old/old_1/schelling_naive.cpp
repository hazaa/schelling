#include "schelling_naive.h"
#include<cstdio>

int DEBUG=0;

//-----------------------------------------------------------
//   ENVIRONMENT CLASSES   
//-----------------------------------------------------------

EnvNaive::EnvNaive(int nr, int nc){
  // see http://stackoverflow.com/questions/3371090/c-vector-based-two-dimensional-array-of-objects
  // http://www.parashift.com/c++-faq-lite/operator-overloading.html#faq-13.12
  nrow = nr;
  ncol= nc;
  // REMOVE: comprendre  ++i ???
  //for (int i=0; i<nrow;++i) theGrid.push_back(vector<Agent* >(ncol));
  theGrid = new Agent*[nrow*ncol];
    
  // initialize
  for (int i=0; i<nrow;i++)
    for (int j=0; j<ncol;j++)
      {
	//REMOVE
	//theGrid[i][j]=NULL ;
	theGrid[ i*nrow+j ]=NULL;
      }
}

EnvNaive::~EnvNaive(){

  // clear theGrid
  // REMOVE
  //for (int i=0; i<nrow;++i) theGrid.pop_back();
  delete theGrid;
  
  // clear lstAgent
  for (list<Agent*>::iterator it = lstAgent.begin();it!=lstAgent.end(); ++it) 
    delete (*it);
  
}

void EnvNaive::pushAgent(Agent* a)
{
  lstAgent.push_front(a);
}


bool EnvNaive::canBePutAt(Agent* a, Position* p){
  
  // remark that a is not used in this function

  Agent* b;
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);

  // REMOVE
  //b = theGrid[pNaive->x][pNaive->y];
  b = theGrid[(pNaive->x)*nrow + pNaive->y];
  
  if(b==NULL)
    return true;
  else
    return false;  
}


Agent* EnvNaive::getAgent(Position *p){
  
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  // REMOVE
  //Agent* a= theGrid[pNaive->x][pNaive->y];
  Agent* a= theGrid[(pNaive->x)*nrow + pNaive->y];
  
  return a;
}


// putAgent
// la grille pointe vers l'agent
void EnvNaive::putAgent(Agent* a, Position* p){
  
  // set position
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  // REMOVE
  //theGrid[pNaive->x][pNaive->y] = a;
  theGrid[(pNaive->x)*nrow + pNaive->y] = a;
  
  // debug
  if(DEBUG)
    cout<<"putAgent:"<< "theGrid[" << pNaive->x <<"]["<<pNaive->y <<"]"<<endl;
  
  // copy position pointer in agent 
  a->setPosition(p);
}


// removeAgent
// la grille pointe vers NULL
void EnvNaive::removeAgent(Agent* a, Position* p){
  PositionNaive* pNaive = static_cast<PositionNaive*>(p);
  
  if(DEBUG)
    cout<<"removeAgent:"<< "theGrid[" << pNaive->x <<"]["<<pNaive->y <<"]"<<endl;
  
  //REMOVE
  //theGrid[pNaive->x][pNaive->y] = NULL;
    theGrid[(pNaive->x)*nrow + pNaive->y] = NULL;
}

void EnvNaive::getNeighborhoodInHalo(Agent* agent, PhysicalHalo* halo,
				     list<Agent*>* neighbors){
  // get agent position
  PositionNaive* pn = static_cast<PositionNaive*>(agent->getPosition());

  // circle around the position
  for(int dx=-1; dx<=1;dx++){
    for(int dy=-1; dy<=1;dy++)
      {
	if(!(dx==0 && dy==0))
	  {
	    // add offset
	    int new_x = pn->x + dx;
	    int new_y = pn->y + dy;
	    // enforce toroidal geometry
	    if(new_x==nrow){new_x=0;}
	    if(new_x==-1){new_x=nrow-1;}
	   
	    if(new_y==ncol){new_y=0;}
	    if(new_y==-1){new_y=ncol-1;}

	    // if cell is nonempty, push agent in neighbor list
	    //cout<< "x="<< pn->x << " dx="<<dx<< " new_x="<<new_x <<"   y="<< pn->y<<" dy="<<dy<< " new_y="<<new_y<<endl;

	    //REMOVE
	    //Agent* a =theGrid[new_x][new_y];
	    Agent* a = theGrid[new_x*nrow + new_y];
	    
	    if(a !=NULL){
	      //cout<<"add neighbor"<<endl;
	      //(*neighbors).push_front(a);
	      neighbors->push_front(a);
	    }
	  }
      }
  }
  
}

int EnvNaive::getNrow()
{
  return nrow;
}
int EnvNaive::getNcol()
{
  return ncol;
}



//-----------------------------------------------------------
//   AGENT CLASSES   
//-----------------------------------------------------------

Blue::Blue(){
  PositionNaive *pn=new PositionNaive;
  pn->x =0;
  pn->y =0;

  p = static_cast<Position*>(pn);
}

Blue::Blue(int c){
  PositionNaive *pn=new PositionNaive;
  pn->x =0;
  pn->y =0;
  p = static_cast<Position*>(pn);
  color=c;
}


void Blue::setPosition(Position* pos){
  PositionNaive *pn = static_cast<PositionNaive*>(p);
  PositionNaive *posn = static_cast<PositionNaive*>(pos);
  pn->x = posn->x;
  pn->y = posn->y;
}


Blue::~Blue(){
  
  delete p;
}

int Blue::getColor(){
  return color;
}


void Blue::resetNeighborCount(){
  nbLikeAgents=0;
  nbUnlikeAgents=0;
}

void Blue::incrementLikeCount(){
  nbLikeAgents++;
}

void Blue::incrementUnlikeCount(){
  nbUnlikeAgents++;  
}

int Blue::getLikeCount(){
  return nbLikeAgents;
}

int Blue::getUnlikeCount(){
  return nbUnlikeAgents;  
}


void Blue::perceive(Env* e, list<Agent*>* neighbors){
  
  // init
  resetNeighborCount();
  int nNeighbor = (*neighbors).size();
  list<Agent*>::iterator it;
  
  // count neighbors that have the same color and those who don't
  for ( it = (*neighbors).begin();it!=(*neighbors).end(); ++it) 
    {
      Agent *a = *it;

      // !!!! PROBLEME: ici il faut connaître la classe de l'agent!!
      Blue* b = static_cast<Blue*>(a);

      if(color==b->getColor())
	incrementLikeCount();
      else
	incrementUnlikeCount();
    }

  //cout<< nNeighbor << " neighbors"<<"  nbLikeAgents="<< nbLikeAgents<< "  nbUnlikeAgents="<<nbUnlikeAgents<<endl;
    
  
  /* loops through neighbors and count likes and unlikes
  for(int i=0; i<neighborList.size(); i++){
    neighbor = neighborList.get(i);
    // INTERET: on n'a pas a mentionner le type de l'agent !!
    AskColorTargetFunction ctarget = (AskColorTargetFunction) neighbor;
    
    // get target color
    targetColor= ctarget.retrieveAgentColor();
    // increment
    if(targetColor.equals(agentColor))
    incrementLikeCount();
    else
    incrementUnlikeCount();		
    
  } 
  */

}




//-----------------------------------------------------------
//   INTERACTION CLASSES   
//-----------------------------------------------------------

WanderSchellingInteraction::WanderSchellingInteraction(double t, double uH, double uU, int iMax){
  tol = t;
  uMoveIfHappy=uH;
  uMoveIfUnhappy=uU;
  iterMaxRandomPosition=iMax;
}

void WanderSchellingInteraction::perform(Env* e, Agent* source, Agent* target) 
{
  // get like and unlike
  //  PROBLEM : agent type has to be known !!
  Blue *b=static_cast<Blue*>(source);
  int like = b->getLikeCount();
  int unlike = b->getUnlikeCount();
  int nNeighbor = like+unlike;
  bool happy;

  // compute happiness
  double t;
  if(nNeighbor==0){
    happy=true;
    t=1;
  }	
  else{	
    // attention: differs from jedi's implementation
    t = ((double)unlike)/((double)(like+unlike));// not defined if nNeighbor==0
    if( t<tol )
      happy = true;
    else
      happy=false;	
  }

  // decide to move
  // toss a coin to know if we try to move or not
  double u=(double)rand()/((double)RAND_MAX + 1); // !!!!! TOCHANGE
  bool tryMoveAgent;
  if((happy && u<uMoveIfHappy)||( !happy && u<uMoveIfUnhappy ))
    tryMoveAgent=true;
  else
    tryMoveAgent=false;

  // debug
  /*cout<< "Wander:perform:   t="<<t<< " tol="<<tol<< " happy="<<happy;
  cout<< " u="<<u<< "  uH="<<uMoveIfHappy<< "  uU="<<uMoveIfUnhappy; 
  cout<< " try="<<   tryMoveAgent<<endl;*/
   
  // move
  if(tryMoveAgent)
    moveToRandomPos( b, e, iterMaxRandomPosition);
  
}


//-----------------------------------------------------------
//   SCHELLING_NAIVE CLASSES   
//-----------------------------------------------------------

SchellingNaive::SchellingNaive(){
    iter = 0 ;
    step = 0;
}



SchellingNaive::~SchellingNaive(){
  // SEEMS NOT TO BE CALLED (WHEN DOING RM) ???
  if(DEBUG)
    cout << "entering destructor" << endl;
  
}


void SchellingNaive::clear(){
  // clear environment
  cout << "clear environment" << endl;
  delete this->e;
  
  // clear agents
  // is done in environment

  // sim core
  cout << "clear simcore" << endl;
  delete simcore;
}




void SchellingNaive::init(  int sM, int iM, 
			    int nb,  int nr, int ng,  
			    int w,  int h, 
			    double uH,  double uU, 
			    int itM, double t){
  // copy params
  stepMax = sM;
  iterMax=iM;
  nBlue =nb;
  nRed = nr;
  nGreen=ng;
  width=w;
  height= h;
  uMoveIfHappy=uH;
  uMoveIfUnhappy=uU; 
  iterMaxRandomPosition=itM;
  tol=t;

  // create environment
  this->e = new EnvNaive(width, height);
  
  // init random position generation
  srand(time(NULL));		// TODO: get seed as argument
  
  // create interactions
  InteractionMatrixLine* iml=new InteractionMatrixLine;
  WanderSchellingInteraction* ws = new WanderSchellingInteraction(t,uH,uU, iterMaxRandomPosition);
  int priority = 1;
  iml->add(static_cast<DegenerateInteraction*>(ws),priority);
  int distance=1;
  PhysicalHalo* halo= new PhysicalHalo(distance);
  int color;

  // create blue and red agents
  for(int i=0; i<(nBlue+nRed); i++){

    // choose color
    if(i<nBlue)
      color=1;
    else
      color=2;

    // construct
    Blue* b=new Blue(color);
    
    // push to listAgents
    e->pushAgent( static_cast<Agent*>(b) );
    
    // register interactions & halo
    b->setInteractionMatrixLine(iml);
    b->setHalo(halo);

    // move agent to random position
    moveToRandomPos( b, e, iterMaxRandomPosition);
  }
  
  // create agent ordering policy
  orderPolicy=new BasicAgentOrderingPolicy;
    
  // create simulation core
  SimulationCoreNaive *sc = new SimulationCoreNaive(static_cast<AgentOrderingPolicy*>(orderPolicy), static_cast<Env*>(e));
  simcore=static_cast<SimulationCore*>(sc); 

  
}


void SchellingNaive::resetState(){
  cout<<" resetState not implemented so far"<<endl;
}


void SchellingNaive::singleRun(int s){
  for( step=0; step<s; step++)
    simcore->step();
  
}	

void SchellingNaive::fullRun(){
  for( iter=0; iter<iterMax; iter++){
    for( step=0; step<stepMax; step++){     
      // simulation step
      simcore->step();
      // compute instant order parameter
      //    segregation
    }
    resetState();
    // compute average order parameter over time steps
    //    seg
  }
  // compute average over iterations
  //    seg

}	     


vector< vector<int> > SchellingNaive::getState(){
  vector< vector<int> > state(width, vector<int>(height,0));
  
  Agent *a;
  Blue *b;
  Position *p;
  PositionNaive *pn=new PositionNaive;

  for(int i=0; i<width; i++)
    for(int j=0; j<height; j++)
      {
	// generate position
	pn->x = i;
	pn->y = j;
	p = static_cast<Position*>(pn);

	// get agent
	a = e->getAgent(p);
	
	// get agent's color
	if(a==NULL)
	  state[i][j]=0;
	else{
	  // PROBLEME: ici on doit connaître le type de l'agent
	  b = static_cast<Blue*>(a);
	  state[i][j] = b->getColor();
	}
      }
  
  // clear 
  delete pn;
  
  return state ;
}



vector<double> SchellingNaive::getSegregation(){
  vector<double> seg(10,0);
  return seg;
}



void SchellingNaive::printConfig(){
  cout<<"stepMax="<<stepMax<<" iterMax="<<iterMax<<" nBlue="<<nBlue<<" nRed="<<nRed<<" nGreen="<<nGreen<<endl;
  cout<<"width="<<width<<" heigth="<<height<<endl;
  cout<<"uMoveIfHappy="<<uMoveIfHappy<<" uMoveIfUnhappy="<<uMoveIfUnhappy<<endl;
  cout<<"iterMaxRandomPosition="<<iterMaxRandomPosition<<" tol="<<tol<<endl;
  
}



//-----------------------------------------------------------
//   SIMULATION CLASSES   
//-----------------------------------------------------------


void SimulationCoreNaive::initialize()
{
  
}



//-----------------------------------------------------------
//   OTHER FUNCTIONS
//-----------------------------------------------------------


void moveToRandomPos(Blue *b, Env *e, int iterMaxRandomPosition){
  //allocation and initialisation
  Position* old_p = b->getPosition();
  PositionNaive* new_p=new PositionNaive();
  EnvNaive *en=static_cast<EnvNaive*>(e);
  
  bool canBePut;
  int width = en->getNrow();
  int height = en->getNcol();
  int j=0;
  
  // main loop
  do{
    new_p->x= rand()%width ; // watch index: must be in [0,width-1]
    new_p->y= rand()%height ;    

    canBePut = en-> canBePutAt(b, static_cast<Position*>(new_p));
    j++;
  }while ( !canBePut && j< iterMaxRandomPosition);

  // DEBUG
  if(DEBUG){
    cout<<"moveToRandomPos: canBePut="<< canBePut;
    cout <<"  new_pos x=";
    if(canBePut)
      cout<< new_p->x  <<" y=" << new_p->y<<endl;
    else
      cout<<" ---- failed ---- "<<endl;
  }    

  // put in environment
  if( canBePut )
    {
      en->removeAgent(b, static_cast<Position*>(old_p));     
      en->putAgent(b, static_cast<Position*>(new_p));
    }
  else
    throw std::exception();
  
  // clear
  delete new_p;
}
