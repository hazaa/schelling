#ifndef _SCHELLING_NAIVE_H
#define _SCHELLING_NAIVE_H
 
#include "schelling.h"

#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

/* 
   Naive implementation of Schelling abstract class
   TODO:
   - RNG ? 
   - ordering policy ?? fr.lifl.jedi.controllersCore.orderingPolicies.
*/


//-----------------------------------------------------------
//   POSITION CLASSES   
//-----------------------------------------------------------

class PositionNaive: public Position{
 public:
  int x;
  int y;
};

//-----------------------------------------------------------
//   AGENT CLASSES   
//-----------------------------------------------------------


class Blue:public Agent{
private:
  int color;
  int nbLikeAgents;
  int nbUnlikeAgents;
  bool happy;
  
private:
  void resetNeighborCount();
  void incrementLikeCount();
  void incrementUnlikeCount();
  
public:
  Blue();	
  Blue(int c);
  ~Blue();	
  void setPosition(Position* pos);
  void perceive(Env* e,list<Agent*>* neighbors);
  int getColor();
  int getLikeCount();
  int getUnlikeCount();
};



//-----------------------------------------------------------
//   ENVIRONMENT CLASSES   
//-----------------------------------------------------------


class EnvNaive: public Env{
  
  //vector< vector<Agent*>  > theGrid;
  Agent** theGrid;

  int nrow; 
  int ncol;

 public:
  //EnvNaive();
  EnvNaive(int nr, int nc);
  ~EnvNaive();
  void pushAgent(Agent* a);
  bool canBePutAt(Agent* a, Position* p);
  Agent* getAgent(Position* p);
  void putAgent(Agent* a, Position* p);
  void removeAgent(Agent* a, Position* p);
  void getNeighborhoodInHalo(Agent* agent, PhysicalHalo* halo,
			     list<Agent*>* neighbors); 
  int getNrow();
  int getNcol();

};

//-----------------------------------------------------------
//   VARIOUS FUNCTIONS   
//-----------------------------------------------------------

void moveToRandomPos(Blue *b, Env *e, int iterMaxRandomPosition);


//-----------------------------------------------------------
//   INTERACTION CLASSES   
//-----------------------------------------------------------

class WanderSchellingInteraction: public DegenerateInteraction{
private:
  double tol;
  double uMoveIfHappy;
  double uMoveIfUnhappy;
  int iterMaxRandomPosition;
public:
  WanderSchellingInteraction(double t , double uH, double uU, int iMax);
  void perform(Env* e, Agent* source, Agent* target) ;
};


//-----------------------------------------------------------
//   SIMULATION CLASSES   
//-----------------------------------------------------------




class SimulationCoreNaive: public SimulationCore{ 
  
public:
  SimulationCoreNaive (AgentOrderingPolicy* p, Env* e) 
    : SimulationCore(p,e){ }
  virtual void initialize();
};




//-----------------------------------------------------------
//   SCHELLING_NAIVE CLASSES   
//-----------------------------------------------------------

class SchellingNaive :public Schelling {

private:
  /* simulation parameters */
  int stepMax ;               // maximum steps per run
  int step ;                  // current step
  int iterMax ;               // maximum number of monte-carlo simulations
  int iter;              // index of current monte-carlo simulations
  int nBlue ;	         // number of blue agents
  int nRed ;             // number of red agents
  int nGreen ;           // number of green agents
  int width ;            // grid width
  int height ;	         // grid height
  double uMoveIfHappy ;  // probability of jump if happy 
  double uMoveIfUnhappy ;// probability of jump if unhappy  
  int iterMaxRandomPosition ;// nb of attempts to find a place when jumping
  double tol; // tolerance

  /* order parameters */
  vector<double> segregation; // one per montecarlo run
  
private:
   
  void resetState();
  
public:
  SchellingNaive();
  
  ~SchellingNaive();

  void init(  int sM, int iM, int nb,  int nr, int ng,  
	      int w,  int h, double uH,  double uU, 
	      int itM, double t); 
  
  void singleRun(int s);
  
  void fullRun();	     
  
  vector< vector<int> > getState();
  
  vector<double> getSegregation();
  
  void printConfig();

  void clear();	
  
};




#endif
