#include "iface_dens.h"

double iface_dens(int** matrix, int nrow, int ncol, int ncolor, int* nagent){
  
  // init & allocation of local matrix
  double iface_dens=-1.0 ;   // density of heterogeneous interface 
  int iface_nb   = 0;   // number of heterogeneous interface 
 
  // main loop 
  //for (int icolor=0; icolor<ncolor; icolor++) (double)nagent[icolor]
  int c;        // this cell
  int c_next_x; // next cell along x axis
  int c_next_y; // next cell along y axis

  for(int i=0; i<nrow; i++) 
    for(int j=0; j<ncol; j++) 
      {
	// get content of cells
	c = matrix[i][j];
	
	// compute interface number
	if( c>0)
	  {
	    // get content of cells
	    if(i==nrow-1)
	      c_next_x = matrix[0][j]; // toroidal geometry
	    else
	      c_next_x = matrix[i+1][j];
	    
	    if(j==ncol-1)
	      c_next_y = matrix[i][0]; // toroidal geometry
	    else
	      c_next_y = matrix[i][j+1];
	    
	    // increment heterogeneous interface
	    if( c_next_x>0)
	      if( c != c_next_x) iface_nb++;
	    
	    if( c_next_y>0)
	      if( c != c_next_y) iface_nb++;	    
	  }
      }
  // normalize to get interface density
  iface_dens = ((double)iface_nb )/((double)(nrow*ncol)) ;
  
  return iface_dens;
}



