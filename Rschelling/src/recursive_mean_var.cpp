#include "recursive_mean_var.h"

// see http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#On-line_algorithm
// http://mathworld.wolfram.com/SampleVarianceComputation.html
// http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19690028796_1969028796.pdf
void recursive_mean_var( double *mean_new, double *M2_new, double mean, double M2, double x_new, int n)
{
  double delta = x_new - mean;
  *mean_new = mean + delta/n;
  *M2_new = M2 + delta*(x_new - *mean_new);
}
