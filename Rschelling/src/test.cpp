#include "schelling_naive.h"
#include "test.h"

void barfoo(){
  // init
  int n=5;
  vector<foo> v;
  v.reserve(5);
  vector<foo>::iterator it;
  foo *foo_ptr;
  foo f;
  f.printb();
  for(int i=0;i<n;i++)
    v.push_back(f);
  //v->assign(5,f);
  
  // print
  for(it = v.begin(); it!=v.end(); it++ )
    it->printb();
  
  // change content
  int i=0;
  for(it = v.begin(); it!=v.end(); it++,i++ )
    {
      foo_ptr = it->getptr();
      dofoo(foo_ptr,i);
    }
  // print again
  for(it = v.begin(); it!=v.end(); it++ )
    it->printb();
  
  cout<<"size v="<< v.size()<<endl;
  //v->erase( v->begin(), v->end() );
}



int main(void){
  
  //barfoo();
  //return 0;
  
  /* ------------------------- */
  int stepMax = 3000;
  int iterMax= 20; 

  int nBlue = 300;
  int nRed = 300;
  int nGreen= 10;
  int width= 30;
  int height= 30;

  double uMoveIfHappy=  0.0001;
  double uMoveIfUnhappy= 0.1; 
  double pChange= 0.1;
  int trigTime = 100;
  int iterMaxRandomPosition= 1000;
  double tol=0.5;
  int stepBurn=500;

  SchellingNaive sch;
  sch.init(stepMax, iterMax, nBlue, nRed, nGreen, width, height, 
	   uMoveIfHappy, uMoveIfUnhappy, 
	   pChange, trigTime,
	   iterMaxRandomPosition, tol,   stepBurn);
  cout<<"--------checkstate------------"<<endl;
  sch.checkState();
  cout<<"---------getstate-----------"<<endl;
  sch.getState();
  cout<<"---------printconfig-----------"<<endl;
  sch.printConfig();

  sch.fullRun();
  sch.getState();
}
