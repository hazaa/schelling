/*
  This class provides the glue between class schelling (and derived)
  and rccp_module.cpp, so that schelling can be independent from Rcpp
  
*/
#ifndef _SCHELLING_RWRAP_H
#define _SCHELLING_RWRAP_H

#include <Rcpp.h>
using namespace Rcpp;

#include "schelling_naive.h"          // HERE THE IMPLEMENTATION IS CHOSEN

/*  */
typedef std::vector<int> vec_int;

/*
  this class wraps 
*/
class SchellingRwrap{
  
private:
  SchellingNaive schell;                 // HERE THE IMPLEMENTATION IS CHOSEN
  
public:
  void init(  int stepMax, int iterMax, 
	      int nBlue,  int nRed, int nGreen,  
	      int width,  int height, 
	      double uMoveIfHappy,  double uMoveIfUnhappy, 
	      int iterMaxRandomPosition, double tol, int stepBurn){
    schell.init(stepMax, iterMax, 
		nBlue, nRed, nGreen,  
		width, height, 
		uMoveIfHappy, uMoveIfUnhappy, 
		iterMaxRandomPosition,  tol, stepBurn);
  } 
  

  void singleRun(int step){
    schell.singleRun(step);
  }	
  
  void fullRun(){
    schell.fullRun();
  }	     

  void addAgent(Rcpp::NumericVector nb_agent){
    /* cf Rcpp-modules.pdf, p.13 */
    vec_int obj;
    vec_int::iterator it = obj.begin() ;
    obj.insert( it, nb_agent.begin(), nb_agent.end() );

    schell.addAgent(obj);
  }
  
  SEXP getState(){
    return wrap(schell.getState());
  }
  
  SEXP getSegregation(){
    // wrap: cf Rcpp-introduction.pdf, §3.1
    return wrap(schell.getSegregation()) ;
  }

  SEXP getSegregationRun(){
    // wrap: cf Rcpp-introduction.pdf, §3.1
    return wrap( schell.getSegregationRun() );
  }

  SEXP getSegregationIter(){
    // wrap: cf Rcpp-introduction.pdf, §3.1
    return wrap( schell.getSegregationIter() );
  }

  SEXP getSegregationRecMean(){
    // wrap: cf Rcpp-introduction.pdf, §3.1
    return wrap( schell.getSegregationRecMean() );
  }
  SEXP getSegregationRecVar(){
    // wrap: cf Rcpp-introduction.pdf, §3.1
    return wrap( schell.getSegregationRecVar() );
  }
  
  void printConfig(){
    schell.printConfig();
  }	     
  
  void clear(){
    schell.clear();
  }	     

  void resetState(){
    schell.resetState();
  }

};


#endif
