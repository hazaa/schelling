#ifndef _HK_H
#define _HK_H


int uf_find(int x);
int uf_union(int x, int y);
int uf_make_set(void);
void uf_initialize(int max_labels);
void uf_done(void) ;
void print_matrix(int **matrix, int m, int n) ;
int hoshen_kopelman(int **matrix, int m, int n);
void check_labelling(int **matrix, int m, int n); 
void cluster_size(int **matrix, int m, int n,int* clust_vect, int clusters);
void mask_matrix(int **matrix, int m, int n,int clust_vect);
int sum_square(int *v, int n);

#endif
