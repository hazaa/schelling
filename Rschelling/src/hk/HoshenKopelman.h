/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class phystat_hk_HoshenKopelman */

#ifndef _Included_phystat_hk_HoshenKopelman
#define _Included_phystat_hk_HoshenKopelman
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     phystat_hk_HoshenKopelman
 * Method:    clusters
 * Signature: ([[IIII[I)F
 */
JNIEXPORT jfloat JNICALL Java_phystat_hk_HoshenKopelman_clusters
  (JNIEnv *, jobject, jobjectArray, jint, jint, jint, jintArray);

#ifdef __cplusplus
}
#endif
#endif
