#include "hk_wrap.h"
#include <iostream>
using namespace std;

int main(){
  
  // params
  int nrow = 10;
  int ncol = 9;
  int ncolor = 2;

  // allocation
  int** matrix = new int*[nrow];
  for (int i=0; i<nrow; i++)
    matrix[i] = new int[ncol];
  
  matrix[1][1]=1;
  matrix[1][2]=1;

  matrix[5][5]=2;
  matrix[5][6]=2;
  matrix[6][5]=2;
  
  int *nagent = new int[ncolor];
  nagent[0]=2;
  nagent[1]=3;

  // main call
  double s = hk_wrap(matrix, nrow, ncol, ncolor, nagent);
  cout <<"segregation="<< s<<endl;

  // free memory
  for (int i=0; i<nrow; i++)
    delete matrix[i];
  delete matrix;
  
  delete nagent;

  return 0;
}
