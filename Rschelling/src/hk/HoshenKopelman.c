#include <jni.h>
#include "HoshenKopelman.h"
#include <stdlib.h>

#include "hk.h"

JNIEXPORT float JNICALL Java_phystat_hk_HoshenKopelman_clusters
  (JNIEnv *jenv, jobject jobj, jobjectArray grid, jint nrow, jint ncol, jint ncolor, jintArray nagent)
  {

/*
  for(i=0; i<3; i++) {
     jintArray oneDim= 
	(jintArray)env->GetObjectArrayElement(
	                     elements, i);
     jint *element=env->GetIntArrayElements(oneDim, 0);
     for(j=0; j<3; j++) {
        localArrayCopy[i][j]= element[j];
     }
  }
*/
  
  	// allocate matrices
  	int m=nrow;
  	int n=ncol;
  	int **matrix;
  	float s=0;
  
    matrix = (int **)calloc(m, sizeof(int*));
     
  // allocate local matrix
   for (int i=0; i<m; i++)
    matrix[i] = (int *)calloc(n, sizeof(int));
     
	for (int icolor=0; icolor<ncolor; icolor++){
	
	  // copy into local matrix
	  for(int i=0; i<m; i++) {
	     jintArray oneDim= 
		(jintArray)jenv->GetObjectArrayElement(grid, i);
	     jint *element=jenv->GetIntArrayElements(oneDim, 0);
    	 for(int j=0; j<n; j++) 
    	    matrix[i][j]= element[j];
    	 }

		// mask
		int color = icolor+1;
		mask_matrix(matrix, m, n, color);
		
	  	// Hoshen Kopelman
  		int clusters = hoshen_kopelman(matrix,m,n);
  	
  		// Computer cluster size
  		int* clust_vect = (int *)calloc(clusters, sizeof(int));
		cluster_size(matrix, m, n, clust_vect, clusters);
  	
  		// get agent number for this color
  		jint *na=jenv->GetIntArrayElements(nagent, 0);
  	
  		// compute segregation
  		s += ((float)sum_square(clust_vect,clusters))/((float)na[icolor] );	
  		
  		// DEBUG
  		//print_matrix(matrix,m,n);
		//for(int i=0; i<clusters; i++)
		//	printf("cluster %d has %d elements, nagent=%d, s=%f \n", i, clust_vect[i], na[icolor], s);
		
		// free memory
  		free(clust_vect);

  }
  
  	// free memory
	// ReleaseIntArrayElements ???
  
  	 for (int i=0; i<m; i++)
      free(matrix[i]);
    free(matrix);
  
	return s;
  }
