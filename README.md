# Schelling

Code de simulation et diagrammes de phase pour le modèle de Schelling avec
agents switch.


# Publications


*  [epjb13](https://doi.org/10.1140/epjb/e2013-31142-1), [arxiv](https://arxiv.org/abs/1212.4929)


# Installation

   >git clone git@gitlab.com:hazaa/schelling.git

Attention aux variables d'environnement:

   >export PYTHONPATH= $SCHELL_PATH/phasePy

Ou bien en début de code: 

   >import sys
   >sys.path.append('/your-path/git/schelling/phasePy')

# Description des fichiers

* [code_papers/](code_papers): les codes associés aux papiers.
   *  les données brutes d'expérience au format hdf5: [schelling-2012/data](schelling/code_papers/schelling-2012/data), [schelling-2013/data](schelling/code_papers/schelling-2013/data)
   *  les figures: [schelling-2012/fig](schelling/code_papers/schelling-2012/fig), [schelling-2013/fig](schelling/code_papers/schelling-2013/fig)
* [phasePy/](phasePy): diagramme de phase.
* [schellingCpp/](schellingCpp): le simulateur naif en c++, utilisé pour le papier epjb13.
* [pyschell2/](pyschell2): le simulateur c++ de L.Gauvin ([PhD](https://hal.archives-ouvertes.fr/tel-00556769)) modifié pour nos expériences.
   A préférer pour des expériences ultérieures.
* notebook jupyter/ipython: [schelling_final_state.ipynb](https://gitlab.com/hazaa/schelling/blob/master/code_papers/schelling-2013/schelling_final_state.ipynb) (les figures n'apparaissent pas !!), avec [nbviewer](https://nbviewer.jupyter.org/url/gitlab.com/hazaa/schelling/blob/master/code_papers/schelling-2013/schelling_final_state.ipynb) (ne marche pas !!)

# Exemple d'utilisation

Dépendances: python 2.7, python-h5py


Lecture et affichage de résultats:

```python

import sys
sys.path.append('/home/aurelien/local/git/schelling/phasePy')
sys.path.append('/home/aurelien/local/git/schelling/code_papers/schelling-2012')
import phase_diag   
from phase_diagram_noisetol import *
import numpy as np
import matplotlib.pylab as plt
import h5py
    
fname='/home/aurelien/local/git/schelling/code_papers/schelling-2012/data/phase_diagram_noisetol_9.hdf5'
pd_noisetol = phase_diagram_noisetol()
pd_noisetol.init() 
pd_noisetol.load(fname)

# get datasets:  interface density
dset = pd_noisetol.pd.hdf5_file['OrderParameter0'] 
order_param = pd_noisetol.pd.get_ord(0)
C1 = dset.value

# get control parameters
frac=[]
tol=[]
for k,v in dset.attrs.iteritems(): 
	print k,v
	if k=="control_param frac": 
		frac = v
	if k=="control_param tol":
		tol = v

# plot
plt.matshow(C1)
plt.contourf(C1)
j=10;plt.plot(frac,C1[:,j])
plt.xlabel('frac');plt.ylabel('$x_{\infty}$');plt.title('tol=%f'%(tol[j]))
i=1;plt.plot(tol,C1[i,:])
plt.xlabel('tol');plt.ylabel('$x_{\infty}$');plt.title('frac=%f'%(frac[i]))

for i in range(20):
	plt.plot(C1[i,:])
for j in range(20):
	plt.plot(C1[:,j])

# get datasets: susceptibility
dset = pd_noisetol.pd.hdf5_file['OrderParameter1'] 

# other plots
# contour plot => marche pas ???
C2 =  dset.value
dset.attrs.items()
levels = np.linspace(np.min(order_param),
						np.max(order_param), 
					 pd_noisetol.pd.nlevels)
levels=30					 
CS=plt.contourf(C1,C2,order_param, levels,
					#cmap=plt.cm.get_cmap('jet', len(levels)-1),
					)
# 3d plot	=> marche pas ???	
ax = plt.gca(projection='3d')
ax.plot_wireframe(C1,C2,order_param,rstride=8, cstride=8,colors='k')

# auto plot => marche pas ???
pd_noisetol.plot() 
```

Ou bien:

   >cd $SCHELL_PATH/code_papers/schelling-2012 
   
   >python phase_diagram_noisetol.py -infile data/phase_diagram_noisetol_1.hdf5


Simuler:
